# deprecated - we will not read CBIS DDSM for NYU as it doesn't have 4 views
#
# prepare.py - preparar dataset CBIS-DDSM para rodar com NYU ou outras redes
#
#             "A principio nao sera necessario pois NYU nao roda com images separadas"
#
# Versao 1 - le arquivos CSV e gera imagens FULL com nomes com base no patient_id
#            e cria novo CSV com labels para comparar com resultado NYU.
#          - precisa ter as 4 imagens CC{left, right} e MLO{left, right} para NYU

#TODO
#           -ler arquivos CSV e gera imagens FULL com nomes com base no patient_id
#            e cria novo CSV com labels para comparar com resultado NYU.

##DONE:
#          - precisa ter as 4 imagens CC{left, right} e MLO{left, right} para NYU? 
#               -->SIM! NYU usa na camada full conected final (nao adianta enganar tb)

import pandas as pd
df = pd.read_csv("csv/calc_case_description_test_set.csv")

print(df.info())
print(df.head(10))

print(df['image view'].dtypes)
print(df[['patient_id', 'left or right breast', 'image view', 'abnormality type', 'pathology', 'image file path']])

#filtra por um campo e mostra os outros campos desejados
print(df[df['abnormality id'] == 1][['patient_id', 'left or right breast', 'image view', 'abnormality type', 'pathology', 'image file path']])

# agrupa e conta por imagens completas
print(df.groupby('image file path').count())

# agrupa e conta por imagens completas
print(df.groupby('patient_id').count())

# juntar essas categorias acima
# ver tutorial em https://data36.com/pandas-tutorial-3-important-data-formatting-methods-merge-sort-reset_index-fillna/

## Campos do CSV
# Patient ID: the first 7 characters of images in the case file
# Density category
# Breast: Left or Right
# View: CC or MLO
# Number of abnormality for the image (This is necessary as there are some cases containing multiple abnormalities.
# Mass shape (when applicable)
# Mass margin (when applicable)
# Calcification type (when applicable)
# Calcification distribution (when applicable)
# BI-RADS assessment
# Pathology: Benign, Benign without call-back, or Malignant
# Subtlety rating: Radiologists’ rating of difficulty in viewing the abnormality in the image
# Path to image files