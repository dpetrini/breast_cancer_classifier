# obtido repositorio github.com/ddsm_tools e modificado.
# Jun 2019

import os

####################################################
# Extract value from split list of data
####################################################
def get_value(lst, row_name, idx):
    """
    :param lst: data list, each entry is another list with whitespace separated data
    :param row_name: name of the row to find data
    :param idx: numeric index of desired value
    :return: value
    """
    val = None
    for l in lst:
        if not l:
            continue

        if l[0] == row_name:
            try:
                val = l[idx]
            except Exception:
                print (row_name, idx)
                print (lst)
                val = None
            break
    return val


####################################################
# ICS Information extraction
####################################################
scanner_map = {
    ('A', 'DBA'): 'MGH',
    ('A', 'HOWTEK'): 'MGH',
    ('B', 'LUMISYS'): 'WFU',
    ('C', 'LUMISYS'): 'WFU',
    ('D', 'HOWTEK'): 'ISMD'
}


def get_ics_info(ics_file_path):
    """
    :param ics_file_path: path to ics file
    :return: dictionary containing all relevant data in ics file
    """
    # get letter for scanner type
    ics_file_name = os.path.basename(ics_file_path)
    letter = ics_file_name[0]

    ics_dict = {}

    # get data from ics file
    with open(ics_file_path, 'r') as f:
        linha = f.readlines()

    for linhas in linha:
        k = linhas.strip().split()
        if (len(k)) == 0:   # skip first
            continue
        #print(k)
        #print(k[0])
        if k[0] == 'filename':
            if len(k) > 1:
                ics_dict['patient_id'] = k[1]        
        if k[0] == 'PATIENT_AGE':
            if len(k) > 1:
                ics_dict['age'] = k[1]
        if k[0] == 'DIGITIZER':
            if len(k) > 1:    
                ics_dict['scanner_type'] = k[1] 
                ics_dict['scan_institution'] = scanner_map[(letter, k[1])]       
        if k[0] == 'DENSITY':
            if len(k) > 1:
                ics_dict['density'] = k[1]
        for sequence in ['LEFT_CC', 'RIGHT_CC', 'LEFT_MLO', 'RIGHT_MLO']:
            if k[0] == sequence:
                sequence_dict = {
                    'height': int(k[2]),
                    'width': int(k[4]),
                    'bpp': int(k[6]),
                    'resolution': float(k[8])
                }
                ics_dict[sequence] = sequence_dict       
  

    return ics_dict


####################################################
# Overlay Information extraction
####################################################
def get_abnormality_data(file_name):
    """
    :param file_name: file path of overlay file
    :return: data about abnormality
    """

    # get data from ics file
    with open(file_name, 'r') as f:
        linhas = f.readlines()

    for linha in linhas:
        k = linha.strip().split()
        if (len(k)) == 0:   # skip first
            continue
        #print(k)
        #print(k[0])
        if k[0] == 'TOTAL_ABNORMALITIES':
            if len(k) > 1:
                try:
                    total_abnormalities = int(k[1])  
                    print('abn', total_abnormalities)
                except:
                    total_abnormalities = 0   
        if total_abnormalities == 0:
            return []


    abnormality_data = []
    next_data = False
    abn = 1     # numero de anormalidades (pode ter mais que um no arquivo)
    # percorre as linhas obtendo os valores das chaves
    for linha in linhas:
        tokens = linha.strip().split() 

        if len(tokens) == 0:
            continue

        if tokens[0] == 'ABNORMALITY':
            if int(tokens[1]) != (abn):
                print('Grande problema', tokens[1], abn, tokens)
            abn +=1    # se vir aqui de novo pode ser a segunda
            abn_num = tokens[1]
        if tokens[0] == 'LESION_TYPE':
            abnormality_type = tokens[1:]
        if tokens[0] == 'ASSESSMENT':
            assessment = tokens[1]
        if tokens[0] == 'PATHOLOGY':
            pathology = tokens[1]
        if tokens[0] == 'SUBTLETY':
            subtlety = tokens[1]                
        if tokens[0] == 'BOUNDARY':
            next_data = True
            continue
        if next_data:
            data = tokens     # ROI 

            abnormality_data.append((file_name, abn_num, abnormality_type, 
                                        assessment, pathology, subtlety,data))
            print(file_name.split('/')[-1], abn_num, abnormality_type, 
                                        assessment, pathology, subtlety, len(data))
            next_data = False

    return abnormality_data