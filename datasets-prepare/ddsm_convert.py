#
#   Creates modern images copy of DDSM
#
#   Example: python3 ddsm_convert.py -p 'DDSM category path' -o 'new DDSM category path'
#
#   Example: python3 ddsm_convert.py -p '/Volumes/DGPP02_1TB/usp/DDSM/benign_without_callbacks' \
#                                    -o '/Volumes/DGPP02_1TB/usp/DDSM-new/benign_without_callbacks'
#
#   You may run for each of categories: benign_without_callbacks, benigns, cancers, normals
#   

import numpy as np
import argparse
import os
import cv2
from subprocess import call
import sys

from ddsm_util import get_ics_info

# funcao direto do ddsm - cbis conversion
def od_correct(im_raw):
    """
    Map gray levels to optical density level
    :param im: image
    :return: optical density image
    """
    im_od = np.zeros_like(im_raw, dtype=np.float64)

    if (ics_dict['scan_institution'] == 'MGH') and (ics_dict['scanner_type'] == 'DBA'):
        im_od = (np.log10(im_raw + 1) - 4.80662) / -1.07553  # add 1 to keep from log(0)
    elif (ics_dict['scan_institution']  == 'MGH') and (ics_dict['scanner_type'] == 'HOWTEK'):
        im_od = (-0.00094568 * im_raw) + 3.789
    elif (ics_dict['scan_institution']  == 'WFU') and (ics_dict['scanner_type'] == 'LUMISYS'):
        im_od = (im_raw - 4096.99) / -1009.01
    elif (ics_dict['scan_institution']  == 'ISMD') and (ics_dict['scanner_type'] == 'HOWTEK'):
        im_od = (-0.00099055807612 * im_raw) + 3.96604095240593

     # perform heath noise correction
    im_od[im_od < 0.05] = 0.05
    im_od[im_od > 3.0] = 3.0

    return im_od

# MAIN
ap = argparse.ArgumentParser()
ap.add_argument("-b", "--bpp",
     help = "bits per pixel to convert")
ap.add_argument("-p", "--path", required = True,
    help = "path to the root image dataset which contain directories")
ap.add_argument("-o", "--outpath", required = True,
    help = "path to tnew folder which will contain converted dataset")

args = vars(ap.parse_args())

if (args["bpp"]):
    bpp = int(args["bpp"])
    print(bpp)
    if (bpp != 8 and bpp != 16):
        print("Entrar com bpp valido 8 ou 16")
        sys.exit()
else:
    bpp = 16

root = args["path"]
outpath = args["outpath"]

# checa diretorios
if (not os.path.isdir(root)) :
    print ("Not a valid input directory")
    sys.exit()

# cria diretorio de saida
if (not os.path.isdir(outpath)):
    #print ("Not a valid output directory")
    print('Creating dir: ', outpath)
    os.makedirs(outpath, exist_ok=True)
    if (not os.path.isdir(outpath)):
        print("Error creating output dir")
        sys.exit()

#root = '/Users/danielpetrini/usp/fmusp/ddsm_tools/'

top_dir_list = []

# recupera os diretorios internos TOP LEVEL ao path argumento
# ex. benign_01, benign_02...
for top_dirs in os.listdir(root):
    if (os.path.isdir(os.path.join(root,top_dirs))):
        top_dir_list.append(os.path.join(root,top_dirs))
        print(top_dirs, os.path.join(root,top_dirs))

print("Number of TOP directories to process: ", len(top_dir_list))
print(top_dir_list)

# counters
num_top_dirs = len(top_dir_list)
num_bottom_dirs = 0
num_files = 0

for top_dir in top_dir_list:
    
    bottom_dir_list = []

    # recupera os diretorios internos ao path argumento
    for dirs in os.listdir(top_dir):
        if (os.path.isdir(os.path.join(top_dir,dirs))):
            bottom_dir_list.append(dirs)
            #print(dirs, os.path.join(root,dirs))

    print("Number of BOTTOM directories to process: ", len(bottom_dir_list))
    print(bottom_dir_list)

    #sys.exit()

    image_files = []

    ics_file_path = None


    for directory in bottom_dir_list:
        print('Current directory: ', directory, os.path.join(top_dir,directory))
        image_files = []
        num_bottom_dirs += 1
        for curdir, dirs, files in os.walk(os.path.join(top_dir,directory)):
            ics_file_path = None
            for f in files:
                #print(f)
                if f[0].isalpha():
                    if f.endswith('.ics'):
                        # ics is tuple of (full_file_path,file_name)
                        ics_file_path = os.path.join(directory, curdir, f)

                    if f.endswith('.LJPEG'):
                        file_path = os.path.join(directory, curdir, f)
                        image_files.append(file_path)

        if (ics_file_path == None):
            continue

        ics_dict = get_ics_info(ics_file_path)
        #print (ics_dict)

        # cria diretorio de saida
        type_name = top_dir.split("/")[-1]
        if (not os.path.isdir(os.path.join(outpath,type_name,directory+'_png'))):
            print('Creating dir: ', directory+'_png')
            os.makedirs(os.path.join(outpath,type_name,directory+'_png'), exist_ok=True)
            
        outdir = os.path.join(outpath,type_name,directory+'_png')

        print ("Files: ", image_files)

        # loop nas imagens para descompressao do LJPEG em LJPEG.1
        for image_file in image_files:
            image_type = image_file.split('.')
            #print(image_type[1], ics_dict[image_type[1]]['height'])
            if os.path.exists(image_file + '.1'):
                print("image ", image_file, " ja descomprimida." )
                # descomprime com executavel jpeg
            else:
                call_lst = ['./jpegdir/jpeg', '-d', '-s',  image_file]
                #'-ql', '12', 
                # p 12
                call(call_lst)

        # Obter as imagens LJPEG.1 (descomprimidas)
        image_files_dec = []
        for curdir, dirs, files in os.walk(os.path.join(top_dir,directory)):
            for f in files:
                if f.endswith('.LJPEG.1'):
                    file_path = os.path.join(directory, curdir, f)
                    #print(file_path)
                    image_files_dec.append(file_path)

        # para cada imagem descomprimida cria PNG
        for image_file in image_files_dec:
            image_type = image_file.split('.')
            print('Convertendo: ', image_type[1], ics_dict[image_type[1]]['height'], image_type[2], image_type[3])
            
            im = np.fromfile(image_file, dtype=np.uint16)
            im.shape = (ics_dict[image_type[1]]['height'], ics_dict[image_type[1]]['width'])
            im_raw = im.byteswap()

            # convert to optical density
            im_od = od_correct(im_raw)
            if (bpp == 8):
                im = np.interp(im_od, (0.0, 4.0), (255, 0))
                im = im.astype(np.uint8)
            elif (bpp == 16):
                im = np.interp(im_od, (0.0, 4.0), (65535, 0))
                im = im.astype(np.uint16)

            print(im.shape, im.dtype)

            num_files += 1

            print('Escrevendo: ', outdir+'/'+ics_dict['patient_id']+'_'+image_type[1]+".png")
            cv2.imwrite(outdir+'/'+ics_dict['patient_id']+'_'+image_type[1]+".png", im)

        # apagar as imagens LJPEG.1 
        for image_file in image_files_dec:
            print('Apagando: ', image_file)
            os.remove(image_file)


print("Resultado processamento: ")
print("Top level dirs: ", num_top_dirs, " Bottom dirs: ", num_bottom_dirs, " files: ", num_files)

# Resultado processamento:
# benign_without_callbacks
# Top level dirs:  2  Bottom dirs:  141  files:  564
# benigns
# Top level dirs:  14  Bottom dirs:  855(853..)  files:  3415 (3412..)
# cancers
# Top level dirs:  15  Bottom dirs:  914  files:  3656
# normals
# Top level dirs:  12  Bottom dirs:  695  files:  2780

# total 10.412 imagens e 2603 exames

