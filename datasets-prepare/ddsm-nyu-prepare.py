# ddsm-nyu-prepare.py - preparar labels DDSM para rodar com NYU ou outras redes
#              considera casos {left, right}, {normal, malignant, benign} de cada patient_id
#              e indica com '1' na tabela quando eh positivo (malign or benign)
#
#  DGPP - 23/07/2019
#
# Versao 1 - le arquivos CSV de metadados DDSM e com base no patient_id
#            cria novo CSV com labels para comparar com resultado NYU.
#          - Usa as 4 imagens CC{left, right} e MLO{left, right} para NYU
#          - csv ddsm deve ter os campos patient_id, side e pathology

# usage: 

import pandas as pd
import numpy as np
import argparse
import sys

# MAIN
ap = argparse.ArgumentParser(description='Convert ddsm csv in NYU label format.')
ap.add_argument("-f", "--file",  required = True,
     help = "csv file to convert.")

args = vars(ap.parse_args())

print(args)

if (args['file'].split('.')[-1] != 'csv'):
    print('Input must be csv file.')
    sys.exit()

# read to pandas frame
df = pd.read_csv(args['file'])

print(df.info())
#print(df.head(10))

# separa coluna de ids de pacientes
df2 = df.loc[:, ['patient_id']]

# remove duplicados para ter lista unica
df2 = df2.drop_duplicates()

# transforma em np para manipular
patients_list = np.array(df2)
full_table = np.array(df)

print(patients_list.shape, patients_list.dtype)

# cria matriz dos resultados
matrix = np.zeros((patients_list.shape[0], 4), dtype=np.int)

# cria matriz de labels
labels = np.hstack((patients_list, matrix))

print(labels.shape, full_table.shape)

# para cada patient_id
for i in range(patients_list.shape[0]):
    # procura o mesmo patient_id e obtem as caracteristicas (de side e pathology)
    for j in range(full_table.shape[0]):
        if patients_list[i][0] == full_table[j][0]:
            if (full_table[j][2] == 'LEFT') and full_table[j][11] == 'MALIGNANT':   # not matter view (mlo or cc)
                labels[i][3] = 1    # left_malignant
            elif (full_table[j][2] == 'LEFT') and full_table[j][11] == 'BENIGN':
                labels[i][1] = 1    # left_benign
            elif (full_table[j][2] == 'RIGHT') and full_table[j][11] == 'MALIGNANT':
                labels[i][4] = 1    # right_malignant
            elif (full_table[j][2] == 'RIGHT') and full_table[j][11] == 'BENIGN':
                labels[i][2] = 1    # right_benign

            #print(patients_list[i][0], full_table[j][2], full_table[j][11], labels[i]) 

header = [ 'patient_id', 'left_benign', 'right_benign',	'left_malignant', 'right_malignant' ]

df_labels = pd.DataFrame(labels, columns=header)

df_labels.info()
print(df_labels.head(10))

#export to csv
df_labels.to_csv(args['file'].split('.')[0] + '__nyu_labels.csv', index=False)


## Campos do CSV
# Patient ID: the first 7 characters of images in the case file
# Density category
# Breast: Left or Right
# View: CC or MLO
# Number of abnormality for the image (This is necessary as there are some cases containing multiple abnormalities.
# Mass shape (when applicable)
# Mass margin (when applicable)
# Calcification type (when applicable)
# Calcification distribution (when applicable)
# BI-RADS assessment
# Pathology: Benign, Benign without call-back, or Malignant
# Subtlety rating: Radiologists’ rating of difficulty in viewing the abnormality in the image
# Path to image files