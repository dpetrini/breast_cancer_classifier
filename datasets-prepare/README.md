# Projeto POLI-FMUSP usando rede NYU e datasets públicos de mamografia.

Scripts python para manipulação de datasets. Esse diretório não existe no NYU github original. 

DDSM(2000) -> converter JPEG lossless para PNG.

Para criar dataset PNG com base nas imagens JPEG lossless DDSM(2000) executar script `ddsm_convert.py`. Mais instruções no próprio script. Exemplo:

```bash
python3 ddsm_convert.py -p '/Volumes/DGPP02_1TB/usp/DDSM/benigns' -o '/Volumes/DGPP02_1TB/usp/DDSM-new-version/benigns'
```

Mais atualizações com o tempo.
etc, etc