#
#
# Script exclusivo para testar modelos com transfer learning da rede NYU (para modelos gerados com train_model.py)
# 
# Rodar usando script test_usp:
#
# python3 ./test_usp.py  -d 'test_inbreast_36/' -t 'test_inbreast_36_dest/' -r 'test_inbreast_36_dest/'
#
# --> Roda amostragem estatistica - k_folds & N runs
# 
#
# Colocar o modelo a ser usado hardcoded no arquivo run_test.sh
#
import argparse
import collections as col
import numpy as np
import os
import pandas as pd
import torch
import tqdm
import sys
import matplotlib.pyplot as plt
import time
import datetime
import random


import src.utilities.pickling as pickling
import src.utilities.tools as tools
import src.modeling.models as models
import src.data_loading.loading as loading
from src.constants import VIEWS, VIEWANGLES, LABELS, MODELMODES

import src.modeling.layers as layers

import torch #, torchvision
#from torchvision import datasets, models, transforms
import torch.nn as nn
import torch.optim as optim
from torchsummary import summary
from torch.autograd import Variable
import torch.nn.functional as F

from sklearn.metrics import roc_auc_score, auc

import cv2

epochs = 1

DATASET = 'ICESP'

if DATASET == 'DDSM':
    label_file = 'labels/ddsm_description_cases_ALL.csv'
elif DATASET == 'INBREAST':
    label_file = 'labels/inbreast_4_images_cases_ALL.csv'
elif DATASET == 'ICESP':
    label_file = 'labels/relacao_diagnostico_lado_tipolesao_ate226.csv'


QUIET = True           # Display messages per batch

def load_model(parameters):
    """
    Loads trained cancer classifier
    """
    input_channels = 3 if parameters["use_heatmaps"] else 1

    print('----------> using: ', input_channels, 'input channels')
    print('Model: ', parameters["model_path"])

    model_class = {
        MODELMODES.VIEW_SPLIT: models.SplitBreastModel,
        MODELMODES.IMAGE: models.ImageBreastModel,
    }[parameters["model_mode"]]


    # select model, original (NYU) or custom (trained here)
    model = model_class(input_channels)
    if (parameters["model_path"]).endswith('.p'):
        model.load_state_dict(torch.load(parameters["model_path"])["model"]) # NYU model 
    elif (parameters["model_path"]).endswith('.pt'):
        model.load_state_dict(torch.load(parameters["model_path"]))           # custom


    if (parameters["device_type"] == "gpu") and torch.has_cudnn:
        device = torch.device("cuda:{}".format(parameters["gpu_number"]))
        print('GPU')
    else:
        device = torch.device("cpu")

 
    model = model.to(device)

    #model.eval()
    return model, device


# carrega 2
def load_exam_images_heatmaps(datum, parameters, loaded_image_dict, loaded_heatmaps_dict):

    image_extension = ".hdf5" if parameters["use_hdf5"] else ".png"

    for view in VIEWS.LIST:
        for short_file_path in datum[view]:
            #print(short_file_path)
            loaded_image = loading.load_image(
                image_path=os.path.join(parameters["image_path"], short_file_path + image_extension),
                view=view,
                horizontal_flip=datum["horizontal_flip"],
            )

            if parameters["use_heatmaps"]:
                loaded_heatmaps = loading.load_heatmaps(
                    benign_heatmap_path=os.path.join(parameters["heatmaps_path"], "heatmap_benign",
                                                     short_file_path + ".hdf5"),
                    malignant_heatmap_path=os.path.join(parameters["heatmaps_path"], "heatmap_malignant",
                                                        short_file_path + ".hdf5"),
                    view=view,
                    horizontal_flip=datum["horizontal_flip"],
                )
            else:
                loaded_heatmaps = None

            #loaded_heatmaps = None # revert for heatmaps

            loaded_image_dict[view].append(loaded_image)  #numpy float32
            loaded_heatmaps_dict[view].append(loaded_heatmaps)
    #print('loaded_image_dict ', loaded_image_dict)

    return loaded_image_dict, loaded_heatmaps_dict

# global para compartilhar
random_number_generator = np.random.RandomState(0)  #(parameters["seed"])


# generator de imagens augmentadas
# retorna batch_dict no formato esperado pela rede NYU
def load_data_batch(datum, parameters,  #data_batch, #batch_dict,
                                loaded_image_dict, loaded_heatmaps_dict):

    batch_dict = {view: [] for view in VIEWS.LIST}
    i = 0

    #for d in data_batch:
    while(True):
        i = i + 1
        #print ('data_batch d: ', i)
        for view in VIEWS.LIST:
            image_index = 0
            if parameters["augmentation"]:
                image_index = random_number_generator.randint(low=0, high=len(datum[view]))
                #print('image_index',image_index, len(datum[view]))
                image=loaded_image_dict[view][image_index]
                #print(view, image.mean())
            cropped_image, cropped_heatmaps = loading.augment_and_normalize_image(
                image=image, #loaded_image_dict[view][image_index],
                auxiliary_image=loaded_heatmaps_dict[view][image_index],
                view=view,
                best_center=datum["best_center"][view][image_index],
                random_number_generator=random_number_generator,
                augmentation=parameters["augmentation"],
                max_crop_noise=parameters["max_crop_noise"],
                max_crop_size_noise=parameters["max_crop_size_noise"],
            )

            if loaded_heatmaps_dict[view][image_index] is None:
                batch_dict[view].append(cropped_image[:, :, np.newaxis])
            else:
                batch_dict[view].append(np.concatenate([
                    cropped_image[:, :, np.newaxis],
                    cropped_heatmaps,
                ], axis=2))

        # used as generator
        yield batch_dict


# get exam number from filename (ICESP)
def get_id_from_filename(image):
    folder = image[0:3]
    if not folder.isdigit():
        folder = image[0:2]
        if not folder.isdigit():
            folder = image[0:1]
            if not folder.isdigit():
                print('Not a valid filename', image)
                sys.exit()
    return folder

# count malignants
count_mal = 0

def read_label(df, exam, mini_bs, print_tensors):
    """
    Returns tensor with labels (ground truth) of current exam.
    Expects csv read in df, current exam, size of mini-batch.
    """
    global count_mal

    # Carrega labels padrao deste exame do csv
    x = exam['L-CC'][0]

    if DATASET == 'DDSM':
        search_str = x[:6]
    elif DATASET == 'INBREAST':
        search_str = x[9:25]
    elif DATASET == 'ICESP':
        search_str = get_id_from_filename(x)

    if DATASET == 'ICESP':
        label_row = df.loc[df['patient_id'] == int(search_str)].values
    else:
        label_row = df[df['patient_id'].str.contains(search_str)].values

    if len(label_row) > 0:
        labels = torch.tensor(np.array(np.array(label_row[0, 1:5], dtype=np.uint8)), dtype=torch.float)
    else:
        if not QUIET:
            print('Label not found, using all [0, 0, 0, 0], exam: ', x[:6])
        labels = torch.tensor(np.array([0, 0, 0, 0]), dtype=torch.float)

    # replica label por mini_bs vezes
    labels=labels.view(-1, 1).repeat(1, mini_bs)
    
    if print_tensors:
        print('Labels ', labels, labels.size(), labels.dim())

    ###### Count malignants
    if labels[2] == 1 or labels[3] == 1:
        count_mal += 1


    return labels

#Accuracy
def calc_acc(y_hat, labels, mini_bs, print_tensors):
    #print(y_hat)
    y_hat = (y_hat>0.5).float()     # acima de 0.5 consideramos 1
    correct = torch.eq(y_hat, labels).float().sum()
    if print_tensors:
        print('y_hat for acc (thr=0.5): ', y_hat)
        print ('correct', correct)
    acc = correct/(y_hat.shape[0]*mini_bs)

    return acc

# 
def test_model(model, device, exam_list, selection, parameters):

    # tamanho do mini-batch (quant. de exames augmentados)
    mini_bs = 1

    # batch-size == quantidade de imagens
    batch_size = len(exam_list)
    batch_number = 0    # contador
    validation_data_size = batch_size
    print_tensors = False  # debug

    # read labels file
    df = pd.read_csv(label_file)

    print('Testing: Epochs: {:03d}, Batch size: {:03d}, mini-batch: {:03d}.'.format(epochs, batch_size, mini_bs))


    start = time.time()
    history = []

    for epoch in range(epochs):

        epoch_start = time.time()

        # loss and acc for the epoch
        validation_loss = 0.0
        validation_acc = 0.0
        y_hat_val_auc = []
        label_val_auc = []

        print('Size of testset: ', len(selection))

        # esse eh o loop principal de batches que rodam por epoch
        #for datum in exam_list:      # Para cada exame - dataloader , tqdm depois ...
        for sel in selection:      # Para cada exame - dataloader , tqdm depois ...

            datum = exam_list[sel]

            batch_number = batch_number + 1

            loaded_image_dict = {view: [] for view in VIEWS.LIST}
            loaded_heatmaps_dict = {view: [] for view in VIEWS.LIST}

            # carrega as 4 imagens do exame
            loaded_image_dict, loaded_heatmaps_dict = load_exam_images_heatmaps(datum, parameters,
                                                        loaded_image_dict, loaded_heatmaps_dict)

            # a rede NYU estah neste formato de entrada
            batch_dict = {view: [] for view in VIEWS.LIST}

            # Carrega o mini-batch, mini_bs * n_images (n_images sempre 4 == exame)
            # 4 imagens augmentadas (e heatmaps) colocadas no tensor no device
            bs_count = 0
            for batch_dict in load_data_batch(datum, parameters, loaded_image_dict, loaded_heatmaps_dict):
                tensor_batch = {
                    view: torch.tensor(np.stack(batch_dict[view])).permute(0, 3, 1, 2).to(device)
                    #view: print(batch_dict[view])
                    for view in VIEWS.LIST
                }
                bs_count = bs_count + 1
                if bs_count == mini_bs:
                    break

            # Carrega labels padrao deste exame do csv
            labels = read_label(df, datum, mini_bs, print_tensors)
            labels = labels.to(device)

            with torch.no_grad():       # test - no gradient tracking needed

                model.eval()
                output = model(tensor_batch)        # forward pass                   
                y_hat = compute_y_hat(output)       # make output to compatible with labels               

                #loss_val = loss_func(y_hat, labels) # compute loss

                loss_val = compute_loss(output, labels)

                # compute the total loss for the batch and add it to train_loss
                validation_loss += loss_val.item() * mini_bs # multiplica para fazer media geral depois

                #Accuracy
                acc = calc_acc(y_hat, labels, mini_bs, print_tensors)

                validation_acc +=acc.item() * mini_bs # como eh media, mul bs size para media final depois

                if not QUIET:
                    print('Batch number: {:03d}, Validation Loss: {:.4f}, Accuracy: {:.4}'.format(batch_number, loss_val.item(), acc))
                else:
                    print('|', end=''); sys.stdout.flush()

                # guarda Y-hat/label para AUC - VAL
                label_val_auc = np.append(label_val_auc, labels.cpu().detach().numpy())
                y_hat_val_auc = np.append(y_hat_val_auc, y_hat.cpu().detach().numpy())


        batch_number = 0

        # find average training loss and validation acc
        avg_validation_acc  = validation_acc/validation_data_size 
        avg_validation_loss = validation_loss/validation_data_size


        # # calculate AUC
        # if (np.count_nonzero(label_val_auc)):
        #     auc_val_ = roc_auc_score(label_val_auc.ravel(), y_hat_val_auc.ravel())
        # else:
        #     auc_val_ = -1

        # print('\nAUC Test: {:.3f}'.format(auc_val_))

        # # # calculate AUC (separate benign/malignant) 
        # label_auc = np.array(label_val_auc.reshape((-1,4)))
        # y_hat_auc = np.array(y_hat_val_auc.reshape((-1,4)))
        # print('Sizes: exam_list: ', len(exam_list),  ' label: ', label_auc.shape, ' y_hat: ', y_hat_auc.shape, ' DB: ', DATASET)
        # if (np.count_nonzero(label_auc[:, 0:2])):  # happens in some tests...
        #         auc_ = roc_auc_score(label_auc[:, 0:2].ravel(), y_hat_auc[:, 0:2].ravel())
        #         print('AUC Benign: %.3f' % auc_)
        # else:
        #         print('[AUC Benign] Only zero labels')
        # auc_ = roc_auc_score(label_auc[:, 2:5].ravel(), y_hat_auc[:, 2:5].ravel())
        # print('AUC Malignant: %.3f' % auc_)
        

        # calculate AUC VAL
        label_auc = np.array(label_val_auc.reshape((-1,4)))
        y_hat_auc = np.array(y_hat_val_auc.reshape((-1,4)))
        if (np.count_nonzero(label_auc[:, 0:2])):  # happens in some tests...
                auc_ben_val = roc_auc_score(label_auc[:, 0:2].ravel(), y_hat_auc[:, 0:2].ravel())
                print('\n[Test] AUC Benign: %.3f' % auc_ben_val, end='')
        else:
                print('[Test] AUC Benign Only zero labels', end='')
                auc_ben_val = 0
        if (np.count_nonzero(label_auc[:, 2:5])):  # happens in some tests...
                auc_mal_val = roc_auc_score(label_auc[:, 2:5].ravel(), y_hat_auc[:, 2:5].ravel())
        else:
                auc_mal_val = 0
        print(' AUC Malignant: %.3f' % auc_mal_val)



        history.append([avg_validation_loss, avg_validation_acc, auc_ben_val, auc_mal_val])

        epoch_end = time.time()
        print('Epoch: {:03d}, Validation: Loss: {:0.4f}, Acc: {:.2f}%, Time: {:.4f}s'.format(epoch, 
                                        avg_validation_loss, 
                                        avg_validation_acc*100, epoch_end-epoch_start))


    print('Total test time:  {:.4f}s'.format(time.time()-start))

    return history


def compute_loss(output, labels):

    loss_func =  nn.BCELoss()
    output_cat = torch.cat([output["CC"], output["MLO"]], dim=1)

    # como a saida aqui eh logits (sem probabilidade), devemos aplicar sigmoid 
    #  para ter probs antes do BCE_Loss que espera probs
    output_cat = torch.sigmoid(output_cat)
    
    #output_cat = torch.exp(output_cat)

    #print(labels, output_cat)

    loss =  loss_func(output_cat[:,3,1], labels[3]) + \
            loss_func(output_cat[:,7,1], labels[3]) + \
            loss_func(output_cat[:,1,1], labels[1]) + \
            loss_func(output_cat[:,5,1], labels[1]) + \
            loss_func(output_cat[:,2,1], labels[2]) + \
            loss_func(output_cat[:,6,1], labels[2]) + \
            loss_func(output_cat[:,0,1], labels[0]) + \
            loss_func(output_cat[:,4,1], labels[0])

    return loss



def compute_y_hat(output, print_tensors=False):
    """
    Format output predictions
    """

    # tensor de saida eh um dicionario com as Split Breast Model Views
    # vamos concatenar para ter um tensor unico e fazer as mesmas 
    # operacoes que sao realizadas na predicao original (test)
    output_cat = torch.cat([output["CC"], output["MLO"]], dim=1)

    # etapa realizada na predicao: gera probs complementadas (soma 1 por linha)
    # equivalente ao softmax??
    #exp1 = torch.exp(output_cat)
    
    exp1 = torch.sigmoid(output_cat)

    # Realiza o calculo de cada probabilidade : MEDIA, deixando explicito para conferencia
    sum = torch.add(exp1[:, 0, 1], exp1[:, 4, 1])   #  grad_fn=<AddBackward0>
    left_benign_y_hat = torch.div(sum, 2)           # grad_fn=<DivBackward0>)
    sum = torch.add(exp1[:, 1, 1], exp1[:, 5, 1])
    right_benign_y_hat = torch.div(sum, 2)
    sum = torch.add(exp1[:, 2, 1], exp1[:, 6, 1])
    left_malign_y_hat = torch.div(sum, 2)
    sum = torch.add(exp1[:, 3, 1], exp1[:, 7, 1])
    right_malign_y_hat = torch.div(sum, 2)

    # concatena as saida individuais para o Loss
    y_hat = torch.stack([left_benign_y_hat, right_benign_y_hat, left_malign_y_hat, right_malign_y_hat])
    
    if print_tensors:
        print('Y_hat ', y_hat, y_hat.size(), y_hat.dim())

    return y_hat



def load_train_save(data_path, output_path, parameters):
    """
    Outputs the predictions as csv file
    """
    exam_list = pickling.unpickle_from_file(data_path)
    model, device = load_model(parameters)

    # original, if want simple test ( k==> 1)
    #trained_model, history, size = test_model(model, device, exam_list, parameters)

    #global count_mal
    histories = []
    history = []

    k_folds = 1
    runs = 1

    for r in range(runs):
        print('Run # ', r+1)
        # create multiple k_folds subsets
        indices = list(range(len(exam_list)))
        random.shuffle(indices)
        size = len(indices)
        subset_size = round(size / k_folds)
        subsets = [indices[x:x+subset_size] for x in range(0, len(indices), subset_size)]

        for k in range(k_folds):
            history = test_model(model, device, exam_list, subsets[k], parameters)
            histories.append(history)

    print('Malignant #: ', count_mal/runs, ' normal#: ', len(exam_list)-count_mal/runs)

    means = np.asarray(histories, dtype = np.float32)
    stds = np.asarray(histories, dtype = np.float32)
 
    means = np.mean(means, axis=0)
    stds = np.std(stds, axis=0)

    print(histories)
    print(means, stds)
    print('Mean AUC: {:.3f} std: {:.3f}'.format(means[0][3], stds[0][3]))



def main():
    parser = argparse.ArgumentParser(description='Run image-only model or image+heatmap model')
    parser.add_argument('--model-mode', default=MODELMODES.VIEW_SPLIT, type=str)
    parser.add_argument('--model-path', required=True)
    parser.add_argument('--data-path', required=True)
    parser.add_argument('--image-path', required=True)
    parser.add_argument('--output-path', required=True)
    parser.add_argument('--batch-size', default=1, type=int)
    parser.add_argument('--seed', default=0, type=int)
    parser.add_argument('--use-heatmaps', action="store_true") # store_true
    parser.add_argument('--heatmaps-path')
    parser.add_argument('--use-augmentation', action="store_true")
    parser.add_argument('--use-hdf5', action="store_true")
    parser.add_argument('--num-epochs', default=1, type=int)
    parser.add_argument('--device-type', default="cpu", choices=['gpu', 'cpu'])
    parser.add_argument("--gpu-number", type=int, default=0)
    args = parser.parse_args()

    parameters = {
        "device_type": args.device_type,
        "gpu_number": args.gpu_number,
        "max_crop_noise": (100, 100),
        "max_crop_size_noise": 100,
        "image_path": args.image_path,
        "batch_size": args.batch_size,
        "seed": args.seed,
        "augmentation": args.use_augmentation,
        "num_epochs": args.num_epochs,
        "use_heatmaps": args.use_heatmaps,
        "heatmaps_path": args.heatmaps_path,
        "use_hdf5": args.use_hdf5,
        "model_mode": args.model_mode,
        "model_path": args.model_path,
    }
    #load_run_save(
    load_train_save(
        data_path=args.data_path,
        output_path=args.output_path,
        parameters=parameters,
    )


if __name__ == "__main__":
    main()


# run for paper
# [[[2.6425048340769375, 0.39889705882352944, 0.44706911636045493, 0.8543478260869566]], 
# [[2.3796006265808556, 0.39705882352941174, 0.5818181818181818, 0.914762516046213]],
# [[2.3914281600976692, 0.4117647058823529, 0.5846153846153846, 0.8779128672745694]], 
# [[2.5968269136022117, 0.38419117647058826, 0.5127118644067796, 0.8874236874236875]], 
# [[2.4474794432959137, 0.4007352941176471, 0.4756363636363637, 0.8907226806701675]], 
# [[2.555238093523418, 0.3952205882352941, 0.6022514071294559, 0.8609189723320159]], 
# [[2.376209661802825, 0.40625, 0.5074365704286964, 0.8705673758865248]], 
# [[2.636576681233504, 0.3897058823529412, 0.5542699724517907, 0.873992673992674]], 
# [[2.446447518599384, 0.39705882352941174, 0.551747311827957, 0.8799407114624506]], 
# [[2.584290782756665, 0.39889705882352944, 0.5228494623655914, 0.8699674918729683]], 
# [[2.743329643326647, 0.3915441176470588, 0.5465564738292011, 0.8544960474308301]], 
# [[2.258248693145373, 0.40441176470588236, 0.5231846019247594, 0.9019754938734683]], 
# [[2.466687975780052, 0.39889705882352944, 0.4929742388758782, 0.8562259306803595]], 
# [[2.601190578849877, 0.39705882352941174, 0.596031746031746, 0.8693236714975845]], 
# [[2.2081670741386272, 0.40808823529411764, 0.5359911406423035, 0.9199799949987496]], 
# [[2.8251182301079525, 0.38786764705882354, 0.5224913494809689, 0.820405138339921]], 
# [[2.8280524140333427, 0.3805147058823529, 0.4864583333333333, 0.8782547501759325]], 
# [[2.16645517265972, 0.41544117647058826, 0.6142578125, 0.8681525241675618]], 
# [[2.5398722979075767, 0.38786764705882354, 0.5282485875706215, 0.9332333083270817]], 
# [[2.453516450655811, 0.40808823529411764, 0.608974358974359, 0.8354743083003953]]]
# [[2.5073621  0.39797792 0.53977877 0.8759039 ]]
#  [[0.1798797  0.00905226 0.04588518 0.02666214]]
# Mean AUC: 0.876 std: 0.027


#Saida dos tensores e predicao
# CC tensor([[[-0.7802, -0.6130],
#          [-0.5453, -0.8666],
#          [-0.7454, -0.6435],
#          [-0.7731, -0.6191]]], device='cuda:1', grad_fn=<LogSoftmaxBackward>)
# MLO tensor([[[-0.6542, -0.7337],
#          [-0.6384, -0.7511],
#          [-0.8660, -0.5458],
#          [-0.7389, -0.6494]]], device='cuda:1', grad_fn=<LogSoftmaxBackward>)

# Aqui tira a exponencial e dá valor complementar para cada linha correspondente a acima

# BP:  OrderedDict([(('left_benign', 'CC'), array([[0.4582997, 0.5417003]], dtype=float32)), 
# (('left_benign', 'MLO'), array([[0.5198763 , 0.48012376]], dtype=float32)), 
# (('right_benign', 'CC'), array([[0.5796419 , 0.42035815]], dtype=float32)),
#  (('right_benign', 'MLO'), array([[0.5281484 , 0.47185153]], dtype=float32)),
#  (('left_malignant', 'CC'), array([[0.474555  , 0.52544504]], dtype=float32)), 
# (('left_malignant', 'MLO'), array([[0.42063877, 0.57936126]], dtype=float32)), 
# (('right_malignant', 'CC'), array([[0.46159303, 0.53840697]], dtype=float32)), 
# (('right_malignant', 'MLO'), array([[0.47763485, 0.5223651 ]], dtype=float32))])

# Aqui pega apenas a segunda prob de cada linha acima 

# pred_df label      left_benign           right_benign           left_malignant           right_malignant          
# view_angle          CC       MLO           CC       MLO             CC       MLO              CC       MLO
# 0               0.5417  0.480124     0.420358  0.471852       0.525445  0.579361        0.538407  0.522365
# predictions [[0.51091206 0.44610482 0.55240315 0.53038603]]
# 100%|████████████████████████████████████████████████████████████████████████████████████████| 1/1 [00:06<00:00,  6.11s/it]
# [array([0.51133454, 0.44542885, 0.55090743, 0.53175557], dtype=float32)]

            # esse print referencia abaixo eh para um mini-batch mini_bs=1

            #     # forward pass
            #     output = model(tensor_batch)

            #     # print output
            #     # for k, v in output.items():
            #     #     print(k, v)

            #     # CC tensor([[[-0.4861, -0.9545],
            #     # [-0.6781, -0.7084],
            #     # [-0.6533, -0.7347],
            #     # [-0.7036, -0.6828]]], device='cuda:1', grad_fn=<LogSoftmaxBackward>)
            #     # MLO tensor([[[-0.7638, -0.6272],
            #     # [-0.9216, -0.5073],
            #     # [-0.7954, -0.6004],
            #     # [-0.7172, -0.6696]]], device='cuda:1', grad_fn=<LogSoftmaxBackward>)

            #     # tensor de saida eh um dicionario com as Views
            #     # vamos concatenar para ter um tensor unico e fazer as mesmas operacoes que
            #     # que sao realizadas na predicao original (test)
            #     output_cat = torch.cat([output["CC"], output["MLO"]], dim=1)
                
            #     #print(output_cat)
            #     # tensor([[[-0.7268, -0.6606],
            #     # [-0.8160, -0.5838],
            #     # [-0.6278, -0.7631],
            #     # [-0.4835, -0.9588],
            #     # [-0.8818, -0.5345],
            #     # [-0.7464, -0.6426],
            #     # [-0.6236, -0.7679],
            #     # [-0.5358, -0.8799]]], device='cuda:1', grad_fn=<CatBackward>)
                
            #     exp1 = torch.exp(output_cat)
                
            #     #print ('exp1: ', exp1)
            #     # exp1:  tensor([[[0.4834, 0.5166],
            #     # [0.4422, 0.5578],
            #     # [0.5338, 0.4662],
            #     # [0.6166, 0.3833],
            #     # [0.4140, 0.5860],
            #     # [0.4741, 0.5259],
            #     # [0.5360, 0.4640],
            #     # [0.5852, 0.4148]]], device='cuda:1', grad_fn=<ExpBackward>)

            #     # Realiza o calculo de cada probabilidade 
            #     left_benign_y_hat = torch.div(torch.add(exp1[0][0][1], exp1[0][4][1]), 2)
            #     right_benign_y_hat = torch.div(torch.add(exp1[0][1][1], exp1[0][5][1]), 2)
            #     left_malign_y_hat = torch.div(torch.add(exp1[0][2][1], exp1[0][6][1]), 2)
            #     right_malign_y_hat = torch.div(torch.add(exp1[0][3][1], exp1[0][7][1]), 2)

            #     # print('Train preds: ', left_benign_y_hat, right_benign_y_hat, left_malign_y_hat, right_malign_y_hat)
            #     # Train preds:  tensor(0.5513, device='cuda:1', grad_fn=<DivBackward0>) 
            #     # tensor(0.5419, device='cuda:1', grad_fn=<DivBackward0>) t
            #     # ensor(0.4651, device='cuda:1', grad_fn=<DivBackward0>) 
            #     # tensor(0.3991, device='cuda:1', grad_fn=<DivBackward0>)

            #     # concatena as saida individuais para o Loss
            #     y_hat = torch.stack([left_benign_y_hat, right_benign_y_hat, left_malign_y_hat, right_malign_y_hat])
                
            #     # print(y_hat)
            #     # tensor([0.5513, 0.5419, 0.4651, 0.3991], device='cuda:1', grad_fn=<StackBackward>)
