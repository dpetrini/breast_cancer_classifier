## REmover esse arquivo - na proxima revisao - MACA esta separado


# treinar mass calcification detection 
# 
# transfer learning based in NYU model
# 
# rodar do raiz do NYU
# export PYTHONPATH=$(pwd):$PYTHONPATH
#
# DGPP 09/2019


import argparse
import numpy as np
import torch
import json

import src.modeling.models as models
#import src.data_loading.loading as loading
#import src.modeling.layers as layers

from torch.autograd import Variable
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader

import sys
import cv2
import time
import copy
import datetime
import matplotlib.pyplot as plt
from sklearn.metrics import roc_auc_score, auc, roc_curve

import src.modeling.dataset_maca as data   # load our dataset class - default = dataset

# some good models
#IMAGE_MODEL_PATH='maca/models_train/2019-10-22-16:51_35ep_180n_model_079.pt'
#IMAGE_MODEL_PATH='maca/models_train/2019-10-22-18:02_45ep_180n_model_079.pt'
IMAGE_MODEL_PATH='maca/models_train/2019-10-22-20:45_45ep_180n_model_079.pt'

# view_simple = 'L-CC'
view = 'CC'
batch_size = 1    # aumentar estah dando problema devido aos diferentes tam. de image
device = 'gpu'
#device = 'cpu'
gpu_number=1
num_epochs = 45
mini_batch = batch_size

transfer = 'FINE_TUNING' 
#transfer = 'FEATURE_EXTRACTION'

#Accuracy
def calc_acc(y_hat, labels, mini_bs, print_tensors):
    y_hat = (y_hat>0.5).float()     # acima de 0.5 consideramos 1
    correct = torch.eq(y_hat, labels).float().sum()
    if print_tensors:
        print('y_hat for acc (thr=0.5): ', y_hat, labels, y_hat.shape[1])
        print ('correct', correct)
    acc = correct/(y_hat.shape[1]*mini_bs)

    return acc

def compute_y_hat(output, print_tensors=False):
    # etapa realizada na predicao: gera probs complementadas (soma 1 por linha)
    # equivalente ao softmax??
    exp1 = torch.exp(output)
    mass_y_hat = exp1[:, 0, 1]
    calc_y_hat = exp1[:, 1, 0]

    # concatena as saida individuais para o Loss
    y_hat = torch.cat([mass_y_hat, calc_y_hat]).reshape(-1,2)  #unsqueeze(0)  #NG
    
    if print_tensors:
        print('Y_hat ', y_hat, y_hat.size(), y_hat.dim())

    return y_hat


# run model test
def run_test(model, loss_criterion,  test_dataloader, device, summary, mini_batch=5):

    '''
    Function to compute the accuracy on the test set
    Parameters
        :param model: Model to test
        :param loss_criterion: Loss Criterion to minimize
    '''
    test_acc = 0.0; test_loss = 0.0
    print_tensors = False
    batch_val_counter = 0
    y_hat_auc_test = []; label_auc_test = []    

    # Validation - No gradient tracking needed
    with torch.no_grad():

        # Set to evaluation mode
        model.eval()

        # validation loop
        for _, (inputs, labels) in enumerate(test_dataloader):

            inputs = inputs.to(device)
            labels = torch.tensor(np.array(labels), dtype=torch.float).to(device)
            
            outputs = model(inputs)                         # forward pass for validation  
            y_hat = compute_y_hat(outputs, print_tensors)   # make output to compatible with labels                
            loss = loss_criterion(y_hat, labels)            # compute loss       
            test_loss += loss.item() * inputs.size(0)       # batch total loss 

            # calculcate validation acc
            acc = calc_acc(y_hat, labels, mini_batch, print_tensors)
            # compute total accuracy in the whole batch and add to valid_acc
            test_acc += acc.item() * inputs.size(0)
            batch_val_counter += inputs.size(0)
            # guarda Y-hat/label para AUC
            label_auc_test = np.append(label_auc_test, labels.cpu().detach().numpy())
            y_hat_auc_test = np.append(y_hat_auc_test, y_hat.cpu().detach().numpy())

        # find average training loss and validation acc
        avg_test_loss = test_loss/batch_val_counter 
        avg_test_acc  = test_acc/batch_val_counter 

        # calculate AUC
        auc_test = roc_auc_score(label_auc_test.ravel(), y_hat_auc_test.ravel())
        print('AUC Test: {:.3f}'.format(auc_test))

    # Find average test loss and test accuracy
    avg_test_loss = test_loss/batch_val_counter 
    avg_test_acc = test_acc/batch_val_counter

    print("Test accuracy : " + str(avg_test_acc)+ " Test loss : " + str(avg_test_loss))

    # Compute ROC curve and ROC area for each class BENIGN (0..1)
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    fpr["micro"], tpr["micro"], thr = roc_curve(label_auc_test.ravel(), y_hat_auc_test.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    #print(fpr["micro"], tpr["micro"], thr)

    # Plot all ROC curves
    plt.figure()
    plt.plot(fpr["micro"], tpr["micro"],
            label='ROC curve (area = {0:0.2f})'
                ''.format(roc_auc["micro"]),
            color='deeppink', linestyle=':', linewidth=4)

    plt.plot([0, 1], [0, 1], 'k--', lw=2)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Mass x Calcification')
    plt.legend(loc="lower right")
    plt.savefig('maca/plot_train/'+summary+'_test_ROC.png')
    plt.show()


# def predict(model, test_image_name):
#     '''
#     Function to predict the class of a single test image
#     Parameters
#         :param model: Model to test
#         :param test_image_name: Test image

#     '''
    
#     transform = image_transforms['test']

#     test_image = Image.open(test_image_name)
#     plt.imshow(test_image)
    
#     test_image_tensor = transform(test_image)

#     if torch.cuda.is_available():
#         test_image_tensor = test_image_tensor.view(1, 3, 224, 224).cuda()
#     else:
#         test_image_tensor = test_image_tensor.view(1, 3, 224, 224)
    
#     with torch.no_grad():
#         model.eval()
#         # Model outputs log probabilities
#         out = model(test_image_tensor)
#         ps = torch.exp(out)
#         topk, topclass = ps.topk(3, dim=1)
#         for i in range(3):
#             print("Predcition", i+1, ":", idx_to_class[topclass.cpu().numpy()[0][i]], ", Score: ", topk.cpu().numpy()[0][i])


class ModelInput:
    def __init__(self, image): 
        self.image = image


def load_model(parameters):
    """
    Loads trained cancer classifier - only supporint IMAGE Type prediction
    """
    global device
    input_channels = 1 
    model = models.SingleImageBreastModel(input_channels)
    #model.load_state_dict(torch.load(parameters["model_path"], map_location=torch.device('cpu')))
    model.load_state_dict(torch.load(parameters["model_path"]))

    if (device == "gpu") and torch.has_cudnn:
        device = torch.device("cuda:{}".format(gpu_number))
    else:
        device = torch.device("cpu")

    #print('Printing the model: ')
    #print(model)
    #sys.exit()

    model = model.to(device)

    return model, device


def main():
    parser = argparse.ArgumentParser(description='Run image-only model or image+heatmap model')
#    parser.add_argument('--view', default=view_simple)
    parser.add_argument('--model-path', default=IMAGE_MODEL_PATH,)
    parser.add_argument('--batch-size', default=1, type=int)
    parser.add_argument('--use-augmentation', action="store_true")
    parser.add_argument('--num-epochs', default=1, type=int)
    parser.add_argument('--device-type', default="cpu", choices=['gpu', 'cpu'])
    parser.add_argument("--gpu-number", type=int, default=0)
    args = parser.parse_args()

    parameters = {
        "model_path": args.model_path,
        "device_type": args.device_type,
        "gpu_number": args.gpu_number,
        "batch_size": args.batch_size,
        "augmentation": args.use_augmentation,
        "num_epochs": args.num_epochs,
    }


    # Dataset = data clean
    test_image_paths = 'data_clean/test'  # 'data_500/test' 'data/test'

    # classe dataset que carregar arquivos e faz transformacoes
    mammo_dataset_test = data.MyDataset(test_image_paths, view=view, train=False)

    n_samples = len(mammo_dataset_test)
    print('Size test:', n_samples)

    # test dataloader
    test_dataloader = DataLoader(mammo_dataset_test, batch_size=batch_size, shuffle=True, num_workers=1)

    model, device = load_model(parameters)

    # Loss function
    loss_func = nn.BCELoss() #nn.CrossEntropyLoss()

    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d-%H:%M')

    summary = str(st)+'_'+str(num_epochs)+'ep_'+str(n_samples)+'n_test'

    # run model test
    run_test(model, loss_func,  test_dataloader, device, summary, mini_batch)


if __name__ == "__main__":
    main()

