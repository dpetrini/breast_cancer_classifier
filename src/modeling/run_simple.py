# Roda NYU single de maneira simples 

# rodar do raiz do NYU
# export PYTHONPATH=$(pwd):$PYTHONPATH

"""
Runs the image only IMAGE model (no image+heatmaps model) for breast cancer prediction.
"""
import argparse
import numpy as np
import torch
import json

import src.modeling.models as models
import src.data_loading.loading as loading

from torch.utils.data import DataLoader

import cv2

import src.modeling.dataset_simple as data   # load our dataset class - default = dataset


IMAGE_MODEL_PATH='models/ImageOnly__ModeImage_weights.p'

cropped_mammogram_path='sample_single_output/cropped.png'
metadata_path='sample_single_output/cropped_metadata.pkl'

view_simple = 'L-CC'

epochs = 2
batch_size = 2
device = 'gpu'
gpu_number=1


train_image_paths =  'data/train/image' #'data/train/image'
train_target_paths = 'data/train/label' #'data/train/label'

validation_image_paths = 'data/validation/image'
validation_target_paths = 'data/validation/label_full'

test_image_paths = 'data/test/image'
test_target_paths = 'data/test/label'

# classe dataset que carregar arquivos e faz transformacoes
#mammo_dataset_train = data.MyDataset(train_image_paths, train_target_paths)
#mammo_dataset_validation = data.MyDataset(validation_image_paths, validation_target_paths)
mammo_dataset_test = data.MyDataset(test_image_paths, test_target_paths)

print('Size train:', len(mammo_dataset_test), ' Size val: ', len(mammo_dataset_test))

# DataLoader eh iterador que retorna tensores pytorch
#train_dataloader = DataLoader(mammo_dataset_train, batch_size=batch_size, shuffle=True, num_workers=1)
#validation_dataloader = DataLoader(mammo_dataset_validation, batch_size=batch_size, shuffle=True, num_workers=1)
test_dataloader = DataLoader(mammo_dataset_test, batch_size=batch_size, shuffle=True, num_workers=1)



def train_and_validate(model, train_dataloader, validation_dataloader, loss_criterion, optimizer, mini_batch=5, epochs=25):

    start = time.time()
    history = []
    batch_counter = 0
    batch_val_counter = 0

    for epoch in range(epochs):
        epoch_start = time.time()
        print('Epoch: {}/{}'.format(epoch+1, epochs))

        # set to training mode
        model.train()

        #loss and acc for the epoch
        train_loss = 0.0
        train_acc = 0.0
        validation_loss = 0.0
        validation_acc = 0.0

        for _, (inputs, labels) in enumerate(train_dataloader):  # no iterator com tam=bs

            inputs = Variable(inputs.cuda())
            labels = Variable(labels.cuda())
            #labels = labels.view(-1, labels.shape[2], labels.shape[3]) # proper format for loss

            optimizer.zero_grad()                   # clean existing gradients
            outputs = model(inputs)                 # forward pass  

            # outputs = inputs.view(-1, outputs.shape[2], outputs.shape[3])
            # print(outputs.shape, labels.shape)         
            loss = loss_criterion(outputs, labels)  # compute loss
            loss.backward()                         # backprop the gradients
            optimizer.step()                        # update parameters

            # compute the total loss for the batch and add it to train_loss
            train_loss += loss.item() * inputs.size(0)

            #print(labels.shape, outputs.shape, inputs.shape, inputs.size(0))
            #print(outputs)

            # compute the accuracy

            preds = torch.argmax(outputs, dim=1)    # pega os valores que sao maximos ()
            #ret, predictions = torch.max(outputs.data, 1)
            correct_counts = preds.eq(labels.data.view_as(preds)) #predictions

            # convert correct_counts to float then compute the mean
            acc = torch.mean(correct_counts.type(torch.FloatTensor))

            # compute total accuracy in the whole batch and add to train_acc
            train_acc += acc.item() * inputs.size(0)

            batch_counter += inputs.size(0)

            #print('Batch number: {:03d}, Training Loss: {:.4f}, Accuracy: {:.4}'.format(batch, loss.item(), acc))

        
        # validation - no gradient tracking needed
        with torch.no_grad():
            
            # set to evaluation mode
            model.eval()

            # validation loop
            for _, (inputs, labels) in enumerate(validation_dataloader):

                inputs = Variable(inputs.cuda())
                labels = Variable(labels.cuda())
                #labels = labels.view(-1, labels.shape[2], labels.shape[3])
                
                outputs = model(inputs)                 # forward pass for validation             
                loss = loss_criterion(outputs, labels)	# compute loss
        
                validation_loss += loss.item() * inputs.size(0) # batch total loss 

                # calculcate validation acc
                preds = torch.argmax(outputs, dim=1)#.float()
                correct_counts = preds.eq(labels.data.view_as(preds))

                # convert correct counts to float and then compute the mean
                acc = torch.mean(correct_counts.type(torch.FloatTensor))

                # compute total accuracy in the whole batch and addd to valid_acc
                validation_acc += acc.item() * inputs.size(0)

                batch_val_counter += inputs.size(0)
            
                #print('Batch number: {:03d}, Validation Loss: {:.4f}, Accuracy: {:.4}'.format(j, loss.item(), acc))

        # fing average training loss and training accuracy
        avg_train_loss = train_loss/batch_counter 
        avg_train_acc  = train_acc/batch_counter

        # find average training loss and validation acc
        avg_validation_loss = validation_loss/batch_val_counter 
        avg_validation_acc  = validation_acc/batch_val_counter 

        batch_counter = 0
        batch_val_counter = 0

        history.append([avg_train_loss, avg_validation_loss, avg_train_acc, avg_validation_acc])

        epoch_end = time.time()
        print('Epoch: {:03d}, Training: Loss: {:.4f}, Acc: {:.2f}%, Validation: Loss: {:0.4f}, Acc: {:.2f}%, Time: {:.4f}s'.format(epoch+1, 
                                        avg_train_loss, avg_train_acc*100, avg_validation_loss, 
                                        avg_validation_acc*100, epoch_end-epoch_start))

    print('Total training time:  {:.4f}s'.format(time.time()-start))
        
    return model, history


class ModelInput:
    def __init__(self, image): 
        self.image = image


def load_model(parameters):
    """
    Loads trained cancer classifier - only supporint IMAGE Type prediction
    """
    global device
    input_channels = 1 
    model = models.SingleImageBreastModel(input_channels)
    model.load_state_from_shared_weights(
        state_dict=torch.load(parameters["model_path"])["model"], #
        view=parameters["view"],
    )
    if (device == "gpu") and torch.has_cudnn:
        device = torch.device("cuda:{}".format(gpu_number))
    else:
        device = torch.device("cpu")
    model = model.to(device)
    model.eval()
    return model, device

# normalize accordingly for model
def standard_normalize_single_image(image):
    image -= np.mean(image)
    image /= np.maximum(np.std(image), 10**(-5))
    return image

# Here we should deliver a augmeted image
def process_augment_inputs(random_number_generator, parameters):

    cropped_image = cv2.imread(cropped_mammogram_path, -1)
    cropped_image = np.float32(cropped_image)
    cropped_image = standard_normalize_single_image(cropped_image)

    return cropped_image[:, :, np.newaxis]


def batch_to_tensor(batch, device):
    """
    Convert list of input ndarrays to tensor on device
    """
    inp = np.stack(batch)
    print(inp.shape, inp.dtype)

    tinp = torch.tensor(inp)
    print(tinp.shape, tinp.dtype)

    tinpp = tinp.permute(0, 3, 1, 2)
    print(tinpp.shape, tinpp.dtype)

    return tinpp.to(device)



def predict(parameters):
    """
    Outputs the predictions as csv file
    """
    random_number_generator = np.random.RandomState(parameters["seed"])
    model, device = load_model(parameters)

    all_predictions = []

    for epoch in range(epochs):

        batch = []

        for _, (inputs, labels) in enumerate(test_dataloader):  # no iterator com tam=bs

            #inputs = inputs.reshape((4360, 2261, 1))
            batch.append(inputs)

            print('-', inputs.shape, inputs.dtype, labels)

            print ('.')
            with torch.no_grad():
                #tensor_batch = batch_to_tensor(batch, device)

                # como usamos data loader, ja vem em tensor com permute certo
                tensor_batch = inputs.to(device)  # ---> nao precsa do permute, deu certo!
                y_hat = model(tensor_batch)
        predictions = np.exp(y_hat.cpu().detach().numpy())[:, :2, 1]
        all_predictions.append(predictions)
    agg_predictions = np.concatenate(all_predictions, axis=0).mean(0)
    predictions_dict = {
        "benign": float(agg_predictions[0]),
        "malignant": float(agg_predictions[1]),
    }
    print(json.dumps(predictions_dict))




def run(parameters):
    """
    Outputs the predictions as csv file
    """
    random_number_generator = np.random.RandomState(parameters["seed"])
    model, device = load_model(parameters)

    all_predictions = []
    for _ in range(epochs):
        batch = []
        for _ in range(batch_size):
            image_t = process_augment_inputs(
                #model_input=model_input,
                random_number_generator=random_number_generator,
                parameters=parameters,
            )
            batch.append(image_t)
            print('-', image_t.shape, image_t.dtype)

        print ('.', len(batch))
        with torch.no_grad():
            tensor_batch = batch_to_tensor(batch, device)
            y_hat = model(tensor_batch)
        predictions = np.exp(y_hat.cpu().detach().numpy())[:, :2, 1]
        all_predictions.append(predictions)
    agg_predictions = np.concatenate(all_predictions, axis=0).mean(0)
    predictions_dict = {
        "benign": float(agg_predictions[0]),
        "malignant": float(agg_predictions[1]),
    }
    print(json.dumps(predictions_dict))




def main():
    parser = argparse.ArgumentParser(description='Run image-only model or image+heatmap model')
    parser.add_argument('--view', default=view_simple)
    parser.add_argument('--model-path', default=IMAGE_MODEL_PATH,)
    parser.add_argument('--cropped-mammogram-path')
    parser.add_argument('--batch-size', default=1, type=int)
    parser.add_argument('--seed', default=0, type=int)
    parser.add_argument('--use-augmentation', action="store_true")
    parser.add_argument('--num-epochs', default=1, type=int)
    parser.add_argument('--device-type', default="cpu", choices=['gpu', 'cpu'])
    parser.add_argument("--gpu-number", type=int, default=0)
    args = parser.parse_args()

    parameters = {
        "view": args.view,
        "model_path": args.model_path,
        "cropped_mammogram_path": args.cropped_mammogram_path,
        "device_type": args.device_type,
        "gpu_number": args.gpu_number,
        "max_crop_noise": (100, 100),
        "max_crop_size_noise": 100,
        "batch_size": args.batch_size,
        "seed": args.seed,
        "augmentation": args.use_augmentation,
        "num_epochs": args.num_epochs,
    }
    run(parameters)
    predict(parameters)



if __name__ == "__main__":
    main()

