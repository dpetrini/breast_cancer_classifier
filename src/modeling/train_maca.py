## REmover esse arquivo - na proxima revisao - MACA esta separado

# treinar mass calcification detection 

# rodar do raiz do NYU
# export PYTHONPATH=$(pwd):$PYTHONPATH
#
# DGPP 09/2019

"""
Runs the image only IMAGE model (no image+heatmaps model) for breast cancer prediction.
"""
import argparse
import numpy as np
import torch
import json

# NYU files
import src.modeling.models as models
import src.modeling.layers as layers

from torch.autograd import Variable
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader

import sys
import cv2
import time
import copy
import datetime
import matplotlib.pyplot as plt
from sklearn.metrics import roc_auc_score, auc, roc_curve


import src.modeling.dataset_maca as data   # load our dataset class - default = dataset


IMAGE_MODEL_PATH='models/ImageOnly__ModeImage_weights.p'

view_simple = 'L-CC'
view = 'CC'

batch_size = 1    # aumentar estah dando problema devido aos diferentes tam. de image
device = 'gpu'
gpu_number=1
num_epochs = 3 #45
mini_batch = batch_size #30


transfer = 'FINE_TUNING' 
#transfer = 'FEATURE_EXTRACTION'


def train_and_validate(model, train_dataloader, validation_dataloader, 
                loss_criterion, device, mini_batch=5, epochs=25):

    start = time.time()
    history = []
    batch_counter = 0
    batch_val_counter = 0
    best_auc_val = 0.60      # minimum acc
    print_tensors = False

    print('Layers to train: ')
    params_to_update = []
    for name,param in model.named_parameters():
        if param.requires_grad == True:
            if transfer == 'FEATURE_EXTRACTION':
                torch.nn.init.normal_(param, 0.01) #init xavier_uniform_  , zeros_
                #torch.nn.init.zeros_(param) #init xavier_uniform_  , zeros_
            elif transfer == 'FINE_TUNING':
                pass

            params_to_update.append(param)
            #print("\t",name, '\t', param)

    optimizer = optim.Adam(params_to_update, lr = 1e-6) #--> lr=5e-6

    for epoch in range(epochs):
        epoch_start = time.time()
        print('Epoch: {}/{}'.format(epoch+1, epochs))

        # set to training mode
        model.train()

        #loss and acc for the epoch
        train_loss = 0.0; train_acc = 0.0; validation_loss = 0.0; validation_acc = 0.0
        # auc for the epoch
        y_hat_auc = []; label_auc = []; y_hat_auc_val = []; label_auc_val = []

        for _, (inputs, labels) in enumerate(train_dataloader): 

            inputs = inputs.to(device) #cuda()) 
            labels = torch.tensor(np.array(labels), dtype=torch.float).to(device)

            optimizer.zero_grad()                   # clean existing gradients
            outputs = model(inputs)                 # forward pass  
            y_hat = compute_y_hat(outputs, print_tensors) # make output to compatible with labels
            loss = loss_criterion(y_hat, labels)    # compute loss  
            loss.backward()                         # backprop the gradients
            optimizer.step()                        # update parameters

            # compute the total loss for the batch and add it to train_loss
            train_loss += loss.item() * inputs.size(0)

            # compute the accuracy
            acc = calc_acc(y_hat, labels, mini_batch, print_tensors)

            # compute total accuracy in the whole batch and add to train_acc
            train_acc += acc.item() * inputs.size(0)

            batch_counter += inputs.size(0)

            # guarda Y-hat/label para AUC
            label_auc = np.append(label_auc, labels.cpu().detach().numpy())
            y_hat_auc = np.append(y_hat_auc, y_hat.cpu().detach().numpy())

        
        # validation - no gradient tracking needed
        with torch.no_grad():
            
            # set to evaluation mode
            model.eval()

            # validation loop
            for _, (inputs, labels) in enumerate(validation_dataloader):

                inputs = Variable(inputs.to(device))

                labels = torch.tensor(np.array(labels), dtype=torch.float)
                labels = Variable(labels.to(device))
                
                outputs = model(inputs)                         # forward pass for validation  
                y_hat = compute_y_hat(outputs, print_tensors)   # make output to compatible with labels                
                loss = loss_criterion(y_hat, labels)            # compute loss       
                validation_loss += loss.item() * inputs.size(0) # batch total loss 

                # calculcate validation acc
                acc = calc_acc(y_hat, labels, mini_batch, print_tensors)

                # compute total accuracy in the whole batch and addd to valid_acc
                validation_acc += acc.item() * inputs.size(0)

                batch_val_counter += inputs.size(0)

                # guarda Y-hat/label para AUC
                label_auc_val = np.append(label_auc_val, labels.cpu().detach().numpy())
                y_hat_auc_val = np.append(y_hat_auc_val, y_hat.cpu().detach().numpy())

        # fing average training loss and training accuracy
        avg_train_loss = train_loss/batch_counter 
        avg_train_acc  = train_acc/batch_counter

        # find average training loss and validation acc
        avg_validation_loss = validation_loss/batch_val_counter 
        avg_validation_acc  = validation_acc/batch_val_counter 

        batch_counter = 0
        batch_val_counter = 0

        # calculate AUC
        auc_train = roc_auc_score(label_auc.ravel(), y_hat_auc.ravel())
        auc_val = roc_auc_score(label_auc_val.ravel(), y_hat_auc_val.ravel())
        print('AUC Training: {:.3f} - AUC Validation: {:.3f}'.format(auc_train, auc_val))

        history.append([avg_train_loss, avg_validation_loss, avg_train_acc, avg_validation_acc, auc_train, auc_val])

        if (auc_val > best_auc_val and epoch >= epochs/3):
            print('----->  Best model now ', auc_val)
            best_model = copy.deepcopy(model)  # Will work
            best_auc_val = auc_val

        epoch_end = time.time()
        print('Epoch: {:03d}, Training: Loss: {:.4f}, Acc: {:.2f}%, Validation: Loss: {:0.4f}, Acc: {:.2f}%, Time: {:.4f}s'.format(epoch+1, 
                                        avg_train_loss, avg_train_acc*100, avg_validation_loss, 
                                        avg_validation_acc*100, epoch_end-epoch_start))

    elapsed_mins = (time.time()-start)/60
    print('Total training time:  {:.2f} mins.'.format(elapsed_mins))

    return best_model, history  # model

#Accuracy
def calc_acc(y_hat, labels, mini_bs, print_tensors):
    y_hat = (y_hat>0.5).float()     # acima de 0.5 consideramos 1
    correct = torch.eq(y_hat, labels).float().sum()
    if print_tensors:
        print('y_hat for acc (thr=0.5): ', y_hat, labels, y_hat.shape[1])
        print ('correct', correct)
    acc = correct/(y_hat.shape[1]*mini_bs)

    return acc

def compute_y_hat(output, print_tensors=False):
    # etapa realizada na predicao: gera probs complementadas (soma 1 por linha)
    # equivalente ao softmax??
    exp1 = torch.exp(output)
    mass_y_hat = exp1[:, 0, 1]
    calc_y_hat = exp1[:, 1, 0]

    # concatena as saida individuais para o Loss
    y_hat = torch.cat([mass_y_hat, calc_y_hat]).reshape(-1,2)  #unsqueeze(0)  #NG
    
    if print_tensors:
        print('Y_hat ', y_hat, y_hat.size(), y_hat.dim())

    return y_hat



def run_test(model, loss_criterion,  test_dataloader, device, summary, mini_batch=5):

    '''
    Function to compute the accuracy on the test set
    Parameters
        :param model: Model to test
        :param loss_criterion: Loss Criterion to minimize
    '''
    test_acc = 0.0
    test_loss = 0.0
    print_tensors = False
    batch_val_counter = 0
    y_hat_auc_test = []
    label_auc_test = []    

    # Validation - No gradient tracking needed
    with torch.no_grad():

        # Set to evaluation mode
        model.eval()

        # validation loop
        for _, (inputs, labels) in enumerate(test_dataloader):

            inputs = Variable(inputs.to(device))

            labels = torch.tensor(np.array(labels), dtype=torch.float)
            labels = Variable(labels.to(device))
            
            outputs = model(inputs)                         # forward pass for validation  
            y_hat = compute_y_hat(outputs, print_tensors)   # make output to compatible with labels                
            loss = loss_criterion(y_hat, labels)            # compute loss       
            test_loss += loss.item() * inputs.size(0)       # batch total loss 

            # calculcate validation acc
            acc = calc_acc(y_hat, labels, mini_batch, print_tensors)

            # compute total accuracy in the whole batch and addd to valid_acc
            test_acc += acc.item() * inputs.size(0)

            batch_val_counter += inputs.size(0)

            # guarda Y-hat/label para AUC
            label_auc_test = np.append(label_auc_test, labels.cpu().detach().numpy())
            y_hat_auc_test = np.append(y_hat_auc_test, y_hat.cpu().detach().numpy())

        # find average training loss and validation acc
        avg_test_loss = test_loss/batch_val_counter 
        avg_test_acc  = test_acc/batch_val_counter 

        # calculate AUC
        auc_test = roc_auc_score(label_auc_test.ravel(), y_hat_auc_test.ravel())
        print('AUC Test: {:.3f}'.format(auc_test))

    # Find average test loss and test accuracy
    avg_test_loss = test_loss/batch_val_counter 
    avg_test_acc = test_acc/batch_val_counter

    print("Test accuracy : " + str(avg_test_acc)+ " Test loss : " + str(avg_test_loss))

    # Compute ROC curve and ROC area for each class BENIGN (0..1)
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    fpr["micro"], tpr["micro"], thr = roc_curve(label_auc_test.ravel(), y_hat_auc_test.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    #print(fpr["micro"], tpr["micro"], thr)

    # Plot all ROC curves
    plt.figure()
    plt.plot(fpr["micro"], tpr["micro"],
            label='ROC curve (area = {0:0.2f})'
                ''.format(roc_auc["micro"]),
            color='deeppink', linestyle=':', linewidth=4)

    plt.plot([0, 1], [0, 1], 'k--', lw=2)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Mass x Calcification')
    plt.legend(loc="lower right")
    plt.savefig('maca/plot_train/'+summary+'_test_ROC.png')
    plt.show()


# class ModelInput:
#     def __init__(self, image): 
#         self.image = image


def load_model(parameters):
    """
    Loads trained cancer classifier - only supporint IMAGE Type prediction
    """
    global device
    input_channels = 1 
    model = models.SingleImageBreastModel(input_channels)
    model.load_state_from_shared_weights(
        state_dict=torch.load(parameters["model_path"])["model"],
        view=parameters["view"],
    )
    if (device == "gpu") and torch.has_cudnn:
        device = torch.device("cuda:{}".format(gpu_number))
    else:
        device = torch.device("cpu")

    # print('Printing the model: ')
    # print(model)
    #sys.exit()

    # freeze model parametes
    for param in model.parameters():
        if transfer == 'FEATURE_EXTRACTION':
            param.requires_grad = False        # Feature extraction
        elif transfer == 'FINE_TUNING':
            param.requires_grad = True          # Fine tuning

    # set the layers to train
    if transfer == 'FEATURE_EXTRACTION':
        fc1_inputs  = model.fc1.in_features
        fc_out_inputs = model.output_layer.fc_layer.in_features

        model.fc1 = layers.OutputLayer(fc1_inputs, 256)  #256
        model.output_layer = layers.OutputLayer(fc_out_inputs, (2,2)) #

        print(fc1_inputs, fc_out_inputs)

    model = model.to(device)

    return model, device


def main():
    parser = argparse.ArgumentParser(description='Run image-only model or image+heatmap model')
    parser.add_argument('--view', default=view_simple)
    parser.add_argument('--model-path', default=IMAGE_MODEL_PATH,)
    parser.add_argument('--batch-size', default=1, type=int)
    parser.add_argument('--seed', default=0, type=int)
    parser.add_argument('--use-augmentation', action="store_true")
    parser.add_argument('--num-epochs', default=1, type=int)
    parser.add_argument('--device-type', default="cpu", choices=['gpu', 'cpu'])
    parser.add_argument("--gpu-number", type=int, default=0)
    args = parser.parse_args()

    parameters = {
        "view": args.view,
        "model_path": args.model_path,
        "device_type": args.device_type,
        "gpu_number": args.gpu_number,
        "max_crop_noise": (100, 100),
        "max_crop_size_noise": 100,
        "batch_size": args.batch_size,
        "seed": args.seed,
        "augmentation": args.use_augmentation,
        "num_epochs": args.num_epochs,
    }


    # dataset 160 train, 40 val, 40 test
    # train_image_paths =  'data/train'
    # validation_image_paths = 'data/validation'
    # test_image_paths = 'data/test'

    # data clean
    train_image_paths =  'data_clean/train'
    validation_image_paths = 'data_clean/validation'
    test_image_paths = 'data_clean/test'

    # dataset 400 train, 100 val, 50 test
    # train_image_paths =  'data_500/train'
    # validation_image_paths = 'data_500/validation'
    # test_image_paths = 'data_500/test'

    # num_epochs = 45
    # mini_batch = batch_size #30

    # classe dataset que carregar arquivos e faz transformacoes
    mammo_dataset_train = data.MyDataset(train_image_paths, view=view)
    mammo_dataset_validation = data.MyDataset(validation_image_paths, view=view, train=False)
    mammo_dataset_test = data.MyDataset(test_image_paths, view=view, train=False)

    print('Size train:', len(mammo_dataset_train), ' Size val: ', len(mammo_dataset_validation))

    n_samples = len(mammo_dataset_train) + len(mammo_dataset_validation)

    # sys.exit()

    # DataLoader eh iterador que retorna tensores pytorch
    train_dataloader = DataLoader(mammo_dataset_train, 
                                        batch_size=batch_size, shuffle=True,
                                        num_workers=1)
    
    validation_dataloader = DataLoader(mammo_dataset_validation, batch_size=batch_size, shuffle=True, num_workers=1)
    test_dataloader = DataLoader(mammo_dataset_test, batch_size=batch_size, shuffle=True, num_workers=1)


    model, device = load_model(parameters)


    # Loss function
    loss_func = nn.BCELoss() #nn.CrossEntropyLoss()

    # train the model 
    trained_model, history = train_and_validate(model, 
                            train_dataloader, validation_dataloader, 
                            loss_func, device, mini_batch, num_epochs)

    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d-%H:%M')

    summary = str(st)+'_'+str(num_epochs)+'ep_'+str(n_samples)+'n'

    # TODO ajustar o dicionario para salvar compativel com NYU
    # dar um print na predicao para ver como fica esse dic, etc,etc.
    torch.save(trained_model.state_dict(), 'maca/models_train/'+summary+'_model.pt')
    #torch.save(trained_model.state_dict(), 'models_train/'+str(st)+'_model_'+'.pt')



    history = np.array(history)
    plt.plot(history[:,0:2])
    plt.legend(['Train Loss', 'Val Loss'])
    plt.xlabel('Epoch Number')
    plt.ylabel('Loss')
    plt.ylim(0,1)
    plt.savefig('maca/plot_train/'+summary+'_loss_curve.png')
    plt.show()

    plt.plot(history[:,2:4])
    plt.legend(['Train Accuracy', 'Val Accuracy'])
    plt.xlabel('Epoch Number')
    plt.ylabel('Acc')
    plt.ylim(0,1)
    plt.savefig('maca/plot_train/'+summary+'_acc_curve.png')
    plt.show()

    plt.plot(history[:,4:6])
    plt.legend(['Train Auc/epoch', 'Val Auc/epoch'])
    plt.xlabel('Epoch Number')
    plt.ylabel('Auc')
    plt.ylim(0,1)
    plt.savefig('maca/plot_train/'+summary+'_auc_curve.png')
    plt.show()

    run_test(model, loss_func,  test_dataloader, device, summary, mini_batch)


if __name__ == "__main__":
    main()

