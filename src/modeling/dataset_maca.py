# Read images and augments - Pytorch Dataset Loader
# DGPP 09/2019

import numpy as np
import os
from PIL import Image, ImageEnhance
import glob
import torch
import torch.nn as nn
from torch.autograd import Variable
from random import randint
from torch.utils.data.dataset import Dataset
import torchvision.transforms.functional as TF
import torchvision.transforms as transforms
import random
import matplotlib.pyplot as plt
import pandas as pd

import sys

import cv2


# returns a list with complete file name and label 
def make_dataset(dir, class_to_idx, view):
    images = []
    dir = os.path.expanduser(dir)
    for target in sorted(class_to_idx.keys()):
        d = os.path.join(dir, target)
        if not os.path.isdir(d):
            continue
        for root, _, fnames in sorted(os.walk(d)):
            for fname in sorted(fnames):
                # load only view (MLO or CC) according to parameter
                if view.upper() in fname or view.lower() in fname:
                    path = os.path.join(root, fname)
                    item = (path, class_to_idx[target])
                    images.append(item)

    return images


class MyDataset(Dataset):
    def __init__(self, image_path, view='CC', train=True):
        self.image_path = image_path

        classes, class_to_idx = self._find_classes(self.image_path)
        print(classes, class_to_idx)

        samples = make_dataset(self.image_path, class_to_idx, view)
        if len(samples) == 0:
            raise (RuntimeError("Found 0 files in subfolders of: " + self.image_path + "\n"))

        self.classes = classes
        self.class_to_idx = class_to_idx
        self.samples = samples
        self.train = train

        # standard size for CC (NYU network)
        self.height = 2677
        self.width = 1942
        self.x_crop = 150      # for shift crop like NYU 
        self.y_crop = 150
        self.limit = 0.2

        print ('Loaded images for view: ' +view+ ', samples size: ', len(samples), self.image_path)


    def _find_classes(self, dir):
        """
        Finds the class folders in a dataset.
        Returns:
            tuple: (classes, class_to_idx) where classes are relative to (dir),
                   and class_to_idx is a dictionary.
        """
        if sys.version_info >= (3, 5):
            # Faster and available in Python 3.5 and above
            classes = [d.name for d in os.scandir(dir) if d.is_dir()]
        else:
            classes = [d for d in os.listdir(dir) if os.path.isdir(os.path.join(dir, d))]
        classes.sort()
        class_to_idx = {classes[i]: i for i in range(len(classes))}
        return classes, class_to_idx


    def transform(self, image, label):

        # image original size
        orig_width, orig_height = image.size

        # random angle
        affine = transforms.RandomAffine((-5,5))
        image = affine(image)

        # perspective = transforms.RandomPerspective(distortion_scale=0.1, 
        #                                             p=0.1, interpolation=3)
        # image = perspective(image)

        # crop similar to NYU (um 'pequeno zoom' no centro + shifts pequenos)
        center_rand_crop = random.uniform(0, 1)
        x_shift = int(self.x_crop*center_rand_crop) + 100
        y_shift = int(self.y_crop*center_rand_crop + orig_height/10)
        image = TF.crop(image, y_shift/2, x_shift/2, orig_height-y_shift, orig_width-x_shift)

        # Random horizontal flipping -- nao atrapalha...
        if random.random() > 0.5:
            image = TF.hflip(image)

        # Random vertical flipping  -- faz sentido para CC
        if random.random() > 0.5:
            image = TF.vflip(image)

        # resize to network input size
        resize = transforms.Resize(size=(self.height, self.width), interpolation=3) #PIL.Image.BICUBIC
        image = resize(image)

        #self.show_image(image)

        #from here on numpy
        image = np.float32(image)
        max = np.max(image)
        min = np.min(image)
        range_im = max-min

        # contrast augmentation
        alpha = 1.0 + self.limit * random.uniform(-1, 1)
        image = image*alpha   

        # brightness aug
        beta = range_im * self.limit * random.uniform(-1, 1)
        image = image+beta

        image = self.standard_normalize(image)
        image = image[:, :, np.newaxis]
        image = TF.to_tensor(image)

        return image, label

    def show_image(self, image):

        temp = np.uint16(image)
        #temp =  cv2.normalize(temp, None, alpha=0, beta=65535, norm_type=cv2.NORM_MINMAX)
        temp = cv2.resize(temp, (temp.shape[1]//6, temp.shape[0]//6))
        cv2.imshow('name', temp)
        cv2.waitKey(0)

        return

    def passthrough(self, image, label):

        resize = transforms.Resize(size=(self.height, self.width), interpolation=3) #PIL.Image.BICUBIC
        image = resize(image)

        #self.show_image(image)

        image = self.standard_normalize(image)
        image = image[:, :, np.newaxis]
        image = TF.to_tensor(image)

        return image, label

    def __len__(self):
        return len(self.samples)


    # normalize accordingly for model
    def standard_normalize(self, image):
        image = np.float32(image)
        image -= np.mean(image)
        image /= np.maximum(np.std(image), 10**(-5))
        return image


    def __getitem__(self,idx):

        path, target = self.samples[idx]

        # set labels according to folder
        if target == 0:
            label = np.array([0, 1])    # calcification
        elif target == 1:
            label = np.array([1, 0])    # mass

        image = Image.open(path)

        #width, height = image.size
        #print(width, height, path)

        if self.train == True:
            #x, y = self.passthrough(image, label)  
            x, y = self.transform(image, label)
        else:
            x, y = self.passthrough(image, label)   # usar para images/labels já aug.

        return x, y