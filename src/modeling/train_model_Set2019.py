# Copyright (C) 2019 Nan Wu, Jason Phang, Jungkyu Park, Yiqiu Shen, Zhe Huang, Masha Zorin,
#   Stanisław Jastrzębski, Thibault Févry, Joe Katsnelson, Eric Kim, Stacey Wolfson, Ujas Parikh,
#   Sushma Gaddam, Leng Leng Young Lin, Kara Ho, Joshua D. Weinstein, Beatriu Reig, Yiming Gao,
#   Hildegard Toth, Kristine Pysarenko, Alana Lewin, Jiyon Lee, Krystal Airola, Eralda Mema,
#   Stephanie Chung, Esther Hwang, Naziya Samreen, S. Gene Kim, Laura Heacock, Linda Moy,
#   Kyunghyun Cho, Krzysztof J. Geras
#
# This file is part of breast_cancer_classifier.
#
# breast_cancer_classifier is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# breast_cancer_classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with breast_cancer_classifier.  If not, see <http://www.gnu.org/licenses/>.
# ==============================================================================
"""
Runs the image only model and image+heatmaps model for breast cancer prediction.
"""
import argparse
import collections as col
import numpy as np
import os
import pandas as pd
import torch
import tqdm
import sys
import matplotlib.pyplot as plt
import time
import datetime
import random


import src.utilities.pickling as pickling
import src.utilities.tools as tools
import src.modeling.models as models
import src.data_loading.loading as loading
from src.constants import VIEWS, VIEWANGLES, LABELS, MODELMODES

import src.modeling.layers as layers

import torch #, torchvision
#from torchvision import datasets, models, transforms
import torch.nn as nn
import torch.optim as optim
from torchsummary import summary
from torch.autograd import Variable
import torch.nn.functional as F

from sklearn.metrics import roc_auc_score, auc

import cv2

transfer = 'FINE_TUNING'
#transfer = 'FEATURE_EXTRACTION'

train_split = 0.7       # factor for training/validation


QUIET = True           # Display messages per batch

def load_model(parameters):
    """
    Loads trained cancer classifier
    """
    input_channels = 3 if parameters["use_heatmaps"] else 1

    print('----------> using: ', input_channels, 'input channels')

    model_class = {
        MODELMODES.VIEW_SPLIT: models.SplitBreastModel,
        MODELMODES.IMAGE: models.ImageBreastModel,
    }[parameters["model_mode"]]
    model = model_class(input_channels)
    model.load_state_dict(torch.load(parameters["model_path"])["model"])

    # for k in model.state_dict():
    #     print(k)

    if (parameters["device_type"] == "gpu") and torch.has_cudnn:
        device = torch.device("cuda:{}".format(parameters["gpu_number"]))
        print('GPU')
    else:
        device = torch.device("cpu")

 
    model = model.to(device)

    # model = torch.nn.DataParallel(model, device_ids=list(
    #     range(torch.cuda.device_count()))).cuda()

    
    
    # print('Printing the model: ')
    # print(model)
    
    # sys.exit()


    #summary(model, input_size=(3, 224, 224), device='cuda')


    # TESTE com FINE TUNING: colocar True para todos parametros
    # Para Feature Extraction, colocar False para todos aqui

    # freeze model parametes
    for param in model.parameters():

        if transfer == 'FEATURE_EXTRACTION':
            param.requires_grad = False        # Feature extraction
        elif transfer == 'FINE_TUNING':
            param.requires_grad = True          # Fine tuning

        #print(param) #ok, pesos
 

    # change the final layer of resnet50 for transfer learning
    #fc_inputs = model.fc.in_features
    fc_inputs_cc  = model.output_layer_cc.fc_layer.in_features
    fc_inputs_mlo = model.output_layer_mlo.fc_layer.in_features

    print(fc_inputs_cc)

    # model.output_layer_cc.fc_layer = nn.Linear(fc_inputs_cc, 8)
    # model.output_layer_mlo.fc_layer = nn.Linear(fc_inputs_mlo, 8)



    # para usar como abaixo precisa fazer com classe/foward, etc
    # h = nn.Linear(fc_inputs_cc, 8)
    # m = nn.LogSoftmax(dim=1)
    # #h = F.log_softmax(h, dim=-1)
    # model.output_layer_cc.fc_layer = m(h)

    # h = nn.Linear(fc_inputs_mlo, 8)
    # m = nn.LogSoftmax(dim=-1)
    # #h = F.log_softmax(h, dim=-1)
    # model.output_layer_mlo.fc_layer = m(h)


    # Mais correto ateh agora
    # model.output_layer_cc.fc_layer = nn.Sequential(
    #     nn.Linear(fc_inputs_cc, 8),
    #     nn.LogSoftmax(dim=-1)	# for use NLLLoss()
    # )

    # model.output_layer_mlo.fc_layer = nn.Sequential(
    #     nn.Linear(fc_inputs_mlo, 8),
    #     nn.LogSoftmax(dim=-1)	# for use NLLLoss()
    # )

    # aparentemente igual acima, mais correto 
    # model.output_layer_cc.fc_layer = nn.Sequential(
    #     layers.OutputLayer(fc_inputs_cc, (4,2))
    # )

    # model.output_layer_mlo.fc_layer = nn.Sequential(
    #     layers.OutputLayer(fc_inputs_mlo, (4,2))
    # )

    # mais enxuto e replica exatamente o final
    model.output_layer_cc = layers.OutputLayer(fc_inputs_cc, (4,2))
    model.output_layer_mlo = layers.OutputLayer(fc_inputs_mlo, (4,2))


    # cc_out = model.output_layer_cc.fc_layer.out_features
    # mlo_out = model.output_layer_mlo.fc_layer.out_features

    # h = {
    #     "C": cc_out,
    #     "MLO": mlo_out,
    # }



    #print(model)

    # out =  torch.cat([cc_out,mlo_out],1) #nn.Sequential(model.output_layer_cc, model.output_layer_mlo)
    # print(out)

    #output_cat = torch.cat([cc_out, mlo_out], dim=1)

    #output_cat = torch.nn.Sequential(*(list(cc_out)+list(mlo_out)))

    # model.output_layer_cc.fc_layer = layers.OutputLayer(fc_inputs_cc, (4,2))
    # model.output_layer_mlo.fc_layer = layers.OutputLayer(fc_inputs_mlo, (4,2))


    # for param in model.parameters():
    #     print(param) #ok, pesos

    #sys.exit()

    model = model.to(device)

    #model.eval()
    return model, device


# carrega 2
def load_exam_images_heatmaps(datum, parameters, loaded_image_dict, loaded_heatmaps_dict):

    image_extension = ".hdf5" if parameters["use_hdf5"] else ".png"

    for view in VIEWS.LIST:
        for short_file_path in datum[view]:
            #print(short_file_path)
            loaded_image = loading.load_image(
                image_path=os.path.join(parameters["image_path"], short_file_path + image_extension),
                view=view,
                horizontal_flip=datum["horizontal_flip"],
            )
            # if parameters["use_heatmaps"]:
            #     loaded_heatmaps = loading.load_heatmaps(
            #         benign_heatmap_path=os.path.join(parameters["heatmaps_path"], "heatmap_benign",
            #                                          short_file_path + ".hdf5"),
            #         malignant_heatmap_path=os.path.join(parameters["heatmaps_path"], "heatmap_malignant",
            #                                             short_file_path + ".hdf5"),
            #         view=view,
            #         horizontal_flip=datum["horizontal_flip"],
            #     )
            # else:
            #     loaded_heatmaps = None
            loaded_heatmaps = None # revert for heatmaps

            loaded_image_dict[view].append(loaded_image)  #numpy float32
            loaded_heatmaps_dict[view].append(loaded_heatmaps)
    #print('loaded_image_dict ', loaded_image_dict)

    return loaded_image_dict, loaded_heatmaps_dict

# global para compartilhar
random_number_generator = np.random.RandomState(0)  #(parameters["seed"])


# generator de imagens augmentadas
# retorna batch_dict no formato esperado pela rede NYU
def load_data_batch(datum, parameters,  #data_batch, #batch_dict,
                                loaded_image_dict, loaded_heatmaps_dict):

    batch_dict = {view: [] for view in VIEWS.LIST}
    i = 0

    #for d in data_batch:
    while(True):
        i = i + 1
        #print ('data_batch d: ', i)
        for view in VIEWS.LIST:
            image_index = 0
            if parameters["augmentation"]:
                image_index = random_number_generator.randint(low=0, high=len(datum[view]))
                #print('image_index',image_index, len(datum[view]))
                image=loaded_image_dict[view][image_index]
                #print(view, image.mean())
            cropped_image, cropped_heatmaps = loading.augment_and_normalize_image(
                image=image, #loaded_image_dict[view][image_index],
                auxiliary_image=loaded_heatmaps_dict[view][image_index],
                view=view,
                best_center=datum["best_center"][view][image_index],
                random_number_generator=random_number_generator,
                augmentation=parameters["augmentation"],
                max_crop_noise=parameters["max_crop_noise"],
                max_crop_size_noise=parameters["max_crop_size_noise"],
            )

            #print(view, cropped_image.mean())

            # Augmentation confirmado ok. (06-09-2019)

            # scale_percent = 10 # percent of original size
            # width = int(cropped_image.shape[1] * scale_percent / 100)
            # height = int(cropped_image.shape[0] * scale_percent / 100)
            # dim = (width, height)
            # # resize image
            # resized = cv2.resize(cropped_image, dim, interpolation = cv2.INTER_CUBIC)
 
            # cv2.imshow('img '+str(i)+" "+str(view), resized)
            # cv2.waitKey(0)
            
            #print('Added img: '+str(d)+" "+str(view)+ " ", cropped_image.shape)

            if loaded_heatmaps_dict[view][image_index] is None:
                batch_dict[view].append(cropped_image[:, :, np.newaxis])
            else:
                batch_dict[view].append(np.concatenate([
                    cropped_image[:, :, np.newaxis],
                    cropped_heatmaps,
                ], axis=2))

        # used as generator
        yield batch_dict


def read_label(df, exam, mini_bs, print_tensors):
    """
    Returns tensor with labels (ground truth) of current exam.
    Expects csv read in df, current exam, size of mini-batch.
    """

    # Carrega labels padrao deste exame do csv
    x = exam['L-CC'][0]
    label_row = df[df['patient_id'].str.contains(x[:6])].values
    if len(label_row) > 0:
        labels = torch.tensor(np.array(np.array(label_row[0, 1:5], dtype=np.uint8)), dtype=torch.float)
    else:
        if not QUIET:
            print('Label not found, using all [0, 0, 0, 0], exam: ', x[:6])
        labels = torch.tensor(np.array([0, 0, 0, 0]), dtype=torch.float)

    # replica label por mini_bs vezes
    labels=labels.view(-1, 1).repeat(1, mini_bs)
    
    if print_tensors:
        print('Labels ', labels, labels.size(), labels.dim())

    return labels

#Accuracy
def calc_acc(y_hat, labels, mini_bs, print_tensors):
    y_hat = (y_hat>0.5).float()     # acima de 0.5 consideramos 1
    correct = torch.eq(y_hat, labels).float().sum()
    if print_tensors:
        print('y_hat for acc (thr=0.5): ', y_hat)
        print ('correct', correct)
    acc = correct/(y_hat.shape[0]*mini_bs)

    return acc

# retorna array de prediction ([array([0.0579794 , 0.07538317, 0.00905164, 0.01794641], dtype=float32)])
def train_model(model, device, exam_list, parameters):
    """
    Returns predictions of image only model or image+heatmaps model.
    Prediction for each exam is averaged for a given number of epochs.
    """

    # https://pytorch.org/docs/stable/_modules/torch/nn/init.html

    print('Layers to train: ')
    params_to_update = []
    for name,param in model.named_parameters():
        if param.requires_grad == True:
            if transfer == 'FEATURE_EXTRACTION':
                torch.nn.init.zeros_(param) #init xavier_uniform_  
            elif transfer == 'FINE_TUNING':
                pass

            params_to_update.append(param)
            #print("\t",name, '\t', param)


    #define optimize and loss function
    loss_func =  nn.BCELoss() #nn.MSELoss() #nn.CrossEntropyLoss() #nn.NLLLoss() #

    # Feature extraction, LR = 0.0001
    # Fine Tuning (update all weights), LR = 0.00001

    optimizer = optim.Adam(params_to_update, lr = 0.00001) #--> em teste sempre o mesmo

    epochs = 50

    # tamanho do mini-batch (quant. de exames augmentados)
    mini_bs = 1

    # batch-size == quantidade de imagens
    batch_size = len(exam_list)

    # maybe shuffle exam_list, check:
    #https://stackoverflow.com/questions/19895028/randomly-shuffling-a-dictionary-in-python

    batch_number = 0    # contador

    train_size = int(batch_size * train_split)
    train_data_size  = train_size * mini_bs  # quantidade de exames augmentadas que treinam rede

    validation_size = batch_size - train_size
    validation_data_size = validation_size * mini_bs

    print('training size: ', train_size, ' val size: ', validation_size, ' Type: ', transfer)

    print_tensors = False  # debug

    # read labels file
    label_file = 'labels/ddsm_description_cases_ALL.csv'
    df = pd.read_csv(label_file)

    print('Starting training: Epochs: {:03d}, Batch size: {:03d}, mini-batch: {:03d}.'.format(epochs, batch_size, mini_bs))

    #set to training mode
    model.train()

    start = time.time()
    history = []

    for epoch in range(epochs):

        epoch_start = time.time()

        # loss and acc for the epoch
        train_loss = 0.0
        train_acc = 0.0
        validation_loss = 0.0
        validation_acc = 0.0

        y_hat_auc = []
        label_auc = []

        # esse eh o loop principal de batches que rodam por epoch
        for datum in exam_list:      # Para cada exame - dataloader , tqdm depois ...

            batch_number = batch_number + 1

            loaded_image_dict = {view: [] for view in VIEWS.LIST}
            loaded_heatmaps_dict = {view: [] for view in VIEWS.LIST}

            # carrega as 4 imagens do exame
            loaded_image_dict, loaded_heatmaps_dict = load_exam_images_heatmaps(datum, parameters,
                                                        loaded_image_dict, loaded_heatmaps_dict)

            # a rede NYU estah neste formato de entrada
            batch_dict = {view: [] for view in VIEWS.LIST}

            # Carrega o mini-batch, mini_bs * n_images (n_images sempre 4 == exame)
            # 4 imagens augmentadas (e heatmaps) colocadas no tensor no device
            bs_count = 0
            for batch_dict in load_data_batch(datum, parameters, loaded_image_dict, loaded_heatmaps_dict):
                tensor_batch = {
                    view: torch.tensor(np.stack(batch_dict[view])).permute(0, 3, 1, 2).to(device)
                    #view: print(batch_dict[view])
                    for view in VIEWS.LIST
                }
                bs_count = bs_count + 1
                if bs_count == mini_bs:
                    break

            # Carrega labels padrao deste exame do csv
            labels = read_label(df, datum, mini_bs, print_tensors)
            labels = labels.to(device)

            if batch_number <= train_size:

                model.train()
                optimizer.zero_grad()           # clean existing gradients
                output = model(tensor_batch)    # forward pass
                y_hat = compute_y_hat(output, print_tensors) # make output to compatible with labels                
                loss = loss_func(y_hat, labels) # compute loss              
                loss.backward()                 # backprop the gradients
                optimizer.step()                # update parameters

                # compute the total loss for the batch and add it to train_loss
                train_loss += loss.item() * mini_bs # multiplica para fazer media geral depois

                #Accuracy
                acc = calc_acc(y_hat, labels, mini_bs, print_tensors)

                train_acc +=acc.item() * mini_bs # como eh media, mul bs size para media final depois

                if not QUIET:
                    print('Batch number: {:03d}, Training Loss: {:.4f}, Accuracy: {:.4}'.format(batch_number, loss.item(), acc))
                else:
                    print('.', end=''); sys.stdout.flush()

            elif batch_number > train_size:
                with torch.no_grad():       # validation - no gradient tracking needed

                    model.eval()
                    output = model(tensor_batch)        # forward pass                   
                    y_hat = compute_y_hat(output)       # make output to compatible with labels
                    loss_val = loss_func(y_hat, labels) # compute loss

                    # compute the total loss for the batch and add it to train_loss
                    validation_loss += loss_val.item() * mini_bs # multiplica para fazer media geral depois

                    #Accuracy
                    acc = calc_acc(y_hat, labels, mini_bs, print_tensors)

                    validation_acc +=acc.item() * mini_bs # como eh media, mul bs size para media final depois

                    if not QUIET:
                        print('Batch number: {:03d}, Validation Loss: {:.4f}, Accuracy: {:.4}'.format(batch_number, loss_val.item(), acc))
                    else:
                        print('|', end=''); sys.stdout.flush()

            # guarda Y-hat/label para AUC
            label_auc = np.append(label_auc, labels.cpu().detach().numpy())
            y_hat_auc = np.append(y_hat_auc, y_hat.cpu().detach().numpy())


        batch_number = 0

        # find average training loss and validation acc
        avg_train_acc = train_acc/train_data_size
        avg_train_loss = train_loss/train_data_size
        avg_validation_acc  = validation_acc/validation_data_size 
        avg_validation_loss = validation_loss/validation_data_size

        # calculate AUC
        auc_ = roc_auc_score(label_auc.ravel(), y_hat_auc.ravel())
        print('\nAUC Training: %.3f' % auc_)

        history.append([avg_train_loss, avg_validation_loss, avg_train_acc, avg_validation_acc, auc_])

        epoch_end = time.time()
        print('Epoch: {:03d}, Training: Loss: {:.4f}, Acc: {:.2f}%, Validation: Loss: {:0.4f}, Acc: {:.2f}%, Time: {:.4f}s'.format(epoch, 
                                        avg_train_loss, avg_train_acc*100, avg_validation_loss, 
                                        avg_validation_acc*100, epoch_end-epoch_start))

        # save if the model has best acc till now (?)
        #torch.save(model, dataset+'_model_'+str(epoch)+'.pt')

    print('Total training time:  {:.4f}s'.format(time.time()-start))

    return model, history, batch_size


# # ok -  como referencia
# def run_model(model, device, exam_list, parameters):
#     """
#     Returns predictions of image only model or image+heatmaps model.
#     Prediction for each exam is averaged for a given number of epochs.
#     """
#     random_number_generator = np.random.RandomState(parameters["seed"])

#     image_extension = ".hdf5" if parameters["use_hdf5"] else ".png"

#     with torch.no_grad():
#         predictions_ls = []
#         for datum in tqdm.tqdm(exam_list):      # Para cada exame
#             predictions_for_datum = []
#             loaded_image_dict = {view: [] for view in VIEWS.LIST}
#             loaded_heatmaps_dict = {view: [] for view in VIEWS.LIST}

#             # carrega as 4 views em um dicionario
#             for view in VIEWS.LIST:
#                 for short_file_path in datum[view]:
#                     loaded_image = loading.load_image(
#                         image_path=os.path.join(parameters["image_path"], short_file_path + image_extension),
#                         view=view,
#                         horizontal_flip=datum["horizontal_flip"],
#                     )
#                     # if parameters["use_heatmaps"]:
#                     #     loaded_heatmaps = loading.load_heatmaps(
#                     #         benign_heatmap_path=os.path.join(parameters["heatmaps_path"], "heatmap_benign",
#                     #                                          short_file_path + ".hdf5"),
#                     #         malignant_heatmap_path=os.path.join(parameters["heatmaps_path"], "heatmap_malignant",
#                     #                                             short_file_path + ".hdf5"),
#                     #         view=view,
#                     #         horizontal_flip=datum["horizontal_flip"],
#                     #     )
#                     # else:
#                     #     loaded_heatmaps = None
#                     loaded_heatmaps = None # revert for heatmaps

#                     loaded_image_dict[view].append(loaded_image)
#                     loaded_heatmaps_dict[view].append(loaded_heatmaps)


#             #print('loaded_image_dict ', loaded_image_dict)
#             for data_batch in tools.partition_batch(range(parameters["num_epochs"]), parameters["batch_size"]):
#                 batch_dict = {view: [] for view in VIEWS.LIST}
#                 for d in data_batch:
#                     print ('data_batch d: ', d)
#                     for view in VIEWS.LIST:
#                         image_index = 0
#                         if parameters["augmentation"]:
#                             image_index = random_number_generator.randint(low=0, high=len(datum[view]))
#                         cropped_image, cropped_heatmaps = loading.augment_and_normalize_image(
#                             image=loaded_image_dict[view][image_index],
#                             auxiliary_image=loaded_heatmaps_dict[view][image_index],
#                             view=view,
#                             best_center=datum["best_center"][view][image_index],
#                             random_number_generator=random_number_generator,
#                             augmentation=parameters["augmentation"],
#                             max_crop_noise=parameters["max_crop_noise"],
#                             max_crop_size_noise=parameters["max_crop_size_noise"],
#                         )

#                         # cv2.imshow('img '+str(d)+" "+str(view), cropped_image)
#                         # cv2.waitKey(0)
                        
#                         print('Added img: '+str(d)+" "+str(view)+ " ", cropped_image.shape)

#                         if loaded_heatmaps_dict[view][image_index] is None:
#                             batch_dict[view].append(cropped_image[:, :, np.newaxis])
#                         else:
#                             batch_dict[view].append(np.concatenate([
#                                 cropped_image[:, :, np.newaxis],
#                                 cropped_heatmaps,
#                             ], axis=2))

#                 print('Tam batch images: ', len(batch_dict))
#                 tensor_batch = {
#                     view: torch.tensor(np.stack(batch_dict[view])).permute(0, 3, 1, 2).to(device)
#                     for view in VIEWS.LIST
#                 }

                

#                 # realiza a predicao dessas 4 imgs augmentadas
#                 model.eval()
#                 output = model(tensor_batch) 

#                 # teste comparativo
#                 for k, v in output.items():
#                     print(k, v)


#                 batch_predictions = compute_batch_predictions(output, mode=parameters["model_mode"])
#                 print('batch_predictions', batch_predictions)
#                 pred_df = pd.DataFrame({k: v[:, 1] for k, v in batch_predictions.items()})
#                 pred_df.columns.names = ["label", "view_angle"]
#                 print('pred_df', pred_df)
#                 predictions = pred_df.T.reset_index().groupby("label").mean().T[LABELS.LIST].values
#                 print('predictions', predictions)
#                 predictions_for_datum.append(predictions)
            
#             # realiza a media das 10 predicoes augmentadas
#             predictions_ls.append(np.mean(np.concatenate(predictions_for_datum, axis=0), axis=0))

#     print(predictions_ls)
#     return np.array(predictions_ls)


def compute_y_hat(output, print_tensors=False):
    """
    Format output predictions
    """

    # tensor de saida eh um dicionario com as Split Breast Model Views
    # vamos concatenar para ter um tensor unico e fazer as mesmas 
    # operacoes que sao realizadas na predicao original (test)
    output_cat = torch.cat([output["CC"], output["MLO"]], dim=1)

    # etapa realizada na predicao: gera probs complementadas (soma 1 por linha)
    # equivalente ao softmax??
    exp1 = torch.exp(output_cat)

    # Realiza o calculo de cada probabilidade : MEDIA, deixando explicito para conferencia
    sum = torch.add(exp1[:, 0, 1], exp1[:, 4, 1])   #  grad_fn=<AddBackward0>
    left_benign_y_hat = torch.div(sum, 2)           # grad_fn=<DivBackward0>)
    sum = torch.add(exp1[:, 1, 1], exp1[:, 5, 1])
    right_benign_y_hat = torch.div(sum, 2)
    sum = torch.add(exp1[:, 2, 1], exp1[:, 6, 1])
    left_malign_y_hat = torch.div(sum, 2)
    sum = torch.add(exp1[:, 3, 1], exp1[:, 7, 1])
    right_malign_y_hat = torch.div(sum, 2)

    # concatena as saida individuais para o Loss
    y_hat = torch.stack([left_benign_y_hat, right_benign_y_hat, left_malign_y_hat, right_malign_y_hat])
    
    if print_tensors:
        print('Y_hat ', y_hat, y_hat.size(), y_hat.dim())

    return y_hat


# def compute_batch_predictions(y_hat, mode):
#     """
#     Format predictions from different heads
#     """

#     if mode == MODELMODES.VIEW_SPLIT:
#         assert y_hat[VIEWANGLES.CC].shape == (1, 4, 2)
#         assert y_hat[VIEWANGLES.MLO].shape == (1, 4, 2)
#         batch_prediction_tensor_dict = col.OrderedDict()
#         batch_prediction_tensor_dict[LABELS.LEFT_BENIGN, VIEWANGLES.CC] = y_hat[VIEWANGLES.CC][:, 0]
#         batch_prediction_tensor_dict[LABELS.LEFT_BENIGN, VIEWANGLES.MLO] = y_hat[VIEWANGLES.MLO][:, 0]
#         batch_prediction_tensor_dict[LABELS.RIGHT_BENIGN, VIEWANGLES.CC] = y_hat[VIEWANGLES.CC][:, 1]
#         batch_prediction_tensor_dict[LABELS.RIGHT_BENIGN, VIEWANGLES.MLO] = y_hat[VIEWANGLES.MLO][:, 1]
#         batch_prediction_tensor_dict[LABELS.LEFT_MALIGNANT, VIEWANGLES.CC] = y_hat[VIEWANGLES.CC][:, 2]
#         batch_prediction_tensor_dict[LABELS.LEFT_MALIGNANT, VIEWANGLES.MLO] = y_hat[VIEWANGLES.MLO][:, 2]
#         batch_prediction_tensor_dict[LABELS.RIGHT_MALIGNANT, VIEWANGLES.CC] = y_hat[VIEWANGLES.CC][:, 3]
#         batch_prediction_tensor_dict[LABELS.RIGHT_MALIGNANT, VIEWANGLES.MLO] = y_hat[VIEWANGLES.MLO][:, 3]
#         batch_prediction_dict = col.OrderedDict([
#             (k, np.exp(v.cpu().detach().numpy()))
#             for k, v in batch_prediction_tensor_dict.items()
#         ])
#     elif mode == MODELMODES.IMAGE:
#         assert y_hat[VIEWS.L_CC].shape == (1, 2, 2)
#         assert y_hat[VIEWS.R_CC].shape == (1, 2, 2)
#         assert y_hat[VIEWS.L_MLO].shape == (1, 2, 2)
#         assert y_hat[VIEWS.R_MLO].shape == (1, 2, 2)
#         batch_prediction_tensor_dict = col.OrderedDict()
#         batch_prediction_tensor_dict[LABELS.LEFT_BENIGN, VIEWS.L_CC] = y_hat[VIEWS.L_CC][:, 0]
#         batch_prediction_tensor_dict[LABELS.LEFT_BENIGN, VIEWS.L_MLO] = y_hat[VIEWS.L_MLO][:, 0]
#         batch_prediction_tensor_dict[LABELS.RIGHT_BENIGN, VIEWS.R_CC] = y_hat[VIEWS.R_CC][:, 0]
#         batch_prediction_tensor_dict[LABELS.RIGHT_BENIGN, VIEWS.R_MLO] = y_hat[VIEWS.R_MLO][:, 0]
#         batch_prediction_tensor_dict[LABELS.LEFT_MALIGNANT, VIEWS.L_CC] = y_hat[VIEWS.L_CC][:, 1]
#         batch_prediction_tensor_dict[LABELS.LEFT_MALIGNANT, VIEWS.L_MLO] = y_hat[VIEWS.L_MLO][:, 1]
#         batch_prediction_tensor_dict[LABELS.RIGHT_MALIGNANT, VIEWS.R_CC] = y_hat[VIEWS.R_CC][:, 1]
#         batch_prediction_tensor_dict[LABELS.RIGHT_MALIGNANT, VIEWS.R_MLO] = y_hat[VIEWS.R_MLO][:, 1]

#         batch_prediction_dict = col.OrderedDict([
#             (k, np.exp(v.cpu().detach().numpy()))
#             for k, v in batch_prediction_tensor_dict.items()
#         ])
#     else:
#         raise KeyError(mode)
#     return batch_prediction_dict


def load_train_save(data_path, output_path, parameters):
    """
    Outputs the predictions as csv file
    """
    exam_list = pickling.unpickle_from_file(data_path)
    model, device = load_model(parameters)
    #predictions = run_model(model, device, exam_list, parameters)
    trained_model, history, size = train_model(model, device, exam_list, parameters)

    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d-%H:%M:%S')

    # TODO ajustar o dicionario para salvar compativel com NYU
    # dar um print na predicao para ver como fica esse dic, etc,etc.
    torch.save(trained_model.state_dict(), 'models_train/'+str(st)+'_model_'+'.pt')
    #torch.save(trained_model.state_dict(), 'models_train/'+str(st)+'_model_'+'.pt')


    history = np.array(history)
    plt.plot(history[:,0:2])
    plt.legend(['Train Loss', 'Val Loss'])
    plt.xlabel('Epoch Number')
    plt.ylabel('Loss')
    plt.ylim(0,1)
    plt.savefig('plot_train/'+str(st)+'_loss_curve.png')
    plt.show()

        
    plt.plot(history[:,2:4])
    plt.legend(['Train Accuracy', 'Val Accuracy'])
    plt.xlabel('Epoch Number')
    plt.ylabel('Acc')
    plt.ylim(0,1)
    plt.savefig('plot_train/'+str(st)+'_acc_curve.png')
    plt.show()


    print('Chack size: ', size, int(size * train_split) + 1)

    plt.plot(history[0:int(size * train_split) + 1, 4])
    plt.legend(['AUC Training'])
    plt.xlabel('Epoch Number')
    plt.ylabel('Auc')
    plt.ylim(0,1)
    plt.savefig('plot_train/'+str(st)+'_auc_training.png')
    plt.show()

    plt.plot(history[int(size * train_split)+1:size, 4])
    plt.legend(['AUC Validation'])
    plt.xlabel('Epoch Number')
    plt.ylabel('Auc')
    plt.ylim(0,1)
    plt.savefig('plot_train/'+str(st)+'_auc_validation.png')
    plt.show()

# def load_run_save(data_path, output_path, parameters):
#     """
#     Outputs the predictions as csv file
#     """
#     exa
# m_list = pickling.unpickle_from_file(data_path)
#     model, device = load_model(parameters)
#     predictions = run_model(model, device, exam_list, parameters)
#     #array de prediction ([array([0.0579794 , 0.07538317, 0.00905164, 0.01794641], dtype=float32)])

#     os.makedirs(os.path.dirname(output_path), exist_ok=True)

#     # Take the positive prediction

#     # insert the first column as ID DGPP
#     col1 = [] 
#     for i in range(len(exam_list)):
#         col1.append([exam_list[i]['case'][0].split('_')[0]]) # remove _png sufix

#     colnp = np.array(col1)
#     ID_predictions = np.hstack((colnp, predictions)) # join ID with predictions

#     # insert ID column header
#     lista = ['ID']
#     for x in LABELS.LIST:
#         lista.append(x)

#     df = pd.DataFrame(ID_predictions, columns=lista)
#     #print(df.head())

#     #df = pd.DataFrame(predictions, columns=LABELS.LIST) #orig
#     df.to_csv(output_path, index=False, float_format='%.4f')

#     # Also output to main dir if single
#     if (exam_list[0]['case'] == ['single']):
#         print('\nResultado predicao tipo "', output_path.split('/')[-1].split('_')[0], '" deste exame:')
#         print(df)
#         print('arquivo csv: ', output_path, '\n')


def main():
    parser = argparse.ArgumentParser(description='Run image-only model or image+heatmap model')
    parser.add_argument('--model-mode', default=MODELMODES.VIEW_SPLIT, type=str)
    parser.add_argument('--model-path', required=True)
    parser.add_argument('--data-path', required=True)
    parser.add_argument('--image-path', required=True)
    parser.add_argument('--output-path', required=True)
    parser.add_argument('--batch-size', default=1, type=int)
    parser.add_argument('--seed', default=0, type=int)
    parser.add_argument('--use-heatmaps', action="store_true") # store_true
    parser.add_argument('--heatmaps-path')
    parser.add_argument('--use-augmentation', action="store_true")
    parser.add_argument('--use-hdf5', action="store_true")
    parser.add_argument('--num-epochs', default=1, type=int)
    parser.add_argument('--device-type', default="cpu", choices=['gpu', 'cpu'])
    parser.add_argument("--gpu-number", type=int, default=0)
    args = parser.parse_args()

    parameters = {
        "device_type": args.device_type,
        "gpu_number": args.gpu_number,
        "max_crop_noise": (100, 100),
        "max_crop_size_noise": 100,
        "image_path": args.image_path,
        "batch_size": args.batch_size,
        "seed": args.seed,
        "augmentation": args.use_augmentation,
        "num_epochs": args.num_epochs,
        "use_heatmaps": args.use_heatmaps,
        "heatmaps_path": args.heatmaps_path,
        "use_hdf5": args.use_hdf5,
        "model_mode": args.model_mode,
        "model_path": args.model_path,
    }
    #load_run_save(
    load_train_save(
        data_path=args.data_path,
        output_path=args.output_path,
        parameters=parameters,
    )


if __name__ == "__main__":
    main()


#Saida dos tensores e predicao
# CC tensor([[[-0.7802, -0.6130],
#          [-0.5453, -0.8666],
#          [-0.7454, -0.6435],
#          [-0.7731, -0.6191]]], device='cuda:1', grad_fn=<LogSoftmaxBackward>)
# MLO tensor([[[-0.6542, -0.7337],
#          [-0.6384, -0.7511],
#          [-0.8660, -0.5458],
#          [-0.7389, -0.6494]]], device='cuda:1', grad_fn=<LogSoftmaxBackward>)

# Aqui tira a exponencial e dá valor complementar para cada linha correspondente a acima

# BP:  OrderedDict([(('left_benign', 'CC'), array([[0.4582997, 0.5417003]], dtype=float32)), 
# (('left_benign', 'MLO'), array([[0.5198763 , 0.48012376]], dtype=float32)), 
# (('right_benign', 'CC'), array([[0.5796419 , 0.42035815]], dtype=float32)),
#  (('right_benign', 'MLO'), array([[0.5281484 , 0.47185153]], dtype=float32)),
#  (('left_malignant', 'CC'), array([[0.474555  , 0.52544504]], dtype=float32)), 
# (('left_malignant', 'MLO'), array([[0.42063877, 0.57936126]], dtype=float32)), 
# (('right_malignant', 'CC'), array([[0.46159303, 0.53840697]], dtype=float32)), 
# (('right_malignant', 'MLO'), array([[0.47763485, 0.5223651 ]], dtype=float32))])

# Aqui pega apenas a segunda prob de cada linha acima 

# pred_df label      left_benign           right_benign           left_malignant           right_malignant          
# view_angle          CC       MLO           CC       MLO             CC       MLO              CC       MLO
# 0               0.5417  0.480124     0.420358  0.471852       0.525445  0.579361        0.538407  0.522365
# predictions [[0.51091206 0.44610482 0.55240315 0.53038603]]
# 100%|████████████████████████████████████████████████████████████████████████████████████████| 1/1 [00:06<00:00,  6.11s/it]
# [array([0.51133454, 0.44542885, 0.55090743, 0.53175557], dtype=float32)]

            # esse print referencia abaixo eh para um mini-batch mini_bs=1

            #     # forward pass
            #     output = model(tensor_batch)

            #     # print output
            #     # for k, v in output.items():
            #     #     print(k, v)

            #     # CC tensor([[[-0.4861, -0.9545],
            #     # [-0.6781, -0.7084],
            #     # [-0.6533, -0.7347],
            #     # [-0.7036, -0.6828]]], device='cuda:1', grad_fn=<LogSoftmaxBackward>)
            #     # MLO tensor([[[-0.7638, -0.6272],
            #     # [-0.9216, -0.5073],
            #     # [-0.7954, -0.6004],
            #     # [-0.7172, -0.6696]]], device='cuda:1', grad_fn=<LogSoftmaxBackward>)

            #     # tensor de saida eh um dicionario com as Views
            #     # vamos concatenar para ter um tensor unico e fazer as mesmas operacoes que
            #     # que sao realizadas na predicao original (test)
            #     output_cat = torch.cat([output["CC"], output["MLO"]], dim=1)
                
            #     #print(output_cat)
            #     # tensor([[[-0.7268, -0.6606],
            #     # [-0.8160, -0.5838],
            #     # [-0.6278, -0.7631],
            #     # [-0.4835, -0.9588],
            #     # [-0.8818, -0.5345],
            #     # [-0.7464, -0.6426],
            #     # [-0.6236, -0.7679],
            #     # [-0.5358, -0.8799]]], device='cuda:1', grad_fn=<CatBackward>)
                
            #     exp1 = torch.exp(output_cat)
                
            #     #print ('exp1: ', exp1)
            #     # exp1:  tensor([[[0.4834, 0.5166],
            #     # [0.4422, 0.5578],
            #     # [0.5338, 0.4662],
            #     # [0.6166, 0.3833],
            #     # [0.4140, 0.5860],
            #     # [0.4741, 0.5259],
            #     # [0.5360, 0.4640],
            #     # [0.5852, 0.4148]]], device='cuda:1', grad_fn=<ExpBackward>)

            #     # Realiza o calculo de cada probabilidade 
            #     left_benign_y_hat = torch.div(torch.add(exp1[0][0][1], exp1[0][4][1]), 2)
            #     right_benign_y_hat = torch.div(torch.add(exp1[0][1][1], exp1[0][5][1]), 2)
            #     left_malign_y_hat = torch.div(torch.add(exp1[0][2][1], exp1[0][6][1]), 2)
            #     right_malign_y_hat = torch.div(torch.add(exp1[0][3][1], exp1[0][7][1]), 2)

            #     # print('Train preds: ', left_benign_y_hat, right_benign_y_hat, left_malign_y_hat, right_malign_y_hat)
            #     # Train preds:  tensor(0.5513, device='cuda:1', grad_fn=<DivBackward0>) 
            #     # tensor(0.5419, device='cuda:1', grad_fn=<DivBackward0>) t
            #     # ensor(0.4651, device='cuda:1', grad_fn=<DivBackward0>) 
            #     # tensor(0.3991, device='cuda:1', grad_fn=<DivBackward0>)

            #     # concatena as saida individuais para o Loss
            #     y_hat = torch.stack([left_benign_y_hat, right_benign_y_hat, left_malign_y_hat, right_malign_y_hat])
                
            #     # print(y_hat)
            #     # tensor([0.5513, 0.5419, 0.4651, 0.3991], device='cuda:1', grad_fn=<StackBackward>)