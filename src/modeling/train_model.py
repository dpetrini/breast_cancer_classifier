#
#
# Script exclusivo para fazer transfer learning da rede NYU 
# 
# Rodar usando inbreast:
#
# python3 ./train_nyu.py  -d 'train_inbreast_36/' -t 'train_inbreast_36_dest/' -r 'train_inbreast_36_dest/'
#
# para transfer learning NYU: python3 ./train_nyu.py -d '/home/dpetrini/usp/fmusp/ICESP-scripts/data_icesp_menor40_100_train/'
#               -t 'train_icesp_menor40_100_dest/' -r 'test_icesp_menor40_136_dest/'
#
# Modelo sera gerado no diretorio models_train
#
# Jan-2020- ASCO paper suportando cross validation. Padrao 20 epocas e kfolds=5
#           realizados ajustes finos nas funcoes loss. (para ver diferença comparar com old_model_train.py no raiz)
#
#
import argparse
import collections as col
import numpy as np
import os
import pandas as pd
import torch
import tqdm
import sys
import matplotlib.pyplot as plt
import time
import datetime
import random
import copy

import src.utilities.pickling as pickling
import src.utilities.tools as tools
import src.modeling.models as models
import src.data_loading.loading as loading
from src.constants import VIEWS, VIEWANGLES, LABELS, MODELMODES

import src.modeling.layers as layers

import torch #, torchvision
import torch.nn as nn
import torch.optim as optim
from torchsummary import summary
from torch.autograd import Variable
import torch.nn.functional as F

from sklearn.metrics import roc_auc_score, auc

import cv2

# from torch.utils.tensorboard import SummaryWriter


transfer = 'FINE_TUNING'
#transfer = 'FEATURE_EXTRACTION'
epochs = 2
train_split = 0.8  #0.8       # factor for training/validation
LR = 1e-6

k_folds = 2

DATASET = 'ICESP'

if DATASET == 'DDSM':
    label_file = 'labels/ddsm_description_cases_ALL.csv'
elif DATASET == 'INBREAST':
    label_file = 'labels/inbreast_4_images_cases_ALL.csv'
elif DATASET == 'ICESP':
    label_file = 'labels/relacao_diagnostico_lado_tipolesao_ate226.csv'


QUIET = False            # Display messages per batch

RANDOM_TRAIN_SET = True # whether or not ramdomize training set 

BEST_MODEL_DIVISOR = 5  # epochs/BEST_MODEL_DIVISOR -> after this epoch save best


# Tensorboard
# default `log_dir` is "runs" - we'll be more specific here
# writer = SummaryWriter('runs/nyu_training_1')


def load_model(parameters):
    """
    Loads trained cancer classifier
    """
    input_channels = 3 if parameters["use_heatmaps"] else 1

    print('----------> using: ', input_channels, 'input channels')

    model_class = {
        MODELMODES.VIEW_SPLIT: models.SplitBreastModel,
        MODELMODES.IMAGE: models.ImageBreastModel,
    }[parameters["model_mode"]]
    model = model_class(input_channels)
    model.load_state_dict(torch.load(parameters["model_path"])["model"])

    # for k in model.state_dict():
    #     print(k)

    if (parameters["device_type"] == "gpu") and torch.has_cudnn:
        device = torch.device("cuda:{}".format(parameters["gpu_number"]))
        print('GPU')
    else:
        device = torch.device("cpu")

 
    #model = model.to(device)

    # freeze model parametes
    for param in model.parameters():

        if transfer == 'FEATURE_EXTRACTION':
            param.requires_grad = False        # Feature extraction
        elif transfer == 'FINE_TUNING':
            param.requires_grad = True          # Fine tuning

        #print(param) #ok, pesos

    if transfer == 'FEATURE_EXTRACTION':
        # change the FC layers for transfer learning
        fc_inputs_cc1  = model.fc1_cc.in_features  #
        fc_inputs_mlo1 = model.fc1_mlo.in_features #
        fc_inputs_cc2  = model.output_layer_cc.fc_layer.in_features
        fc_inputs_mlo2 = model.output_layer_mlo.fc_layer.in_features

        print(fc_inputs_cc2)

        # reassign same layers
        model.fc1_cc  = nn.Linear(fc_inputs_cc1, fc_inputs_cc2)    # 
        model.fc1_mlo = nn.Linear(fc_inputs_mlo1, fc_inputs_mlo2)  #
        model.output_layer_cc = layers.OutputLayer(fc_inputs_cc2, (4,2))
        model.output_layer_mlo = layers.OutputLayer(fc_inputs_mlo2, (4,2))


    model = model.to(device)

    return model, device


# carrega 2
def load_exam_images_heatmaps(datum, parameters, loaded_image_dict, loaded_heatmaps_dict):

    image_extension = ".hdf5" if parameters["use_hdf5"] else ".png"

    for view in VIEWS.LIST:
        for short_file_path in datum[view]:
            loaded_image = loading.load_image(
                image_path=os.path.join(parameters["image_path"], short_file_path + image_extension),
                view=view,
                horizontal_flip=datum["horizontal_flip"],
            )

            if parameters["use_heatmaps"]:
                loaded_heatmaps = loading.load_heatmaps(
                    benign_heatmap_path=os.path.join(parameters["heatmaps_path"], "heatmap_benign",
                                                     short_file_path + ".hdf5"),
                    malignant_heatmap_path=os.path.join(parameters["heatmaps_path"], "heatmap_malignant",
                                                        short_file_path + ".hdf5"),
                    view=view,
                    horizontal_flip=datum["horizontal_flip"],
                )
            else:
                loaded_heatmaps = None

            loaded_image_dict[view].append(loaded_image)  #numpy float32
            loaded_heatmaps_dict[view].append(loaded_heatmaps)

    return loaded_image_dict, loaded_heatmaps_dict

# global para compartilhar
random_number_generator = np.random.RandomState(0)  #(parameters["seed"])


# generator de imagens augmentadas
# retorna batch_dict no formato esperado pela rede NYU
def load_data_batch(datum, parameters,  #data_batch, #batch_dict,
                                loaded_image_dict, loaded_heatmaps_dict):

    batch_dict = {view: [] for view in VIEWS.LIST}
    i = 0

    #for d in data_batch:
    while(True):
        i = i + 1
        #print ('data_batch d: ', i)
        for view in VIEWS.LIST:
            image_index = 0
            if parameters["augmentation"]:
                image_index = random_number_generator.randint(low=0, high=len(datum[view]))
                image=loaded_image_dict[view][image_index]
            cropped_image, cropped_heatmaps = loading.augment_and_normalize_image(
                image=image, #loaded_image_dict[view][image_index],
                auxiliary_image=loaded_heatmaps_dict[view][image_index],
                view=view,
                best_center=datum["best_center"][view][image_index],
                random_number_generator=random_number_generator,
                augmentation=parameters["augmentation"],
                max_crop_noise=parameters["max_crop_noise"],
                max_crop_size_noise=parameters["max_crop_size_noise"],
            )

            # Augmentation confirmado ok. (06-09-2019)

            if loaded_heatmaps_dict[view][image_index] is None:
                batch_dict[view].append(cropped_image[:, :, np.newaxis])
            else:
                batch_dict[view].append(np.concatenate([
                    cropped_image[:, :, np.newaxis],
                    cropped_heatmaps,
                ], axis=2))

        # used as generator
        yield batch_dict

# get exam number from filename (ICESP)
def get_id_from_filename(image):
    folder = image[0:3]
    if not folder.isdigit():
        folder = image[0:2]
        if not folder.isdigit():
            folder = image[0:1]
            if not folder.isdigit():
                print('Not a valid filename', image)
                sys.exit()
    return folder

count_malig = 0
count_benign = 0

def read_label(df, exam, mini_bs, print_tensors):
    """
    Returns tensor with labels (ground truth) of current exam.
    Expects csv read in df, current exam, size of mini-batch.
    """

    global count_malig, count_benign

    # Carrega labels padrao deste exame do csv
    x = exam['L-CC'][0]
    # for each dataset read the identifier
    if DATASET == 'DDSM':
        search_str = x[:6]
    elif DATASET == 'INBREAST':
        search_str = x[9:25]
    elif DATASET == 'ICESP':
        search_str = get_id_from_filename(x)
    # locate identifier
    if DATASET == 'ICESP':
        label_row = df.loc[df['patient_id'] == int(search_str)].values
    else:
        label_row = df[df['patient_id'].str.contains(search_str)].values

    if len(label_row) > 0:
        labels = torch.tensor(np.array(np.array(label_row[0, 1:5], dtype=np.uint8)), dtype=torch.float)
    else:
        if not QUIET:
            print('Label not found, using all [0, 0, 0, 0], exam: ', x[:6])
        labels = torch.tensor(np.array([0, 0, 0, 0]), dtype=torch.float)

    if labels[2] == 1 or labels[3] == 1:      # count malign cases for auc error
        count_malig += 1   #print(' 1')
    else:
        count_benign += 1   #print(' 0')

    # replica label por mini_bs vezes
    labels=labels.view(-1, 1).repeat(1, mini_bs)
    
    if print_tensors:
        print('Labels ', labels, labels.size(), labels.dim())

    return labels

#Accuracy
def calc_acc(y_hat, labels, mini_bs, print_tensors):
    y_hat = (y_hat>0.5).float()     # acima de 0.5 consideramos 1
    correct = torch.eq(y_hat, labels).float().sum()
    if print_tensors:
        print('y_hat for acc (thr=0.5): ', y_hat)
        print ('correct', correct)
    acc = correct/(y_hat.shape[0]*mini_bs)

    return acc

# retorna array de prediction ([array([0.0579794 , 0.07538317, 0.00905164, 0.01794641], dtype=float32)])
def train_model(model, device, exam_list, k_folds, k, parameters):
    """
    Returns predictions of image only model or image+heatmaps model.
    Prediction for each exam is averaged for a given number of epochs.
    """

    # cross validation (compatible with k_fold==1)
    #valid_samples = len(exam_list) // k_folds       # integer number of each fold
    excluded_samples = len(exam_list) % k_folds     # remaining sample (will not be used)
    indices = list(range(len(exam_list) - excluded_samples))    # simply excluded from the end
    size = len(indices)
    subset_size = round(size / k_folds)
    subsets = [indices[x:x+subset_size] for x in range(0, len(indices), subset_size)]
    
    train_set = []
    val_set = []
    for i in range(k_folds):
        if i != k:
            train_set += subsets[i]
        else:
            val_set = subsets[i]

    #random.shuffle(train_set)


    # https://pytorch.org/docs/stable/_modules/torch/nn/init.html

    print('Layers to train: ')
    params_to_update = []
    for name,param in model.named_parameters():
        if param.requires_grad == True:
            if transfer == 'FEATURE_EXTRACTION':
                pass
                #torch.nn.init.zeros_(param) #init xavier_uniform_  
            elif transfer == 'FINE_TUNING':
                pass

            params_to_update.append(param)
            #print("\t",name, '\t', param)


    #define optimize and loss function
    #loss_func =  nn.BCELoss() #nn.MSELoss() #nn.CrossEntropyLoss() #nn.NLLLoss() #

    optimizer = optim.Adam(params_to_update, lr = LR) #--> em teste sempre o mesmo
    # optimizer = optim.Adam(
    #     [
    #         #four_view_resnet
    #         #{"params": model.four_view_resnet.parameters(), "lr": 1e-6},
    #         {"params": model.fc1_cc.parameters(), "lr": 1e-5},
    #         {"params": model.fc1_mlo.parameters(), "lr": 1e-5},
    #         {"params": model.output_layer_cc.parameters(), "lr": 1e-4},
    #         {"params": model.output_layer_mlo.parameters(), "lr": 1e-4}
    #     ],
    #     lr=LR,  # default para as outras camadas do FT
    # )

    print('Learning rates used:')
    for g in optimizer.param_groups:
       print(g['lr'])

    mini_bs = 1         # tamanho do mini-batch (quant. de exames augmentados)
    batch_number = 0    # contador

    if k_folds == 1:
        batch_size = len(exam_list)                     # batch-size == quantidade de imagens
        train_size = int(batch_size * train_split)
    else:
        batch_size = size
        train_size = len(train_set)

    train_data_size  = train_size * mini_bs  # quantidade de exames augmentadas que treinam rede

    validation_size = batch_size - train_size
    validation_data_size = validation_size * mini_bs

    print('training size: ', train_size, ' val size: ', validation_size, ' Type: ', transfer, ' DB: ', DATASET, ' k/kfolds: ', k+1, '/', k_folds)

    print_tensors = False  # debug

    # read labels file
    df = pd.read_csv(label_file)

    print('Starting training: Epochs: {:03d}, Batch size: {:03d}, mini-batch: {:03d}.'.format(epochs, batch_size, mini_bs))

    #set to training mode
    model.train()

    start = time.time()
    history = []
    best_auc = 0.1
    best_model = []

    selection = list(train_set+val_set)   # validation at last
    print('Val. set indices: ', val_set)
    print('All: ', selection)
    print('Size train/val', len(train_set), len(val_set))

    for epoch in range(epochs):

        epoch_start = time.time()

        # loss/ acc / auc for the epoch
        #train_acc = 0.0; validation_acc = 0.0;
        train_loss = 0.0; validation_loss = 0.0
        y_hat_auc = []; label_auc = []; y_hat_val_auc = []; label_val_auc = []

        # esse eh o loop principal de batches que rodam por epoch

        # Ramdomiza a parte de treino (N primeiros)
        if RANDOM_TRAIN_SET:
            selection[0:train_size] = random.sample(list(range(0,train_size)), train_size)
        
        
        #for datum in exam_list:      # Para cada exame - dataloader , tqdm depois ...
        for sel in selection:      # using random

            datum = exam_list[sel]

            #print(sel, datum["L-CC"][0], end='')

            batch_number = batch_number + 1

            loaded_image_dict = {view: [] for view in VIEWS.LIST}
            loaded_heatmaps_dict = {view: [] for view in VIEWS.LIST}

            # carrega as 4 imagens do exame
            loaded_image_dict, loaded_heatmaps_dict = load_exam_images_heatmaps(datum, parameters,
                                                        loaded_image_dict, loaded_heatmaps_dict)

            # a rede NYU estah neste formato de entrada
            batch_dict = {view: [] for view in VIEWS.LIST}

            # Carrega o mini-batch, mini_bs * n_images (n_images sempre 4 == exame)
            # 4 imagens augmentadas (e heatmaps) colocadas no tensor no device
            bs_count = 0
            for batch_dict in load_data_batch(datum, parameters, loaded_image_dict, loaded_heatmaps_dict):
                tensor_batch = {
                    view: torch.tensor(np.stack(batch_dict[view])).permute(0, 3, 1, 2).to(device)
                    #view: print(batch_dict[view])
                    for view in VIEWS.LIST
                }
                bs_count = bs_count + 1
                if bs_count == mini_bs:
                    break

            # Carrega labels padrao deste exame do csv
            labels = read_label(df, datum, mini_bs, print_tensors)
            labels = labels.to(device)

            if batch_number <= train_size:  ### TROCAR PELO LEN(TRAIN)

                model.train()
                optimizer.zero_grad()           # clean existing gradients
                output = model(tensor_batch)    # forward pass
                y_hat = compute_y_hat(output, print_tensors) # for auc       

                loss = compute_loss(output, labels)
                loss.backward()                 # backprop the gradients
                optimizer.step()                # update parameters

                # compute the total loss for the batch and add it to train_loss
                train_loss += loss.item() * mini_bs # multiplica para fazer media geral depois


                #Accuracy
                #acc = calc_acc(y_hat, labels, mini_bs, print_tensors)  # for new loss
                #train_acc +=acc.item() * mini_bs # como eh media, mul bs size para media final depois

                if not QUIET:
                    print('.', end=''); sys.stdout.flush()

                # guarda Y-hat/label para AUC
                label_auc = np.append(label_auc, labels.cpu().detach().numpy())
                y_hat_auc = np.append(y_hat_auc, y_hat.cpu().detach().numpy())


            elif batch_number > train_size:
                with torch.no_grad():       # validation - no gradient tracking needed

                    model.eval()
                    output = model(tensor_batch)        # forward pass                   
                    y_hat = compute_y_hat(output)       # for acc
                    loss_val = compute_loss(output, labels)

                    # compute the total loss for the batch and add it to train_loss
                    validation_loss += loss_val.item() * mini_bs # multiplica para fazer media geral depois

                    #Accuracy
                    #acc = calc_acc(y_hat, labels, mini_bs, print_tensors)
                    #validation_acc +=acc.item() * mini_bs # como eh media, mul bs size para media final depois

                    if not QUIET:
                        print('|', end=''); sys.stdout.flush()

                    # guarda Y-hat/label para AUC - VAL
                    label_val_auc = np.append(label_val_auc, labels.cpu().detach().numpy())
                    y_hat_val_auc = np.append(y_hat_val_auc, y_hat.cpu().detach().numpy())


        batch_number = 0

        # find average training loss and validation acc
        #avg_train_acc = train_acc/train_data_size
        avg_train_loss = train_loss/train_data_size
        #avg_validation_acc  = validation_acc/validation_data_size 
        avg_validation_loss = validation_loss/validation_data_size

        #print('contagem malig ', count_malig)


        # calculate AUC Train
        label_auc = np.array(label_auc.reshape((-1,4)))
        y_hat_auc = np.array(y_hat_auc.reshape((-1,4)))
        if (np.count_nonzero(label_auc[:, 0:2])):  # happens in some tests...
                auc_ben_train = roc_auc_score(label_auc[:, 0:2].ravel(), y_hat_auc[:, 0:2].ravel())
                print('\n[Train] AUC Benign: %.3f' % auc_ben_train, end='')
        else:
                print('[Train] AUC Benign Only zero labels', end='')
                auc_ben_train = 0
        if (np.count_nonzero(label_auc[:, 2:5])):  # happens in some tests...
                auc_mal_train = roc_auc_score(label_auc[:, 2:5].ravel(), y_hat_auc[:, 2:5].ravel())
        else:
                auc_mal_train = 0
        print(' AUC Malignant: %.3f' % auc_mal_train, end='')


        # calculate AUC VAL
        label_auc = np.array(label_val_auc.reshape((-1,4)))
        y_hat_auc = np.array(y_hat_val_auc.reshape((-1,4)))
        if (np.count_nonzero(label_auc[:, 0:2])):  # happens in some tests...
                auc_ben_val = roc_auc_score(label_auc[:, 0:2].ravel(), y_hat_auc[:, 0:2].ravel())
                print('  [Val] AUC Benign: %.3f' % auc_ben_val, end='')
        else:
                print('  [Val] AUC Benign Only zero labels', end='')
                auc_ben_val = 0
        if (np.count_nonzero(label_auc[:, 2:5])):  # happens in some tests...
                auc_mal_val = roc_auc_score(label_auc[:, 2:5].ravel(), y_hat_auc[:, 2:5].ravel())
        else:
                auc_mal_val = 0

        # calc sigma auc accdrding to hanley
        global count_malig, count_benign
        sigma_auc = calc_auc_desv(count_malig, count_benign, auc_mal_val)

        # print sigma only for malignant
        print(' AUC  Malignant: {:.4f} ± {:.4f} '.format(auc_mal_val, sigma_auc),  end='')

        print(' #malign/not-malign: ', count_malig, count_benign)

        count_malig = 0
        count_benign = 0




        history.append([avg_train_loss, avg_validation_loss, 
                        #avg_train_acc, avg_validation_acc,
                        auc_ben_train, auc_mal_train, auc_ben_val, auc_mal_val, 
                        sigma_auc])

        # find best model
        if auc_mal_val > best_auc: # and epoch >= epochs/BEST_MODEL_DIVISOR):
                print('---------------------->  Best model now AUC val malig: {:.3f}'.format(auc_mal_val))
                best_model = copy.deepcopy(model)  # Will work
                best_auc = auc_mal_val



        epoch_end = time.time()
        print('Epoch: {:03d}, Training: Loss: {:.3f}, Acc: {:.2f}%, Validation: Loss: {:0.3f}, Acc: {:.2f}%, Time: {:.2f}s'.format(epoch+1, 
                                        avg_train_loss, 
                                        0, #avg_train_acc*100, 
                                        avg_validation_loss, 
                                        0, #avg_validation_acc*100, 
                                        epoch_end-epoch_start))


    print('Best AUC Val Malignant: {:.4f} ± {:.4f}'.format(best_auc, sigma_auc))

    elapsed_mins = (time.time()-start)/60
    print('Total training time:  {:.2f} mins.'.format(elapsed_mins))


    return model, best_model, history, best_auc


def compute_loss(output, labels):

    loss_func =  nn.BCEWithLogitsLoss()  #nn.BCELoss()  nn.BCEWithLogitsLoss()  # NLLLoss
    output_cat = torch.cat([output["CC"], output["MLO"]], dim=1)

    # como a saida aqui eh logits (sem probabilidade), devemos aplicar sigmoid 
    #  para ter probs antes do BCE_Loss que espera probs
    #output_cat = torch.sigmoid(output_cat)

    # print(output_cat,  labels,output_cat.size(),  labels.size())
    # print(output_cat.unsqueeze(0),  labels.unsqueeze(0))
    # print(output_cat[:,0,1].unsqueeze(0).size(),  labels[0].unsqueeze(0))

    loss =  loss_func(output_cat[:,3,1], labels[3]) + \
            loss_func(output_cat[:,7,1], labels[3]) + \
            loss_func(output_cat[:,1,1], labels[1]) + \
            loss_func(output_cat[:,5,1], labels[1]) + \
            loss_func(output_cat[:,2,1], labels[2]) + \
            loss_func(output_cat[:,6,1], labels[2]) + \
            loss_func(output_cat[:,0,1], labels[0]) + \
            loss_func(output_cat[:,4,1], labels[0])

    return loss



def compute_y_hat(output, print_tensors=False):
    """
    Format output predictions
    """

    # tensor de saida eh um dicionario com as Split Breast Model Views
    # vamos concatenar para ter um tensor unico e fazer as mesmas 
    # operacoes que sao realizadas na predicao original (test)
    output_cat = torch.cat([output["CC"], output["MLO"]], dim=1)

    #exp1 = torch.sigmoid(output_cat)  
    #--> tem que usar exp pois a saida eh log_softmax, para tirar o log e ficar com softmax!
    #exp1 = torch.exp(output_cat)
    exp1 = torch.softmax(output_cat, dim=-1)

    # Realiza o calculo de cada probabilidade : MEDIA, deixando explicito para conferencia
    sum = torch.add(exp1[:, 0, 1], exp1[:, 4, 1])   #  grad_fn=<AddBackward0>
    left_benign_y_hat = torch.div(sum, 2)           # grad_fn=<DivBackward0>)
    sum = torch.add(exp1[:, 1, 1], exp1[:, 5, 1])
    right_benign_y_hat = torch.div(sum, 2)
    sum = torch.add(exp1[:, 2, 1], exp1[:, 6, 1])
    left_malign_y_hat = torch.div(sum, 2)
    sum = torch.add(exp1[:, 3, 1], exp1[:, 7, 1])
    right_malign_y_hat = torch.div(sum, 2)

    # concatena as saida individuais para o Loss
    y_hat = torch.stack([left_benign_y_hat, right_benign_y_hat, left_malign_y_hat, right_malign_y_hat])
    
    if print_tensors:
        print('Y_hat ', y_hat, y_hat.size(), y_hat.dim())

    return y_hat

def save_models(trained_model, best_model, k_folds, k):

    if k_folds > 1:
        cross_val_str = str(k+1)+'_'+str(k_folds)
    else:
        cross_val_str = ''

    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d-%H:%M')

    if transfer == 'FINE_TUNING':
        posfix = cross_val_str+'_FT_'+str(LR)+'_'+DATASET+'_'+ str(epochs)+'ep'
    elif transfer == 'FEATURE_EXTRACTION':
        posfix = cross_val_str+'_FE_'+DATASET+'_'+ str(epochs)+'ep'

    torch.save(trained_model.state_dict(), 'models_train/'+str(st)+'_model_'+posfix+'.pt')
    torch.save(best_model.state_dict(), 'models_train/'+str(st)+'_best_model_'+posfix+'.pt')


def plot_graphs(history, k_folds, k):

    if k_folds > 1:
        cross_val_str = str(k+1)+'_'+str(k_folds)
    else:
        cross_val_str = ''

    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d-%H:%M')

    if transfer == 'FINE_TUNING':
        posfix = cross_val_str+'_FT_'+str(LR)+'_'+DATASET+'_'+ str(epochs)+'ep'
    elif transfer == 'FEATURE_EXTRACTION':
        posfix = cross_val_str+'_FE_'+DATASET+'_'+ str(epochs)+'ep'

    history = np.array(history)
    plt.plot(history[:,0:2])
    plt.legend(['Train Loss', 'Val Loss'], loc="lower right")
    plt.xlabel('Epoch Number')
    plt.ylabel('Loss')
    #plt.legend(loc="lower right")
    plt.ylim(2,7)
    plt.grid(True, ls=':', lw=.5, c='k', alpha=.3)
    plt.savefig('plot_train/'+str(st)+'_'+posfix+'_loss_curve.png')
    #plt.show()

    # plt.plot(history[:,2:4])
    # plt.legend(['Train Accuracy', 'Val Accuracy'],loc="lower right")
    # plt.xlabel('Epoch Number')
    # plt.ylabel('Acc')
    # plt.ylim(0.5,1)
    # #plt.legend(loc="lower right")
    # plt.grid(True, ls=':', lw=.5, c='k', alpha=.3)
    # plt.savefig('plot_train/'+str(st)+'_'+posfix+'_acc_curve.png')
    # plt.show()

    #plt.plot(history[:,4:8])  # --> when enable Acc
    plt.plot(history[:,2:6])
    plt.legend(['AUC Train B', 'AUC Train M', 'AUC Val. B', 'AUC Val. M'],loc="lower right")
    plt.xlabel('Epoch Number')
    plt.ylabel('Auc')
    #plt.legend(loc="lower right")
    plt.ylim(0,1)
    # ls = point, linewidth-0.5, color=k, alpha-transparency
    plt.grid(True, ls=':', lw=.5, c='k', alpha=.3)
    plt.savefig('plot_train/'+str(st)+'_'+posfix+'_auc_all.png')
    #plt.show()

# Hanley and McNeil AUC std dev
# m: positive samples, n: negative samples
# auc: calculated value
# returns : standard deviation
def calc_auc_desv(m, n, auc):
    Pxxy = auc / (2-auc)
    Pxyy = 2*auc**2 / ((1+auc))

    sigma_2 = (auc*(1-auc) + (m-1)*(Pxxy- auc**2) + (n-1)*(Pxyy - auc**2)) / (m*n)
    sigma = np.sqrt(sigma_2)

    return sigma



def load_train_save(data_path, output_path, parameters):
    """
    Outputs the predictions as csv file
    """
    exam_list = pickling.unpickle_from_file(data_path)
    #model, device = load_model(parameters)

    histories = []
    history = []
    best_auc = 0
    best_aucs = []

    print('Using K folds: ', k_folds)

    for k in range(k_folds):

        model, device = load_model(parameters)

        trained_model, best_model, history, best_auc = train_model(model, device, exam_list, k_folds, k, parameters)
        
        save_models(trained_model, best_model, k_folds, k)
        plot_graphs(history, k_folds, k)
        histories.append(history)
        best_aucs.append(best_auc)

        del best_model          # clean everything
        del trained_model
        del model
        del device
        torch.cuda.empty_cache()

        


    means = np.asarray(histories, dtype = np.float32)
    stds = np.asarray(histories, dtype = np.float32)
 
    means = np.mean(means, axis=0)
    stds = np.std(stds, axis=0)

    print('histories', histories)
    print('mean:', means)
    print('std: ', stds)

    print('best_aucs: ', best_aucs)
    print('Best Aucs AVG: ', np.mean(best_aucs))

    # save as numpy file for future manipulation
    histories = np.asarray(histories, dtype = np.float32)
    print('Shape history: ', histories.shape)
    # np.savetxt("cross_val_transfer_k5_20ep_cada.csv", np.squeeze(histories), delimiter=",")

    #sys.exit()

    # ts = time.time()
    # st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d-%H:%M')

    # if transfer == 'FINE_TUNING':
    #     posfix = 'FT_'+str(LR)+'_'+DATASET+'_'+ str(epochs)+'ep'
    # elif transfer == 'FEATURE_EXTRACTION':
    #     posfix = 'FE_'+DATASET+'_'+ str(epochs)+'ep'

    # torch.save(trained_model.state_dict(), 'models_train/'+str(st)+'_model_'+posfix+'.pt')
    # torch.save(best_model.state_dict(), 'models_train/'+str(st)+'_best_model_'+posfix+'.pt')

    # history = np.array(history)
    # plt.plot(history[:,0:2])
    # plt.legend(['Train Loss', 'Val Loss'], loc="lower right")
    # plt.xlabel('Epoch Number')
    # plt.ylabel('Loss')
    # #plt.legend(loc="lower right")
    # plt.ylim(2,7)
    # plt.grid(True, ls=':', lw=.5, c='k', alpha=.3)
    # plt.savefig('plot_train/'+str(st)+'_'+posfix+'_loss_curve.png')
    # plt.show()

    # # plt.plot(history[:,2:4])
    # # plt.legend(['Train Accuracy', 'Val Accuracy'],loc="lower right")
    # # plt.xlabel('Epoch Number')
    # # plt.ylabel('Acc')
    # # plt.ylim(0.5,1)
    # # #plt.legend(loc="lower right")
    # # plt.grid(True, ls=':', lw=.5, c='k', alpha=.3)
    # # plt.savefig('plot_train/'+str(st)+'_'+posfix+'_acc_curve.png')
    # # plt.show()

    # #plt.plot(history[:,4:8])  # --> when enable Acc
    # plt.plot(history[:,2:6])
    # plt.legend(['AUC Train B', 'AUC Train M', 'AUC Val. B', 'AUC Val. M'],loc="lower right")
    # plt.xlabel('Epoch Number')
    # plt.ylabel('Auc')
    # #plt.legend(loc="lower right")
    # plt.ylim(0,1)
    # # ls = point, linewidth-0.5, color=k, alpha-transparency
    # plt.grid(True, ls=':', lw=.5, c='k', alpha=.3)
    # plt.savefig('plot_train/'+str(st)+'_'+posfix+'_auc_all.png')
    # plt.show()


def main():
    parser = argparse.ArgumentParser(description='Run image-only model or image+heatmap model')
    parser.add_argument('--model-mode', default=MODELMODES.VIEW_SPLIT, type=str)
    parser.add_argument('--model-path', required=True)
    parser.add_argument('--data-path', required=True)
    parser.add_argument('--image-path', required=True)
    parser.add_argument('--output-path', required=True)
    parser.add_argument('--batch-size', default=1, type=int)
    parser.add_argument('--seed', default=0, type=int)
    parser.add_argument('--use-heatmaps', action="store_true") # store_true
    parser.add_argument('--heatmaps-path')
    parser.add_argument('--use-augmentation', action="store_true")
    parser.add_argument('--use-hdf5', action="store_true")
    parser.add_argument('--num-epochs', default=1, type=int)
    parser.add_argument('--device-type', default="cpu", choices=['gpu', 'cpu'])
    parser.add_argument("--gpu-number", type=int, default=0)
    args = parser.parse_args()

    parameters = {
        "device_type": args.device_type,
        "gpu_number": args.gpu_number,
        "max_crop_noise": (100, 100),
        "max_crop_size_noise": 100,
        "image_path": args.image_path,
        "batch_size": args.batch_size,
        "seed": args.seed,
        "augmentation": args.use_augmentation,
        "num_epochs": args.num_epochs,
        "use_heatmaps": args.use_heatmaps,
        "heatmaps_path": args.heatmaps_path,
        "use_hdf5": args.use_hdf5,
        "model_mode": args.model_mode,
        "model_path": args.model_path,
    }
    #load_run_save(
    load_train_save(
        data_path=args.data_path,
        output_path=args.output_path,
        parameters=parameters,
    )


if __name__ == "__main__":
    main()


#Saida dos tensores e predicao
# CC tensor([[[-0.7802, -0.6130],
#          [-0.5453, -0.8666],
#          [-0.7454, -0.6435],
#          [-0.7731, -0.6191]]], device='cuda:1', grad_fn=<LogSoftmaxBackward>)
# MLO tensor([[[-0.6542, -0.7337],
#          [-0.6384, -0.7511],
#          [-0.8660, -0.5458],
#          [-0.7389, -0.6494]]], device='cuda:1', grad_fn=<LogSoftmaxBackward>)

# Aqui tira a exponencial e dá valor complementar para cada linha correspondente a acima

# BP:  OrderedDict([(('left_benign', 'CC'), array([[0.4582997, 0.5417003]], dtype=float32)), 
# (('left_benign', 'MLO'), array([[0.5198763 , 0.48012376]], dtype=float32)), 
# (('right_benign', 'CC'), array([[0.5796419 , 0.42035815]], dtype=float32)),
#  (('right_benign', 'MLO'), array([[0.5281484 , 0.47185153]], dtype=float32)),
#  (('left_malignant', 'CC'), array([[0.474555  , 0.52544504]], dtype=float32)), 
# (('left_malignant', 'MLO'), array([[0.42063877, 0.57936126]], dtype=float32)), 
# (('right_malignant', 'CC'), array([[0.46159303, 0.53840697]], dtype=float32)), 
# (('right_malignant', 'MLO'), array([[0.47763485, 0.5223651 ]], dtype=float32))])

# Aqui pega apenas a segunda prob de cada linha acima 

# pred_df label      left_benign           right_benign           left_malignant           right_malignant          
# view_angle          CC       MLO           CC       MLO             CC       MLO              CC       MLO
# 0               0.5417  0.480124     0.420358  0.471852       0.525445  0.579361        0.538407  0.522365
# predictions [[0.51091206 0.44610482 0.55240315 0.53038603]]
# 100%|████████████████████████████████████████████████████████████████████████████████████████| 1/1 [00:06<00:00,  6.11s/it]
# [array([0.51133454, 0.44542885, 0.55090743, 0.53175557], dtype=float32)]

            # esse print referencia abaixo eh para um mini-batch mini_bs=1

            #     # forward pass
            #     output = model(tensor_batch)

            #     # print output
            #     # for k, v in output.items():
            #     #     print(k, v)

            #     # CC tensor([[[-0.4861, -0.9545],
            #     # [-0.6781, -0.7084],
            #     # [-0.6533, -0.7347],
            #     # [-0.7036, -0.6828]]], device='cuda:1', grad_fn=<LogSoftmaxBackward>)
            #     # MLO tensor([[[-0.7638, -0.6272],
            #     # [-0.9216, -0.5073],
            #     # [-0.7954, -0.6004],
            #     # [-0.7172, -0.6696]]], device='cuda:1', grad_fn=<LogSoftmaxBackward>)

            #     # tensor de saida eh um dicionario com as Views
            #     # vamos concatenar para ter um tensor unico e fazer as mesmas operacoes que
            #     # que sao realizadas na predicao original (test)
            #     output_cat = torch.cat([output["CC"], output["MLO"]], dim=1)
                
            #     #print(output_cat)
            #     # tensor([[[-0.7268, -0.6606],
            #     # [-0.8160, -0.5838],
            #     # [-0.6278, -0.7631],
            #     # [-0.4835, -0.9588],
            #     # [-0.8818, -0.5345],
            #     # [-0.7464, -0.6426],
            #     # [-0.6236, -0.7679],
            #     # [-0.5358, -0.8799]]], device='cuda:1', grad_fn=<CatBackward>)
                
            #     exp1 = torch.exp(output_cat)
                
            #     #print ('exp1: ', exp1)
            #     # exp1:  tensor([[[0.4834, 0.5166],
            #     # [0.4422, 0.5578],
            #     # [0.5338, 0.4662],
            #     # [0.6166, 0.3833],
            #     # [0.4140, 0.5860],
            #     # [0.4741, 0.5259],
            #     # [0.5360, 0.4640],
            #     # [0.5852, 0.4148]]], device='cuda:1', grad_fn=<ExpBackward>)

            #     # Realiza o calculo de cada probabilidade 
            #     left_benign_y_hat = torch.div(torch.add(exp1[0][0][1], exp1[0][4][1]), 2)
            #     right_benign_y_hat = torch.div(torch.add(exp1[0][1][1], exp1[0][5][1]), 2)
            #     left_malign_y_hat = torch.div(torch.add(exp1[0][2][1], exp1[0][6][1]), 2)
            #     right_malign_y_hat = torch.div(torch.add(exp1[0][3][1], exp1[0][7][1]), 2)

            #     # print('Train preds: ', left_benign_y_hat, right_benign_y_hat, left_malign_y_hat, right_malign_y_hat)
            #     # Train preds:  tensor(0.5513, device='cuda:1', grad_fn=<DivBackward0>) 
            #     # tensor(0.5419, device='cuda:1', grad_fn=<DivBackward0>) t
            #     # ensor(0.4651, device='cuda:1', grad_fn=<DivBackward0>) 
            #     # tensor(0.3991, device='cuda:1', grad_fn=<DivBackward0>)

            #     # concatena as saida individuais para o Loss
            #     y_hat = torch.stack([left_benign_y_hat, right_benign_y_hat, left_malign_y_hat, right_malign_y_hat])
                
            #     # print(y_hat)
            #     # tensor([0.5513, 0.5419, 0.4651, 0.3991], device='cuda:1', grad_fn=<StackBackward>)

