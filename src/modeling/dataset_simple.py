# training reading patches already made. (not in use now)

import numpy as np
import os
from PIL import Image
import glob
import torch
import torch.nn as nn
from torch.autograd import Variable
from random import randint
from torch.utils.data.dataset import Dataset
import torchvision.transforms.functional as TF
import torchvision.transforms as transforms
import random

import pandas as pd

import cv2

class MyDataset(Dataset):
    def __init__(self, image_paths, target_paths, train=True):
        self.image_paths = image_paths
        self.target_paths = target_paths
        self.files = os.listdir(self.image_paths)
        #self.labels = os.listdir(self.target_paths)

        print ('AQUI')

         # read labels file
        label_file = 'labels/ddsm_description_cases_ALL.csv'
        self.df = pd.read_csv(label_file)

    def transform(self, image, mask):

        # Resize
        resize = transforms.Resize(size=(140, 140))
        image = resize(image)
        mask = resize(mask)

        # # random angle
        angle = transforms.RandomRotation.get_params((-15,15))
        image = TF.rotate(image, angle)
        mask = TF.rotate(mask, angle)

        # Random crop
        i, j, h, w = transforms.RandomCrop.get_params(
            image, output_size=(128, 128))
        image = TF.crop(image, i, j, h, w)
        mask = TF.crop(mask, i, j, h, w)

        # Random horizontal flipping
        if random.random() > 0.5:
            image = TF.hflip(image)
            mask = TF.hflip(mask)

        # Random vertical flipping
        if random.random() > 0.5:
            image = TF.vflip(image)
            mask = TF.vflip(mask)

        # change to [0,1]
        image = np.array(image)
        image = image/255.0

        mask = np.array(mask)
        mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY) # fix dataset
        mask = mask/255.0
        mask[mask>=0.5] = 1; mask[mask<0.5] = 0

        # Transform to tensor
        image = TF.to_tensor(image)
        mask = TF.to_tensor(mask)

        #print(image.shape, mask.shape)

        return image.float(), mask.long() #image, mask

    def passthrough(self, image, label):

        #image = np.array(image)
        image = self.standard_normalize(image)

        image = image[:, :, np.newaxis]

        # mask = np.array(mask)
        # mask = mask/255.0
        # mask[mask>=0.5] = 1; mask[mask<0.5] = 0

        print()

        image = TF.to_tensor(image)
        # mask = TF.to_tensor(mask)

        return image, label

    def __len__(self):
        return len(self.files)


    # normalize accordingly for model
    def standard_normalize(self, image):
        image = np.float32(image)
        image -= np.mean(image)
        image /= np.maximum(np.std(image), 10**(-5))
        return image

    # Here we should deliver a augmeted image
    def process_augment_inputs(self, image):

        #cropped_image = cv2.imread(cropped_mammogram_path, -1)
        cropped_image = np.float32(cropped_image)
        cropped_image = self.standard_normalize(cropped_image)

        return cropped_image[:, :, np.newaxis]
    
    def read_label(self, df, exam, mini_bs, print_tensors):
        """
        Returns tensor with labels (ground truth) of current exam.
        Expects csv read in df, current exam, size of mini-batch.
        """

        # Carrega labels padrao deste exame do csv
        x = exam['L-CC'][0]
        label_row = df[df['patient_id'].str.contains(x[:6])].values
        if len(label_row) > 0:
            labels = torch.tensor(np.array(np.array(label_row[0, 1:5], dtype=np.uint8)), dtype=torch.float)
        else:
            print('Label not found, using all [0, 0, 0, 0], exam: ', x[:6])
            labels = torch.tensor(np.array([0, 0, 0, 0]), dtype=torch.float)

        # replica label por mini_bs vezes
        labels=labels.view(-1, 1).repeat(1, mini_bs)
        
        if print_tensors:
            print('Labels ', labels, labels.size(), labels.dim())

        return labels

    def __getitem__(self,idx):
        img_name = self.files[idx]
        #label_name = self.labels[idx]

        #image = cv2.imread(os.path.join(self.image_paths,img_name), -1) #da pau passar numpy

        label = np.array([0, 0, 0, 0])

        print(img_name)

        image = Image.open(os.path.join(self.image_paths,img_name))
        #mask = Image.open(os.path.join(self.target_paths,label_name))

        #x, y = self.transform(image, mask)
        x, y = self.passthrough(image, label)   # usar para images/labels já aug.

        return x, y