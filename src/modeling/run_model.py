# Copyright (C) 2019 Nan Wu, Jason Phang, Jungkyu Park, Yiqiu Shen, Zhe Huang, Masha Zorin,
#   Stanisław Jastrzębski, Thibault Févry, Joe Katsnelson, Eric Kim, Stacey Wolfson, Ujas Parikh,
#   Sushma Gaddam, Leng Leng Young Lin, Kara Ho, Joshua D. Weinstein, Beatriu Reig, Yiming Gao,
#   Hildegard Toth, Kristine Pysarenko, Alana Lewin, Jiyon Lee, Krystal Airola, Eralda Mema,
#   Stephanie Chung, Esther Hwang, Naziya Samreen, S. Gene Kim, Laura Heacock, Linda Moy,
#   Kyunghyun Cho, Krzysztof J. Geras
#
# This file is part of breast_cancer_classifier.
#
# breast_cancer_classifier is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# breast_cancer_classifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with breast_cancer_classifier.  If not, see <http://www.gnu.org/licenses/>.
# ==============================================================================

#
#
# Para fazer inferencia com rede NYU
#
# Rodar:
#
# python3 ./classify_nyu.py  -d 'test_inbreast_36/' -t 'test_inbreast_36_dest/' -r 'test_inbreast_36_dest/'
# 
# Rodar NYU model original
#
#

"""
Runs the image only model and image+heatmaps model for breast cancer prediction.
"""
import argparse
import collections as col
import numpy as np
import os
import pandas as pd
import torch
import tqdm
import sys

import src.utilities.pickling as pickling
import src.utilities.tools as tools
import src.modeling.models as models
import src.data_loading.loading as loading
from src.constants import VIEWS, VIEWANGLES, LABELS, MODELMODES

from sklearn.metrics import roc_auc_score, auc



DATASET = 'ICESP'

if DATASET == 'DDSM':
    label_file = 'labels/ddsm_description_cases_ALL.csv'
elif DATASET == 'INBREAST':
    label_file = 'labels/inbreast_4_images_cases_ALL.csv'
elif DATASET == 'ICESP':
    label_file = 'labels/relacao_diagnostico_lado_tipolesao_ate226.csv'



def load_model(parameters):
    """
    Loads trained cancer classifier
    """
    input_channels = 3 if parameters["use_heatmaps"] else 1
    model_class = {
        MODELMODES.VIEW_SPLIT: models.SplitBreastModel,
        MODELMODES.IMAGE: models.ImageBreastModel,
    }[parameters["model_mode"]]

    # select model, original (NYU) or custom (trained here)
    model = model_class(input_channels)
    if (parameters["model_path"]).endswith('.p'):
        model.load_state_dict(torch.load(parameters["model_path"])["model"]) # NYU model 
    elif (parameters["model_path"]).endswith('.pt'):
        model.load_state_dict(torch.load(parameters["model_path"]))           # custom


    # custom model
    #model = torch.load(parameters["model_path"])

    if (parameters["device_type"] == "gpu") and torch.has_cudnn:
        device = torch.device("cuda:{}".format(parameters["gpu_number"]))
    else:
        device = torch.device("cpu")

    # print('Printing the model: ')
    # print(model)



    model = model.to(device)

    # # freeze model parametes
    # for param in model.parameters():
    #     param.requires_grad = False

    # # change the final layer of resnet50 for transfer learning
    # #fc_inputs = model.fc.in_features
    # fc_inputs_cc  = model.output_layer_cc.fc_layer.in_features
    # fc_inputs_mlo = model.output_layer_mlo.fc_layer.in_features

    # print(fc_inputs_cc)

    # sys.exit()

    model.eval()
    return model, device


def run_model(model, device, exam_list, parameters):
    """
    Returns predictions of image only model or image+heatmaps model.
    Prediction for each exam is averaged for a given number of epochs.
    """
    random_number_generator = np.random.RandomState(parameters["seed"])

    image_extension = ".hdf5" if parameters["use_hdf5"] else ".png"

    with torch.no_grad():
        predictions_ls = []
        for datum in tqdm.tqdm(exam_list):
            predictions_for_datum = []
            loaded_image_dict = {view: [] for view in VIEWS.LIST}
            loaded_heatmaps_dict = {view: [] for view in VIEWS.LIST}
            for view in VIEWS.LIST:
                for short_file_path in datum[view]:
                    loaded_image = loading.load_image(
                        image_path=os.path.join(parameters["image_path"], short_file_path + image_extension),
                        view=view,
                        horizontal_flip=datum["horizontal_flip"],
                    )
                    if parameters["use_heatmaps"]:
                        loaded_heatmaps = loading.load_heatmaps(
                            benign_heatmap_path=os.path.join(parameters["heatmaps_path"], "heatmap_benign",
                                                             short_file_path + ".hdf5"),
                            malignant_heatmap_path=os.path.join(parameters["heatmaps_path"], "heatmap_malignant",
                                                                short_file_path + ".hdf5"),
                            view=view,
                            horizontal_flip=datum["horizontal_flip"],
                        )
                    else:
                        loaded_heatmaps = None

                    loaded_image_dict[view].append(loaded_image)
                    loaded_heatmaps_dict[view].append(loaded_heatmaps)
            for data_batch in tools.partition_batch(range(parameters["num_epochs"]), parameters["batch_size"]):
                batch_dict = {view: [] for view in VIEWS.LIST}
                for _ in data_batch:
                    for view in VIEWS.LIST:
                        image_index = 0
                        if parameters["augmentation"]:
                            image_index = random_number_generator.randint(low=0, high=len(datum[view]))
                        cropped_image, cropped_heatmaps = loading.augment_and_normalize_image(
                            image=loaded_image_dict[view][image_index],
                            auxiliary_image=loaded_heatmaps_dict[view][image_index],
                            view=view,
                            best_center=datum["best_center"][view][image_index],
                            random_number_generator=random_number_generator,
                            augmentation=parameters["augmentation"],
                            max_crop_noise=parameters["max_crop_noise"],
                            max_crop_size_noise=parameters["max_crop_size_noise"],
                        )
                        if loaded_heatmaps_dict[view][image_index] is None:
                            batch_dict[view].append(cropped_image[:, :, np.newaxis])
                        else:
                            batch_dict[view].append(np.concatenate([
                                cropped_image[:, :, np.newaxis],
                                cropped_heatmaps,
                            ], axis=2))

                tensor_batch = {
                    view: torch.tensor(np.stack(batch_dict[view])).permute(0, 3, 1, 2).to(device)
                    for view in VIEWS.LIST
                }
                output = model(tensor_batch)
                batch_predictions = compute_batch_predictions(output, mode=parameters["model_mode"])
                pred_df = pd.DataFrame({k: v[:, 1] for k, v in batch_predictions.items()})
                pred_df.columns.names = ["label", "view_angle"]
                predictions = pred_df.T.reset_index().groupby("label").mean().T[LABELS.LIST].values
                predictions_for_datum.append(predictions)
            predictions_ls.append(np.mean(np.concatenate(predictions_for_datum, axis=0), axis=0))

    return np.array(predictions_ls)


def compute_batch_predictions(y_hat, mode):
    """
    Format predictions from different heads
    """

    if mode == MODELMODES.VIEW_SPLIT:
        assert y_hat[VIEWANGLES.CC].shape == (1, 4, 2)
        assert y_hat[VIEWANGLES.MLO].shape == (1, 4, 2)
        batch_prediction_tensor_dict = col.OrderedDict()
        batch_prediction_tensor_dict[LABELS.LEFT_BENIGN, VIEWANGLES.CC] = y_hat[VIEWANGLES.CC][:, 0]
        batch_prediction_tensor_dict[LABELS.LEFT_BENIGN, VIEWANGLES.MLO] = y_hat[VIEWANGLES.MLO][:, 0]
        batch_prediction_tensor_dict[LABELS.RIGHT_BENIGN, VIEWANGLES.CC] = y_hat[VIEWANGLES.CC][:, 1]
        batch_prediction_tensor_dict[LABELS.RIGHT_BENIGN, VIEWANGLES.MLO] = y_hat[VIEWANGLES.MLO][:, 1]
        batch_prediction_tensor_dict[LABELS.LEFT_MALIGNANT, VIEWANGLES.CC] = y_hat[VIEWANGLES.CC][:, 2]
        batch_prediction_tensor_dict[LABELS.LEFT_MALIGNANT, VIEWANGLES.MLO] = y_hat[VIEWANGLES.MLO][:, 2]
        batch_prediction_tensor_dict[LABELS.RIGHT_MALIGNANT, VIEWANGLES.CC] = y_hat[VIEWANGLES.CC][:, 3]
        batch_prediction_tensor_dict[LABELS.RIGHT_MALIGNANT, VIEWANGLES.MLO] = y_hat[VIEWANGLES.MLO][:, 3]
        batch_prediction_dict = col.OrderedDict([
            (k, np.exp(v.cpu().detach().numpy()))
            for k, v in batch_prediction_tensor_dict.items()
        ])
    elif mode == MODELMODES.IMAGE:
        assert y_hat[VIEWS.L_CC].shape == (1, 2, 2)
        assert y_hat[VIEWS.R_CC].shape == (1, 2, 2)
        assert y_hat[VIEWS.L_MLO].shape == (1, 2, 2)
        assert y_hat[VIEWS.R_MLO].shape == (1, 2, 2)
        batch_prediction_tensor_dict = col.OrderedDict()
        batch_prediction_tensor_dict[LABELS.LEFT_BENIGN, VIEWS.L_CC] = y_hat[VIEWS.L_CC][:, 0]
        batch_prediction_tensor_dict[LABELS.LEFT_BENIGN, VIEWS.L_MLO] = y_hat[VIEWS.L_MLO][:, 0]
        batch_prediction_tensor_dict[LABELS.RIGHT_BENIGN, VIEWS.R_CC] = y_hat[VIEWS.R_CC][:, 0]
        batch_prediction_tensor_dict[LABELS.RIGHT_BENIGN, VIEWS.R_MLO] = y_hat[VIEWS.R_MLO][:, 0]
        batch_prediction_tensor_dict[LABELS.LEFT_MALIGNANT, VIEWS.L_CC] = y_hat[VIEWS.L_CC][:, 1]
        batch_prediction_tensor_dict[LABELS.LEFT_MALIGNANT, VIEWS.L_MLO] = y_hat[VIEWS.L_MLO][:, 1]
        batch_prediction_tensor_dict[LABELS.RIGHT_MALIGNANT, VIEWS.R_CC] = y_hat[VIEWS.R_CC][:, 1]
        batch_prediction_tensor_dict[LABELS.RIGHT_MALIGNANT, VIEWS.R_MLO] = y_hat[VIEWS.R_MLO][:, 1]

        batch_prediction_dict = col.OrderedDict([
            (k, np.exp(v.cpu().detach().numpy()))
            for k, v in batch_prediction_tensor_dict.items()
        ])
    else:
        raise KeyError(mode)
    return batch_prediction_dict


# get exam number from filename (ICESP)
def get_id_from_filename(image):
    folder = image[0:3]
    if not folder.isdigit():
        folder = image[0:2]
        if not folder.isdigit():
            folder = image[0:1]
            if not folder.isdigit():
                print('Not a valid filename', image)
                sys.exit()
    return folder

# DP
QUIET = False
def read_label(df, exam, mini_bs, print_tensors):
    """
    Returns tensor with labels (ground truth) of current exam.
    Expects csv read in df, current exam, size of mini-batch.
    """

    # Carrega labels padrao deste exame do csv
    x = exam['L-CC'][0]

    if DATASET == 'DDSM':
        search_str = x[:6]
    elif DATASET == 'INBREAST':
        search_str = x[9:25]
    elif DATASET == 'ICESP':
        search_str = get_id_from_filename(x)


    #print('x: ', x, ' str: ', search_str)
    if DATASET == 'ICESP':
        label_row = df.loc[df['patient_id'] == int(search_str)].values
    else:
        label_row = df[df['patient_id'].str.contains(search_str)].values

    if len(label_row) > 0:
        labels = np.array(np.array(label_row[0, 1:5], dtype=np.uint8), dtype=np.float32)
    else:
        if not QUIET:
            print('Label not found, using all [0, 0, 0, 0], exam: ', x[:6])
        labels = np.array([0, 0, 0, 0], dtype=np.float32)
    
    if print_tensors:
        print('Labels ', labels, labels.shape, label_row)
        if len(label_row) > 0: print('Labels ', label_row[0, 0])
        #else: print('Labels [0, 0, 0, 0]')

    return labels


def calc_auc(exam_list, ID_predictions):

    # read labels file (AUC)
#    label_file = 'labels/ddsm_description_cases_ALL.csv'
    df = pd.read_csv(label_file)
    y_hat_auc = []
    label_auc = []

    # Get labels for AUC
    for i in range(len(exam_list)):
        # Carrega labels padrao deste exame do csv
        labels = read_label(df, exam_list[i], 1, False)
        #label_auc = np.append(label_auc, labels)
        label_auc.append(np.float32(labels))

    label_auc = np.array(label_auc)
    label_auc.reshape((len(exam_list), 4))
    #print('Labels', label_auc, label_auc[:, 0:2].ravel())

    # Now get predictions
    for y_hat in ID_predictions:
        #print(y_hat[0])
        #y_hat_auc = np.append(y_hat_auc, np.float32(y_hat[1:5]))
        y_hat_auc.append(np.float32(y_hat[1:5]))
 
    #print('Predictions: ', y_hat_auc)#, y_hat_auc.shape)
    y_hat_auc = np.array(y_hat_auc)
    y_hat_auc.reshape((-1, 4))
    #print(y_hat_auc, y_hat_auc.shape, y_hat_auc[:, 0:2])

    # AUC
    print('Sizes: exam_list: ', len(exam_list),  ' label: ', label_auc.shape, ' y_hat: ', y_hat_auc.shape, ' DB: ', DATASET)
    #print(label_auc, y_hat_auc)

    # calculate AUC
    if (np.count_nonzero(label_auc[:, 0:2])):  # happens in some tests...
        auc_ = roc_auc_score(label_auc[:, 0:2].ravel(), y_hat_auc[:, 0:2].ravel())
        print('AUC Benign: %.3f' % auc_)
    else:
        print('[AUC Benign] Only zero labels')

    auc_ = roc_auc_score(label_auc[:, 2:5].ravel(), y_hat_auc[:, 2:5].ravel())
    print('AUC Malignant: %.3f' % auc_)

    # auc_ = roc_auc_score(label_auc.ravel(), y_hat_auc.ravel())
    # print('\nAUC Test: {:.3f} '.format(auc_))



def load_run_save(data_path, output_path, parameters):
    """
    Outputs the predictions as csv file
    """
    exam_list = pickling.unpickle_from_file(data_path)
    model, device = load_model(parameters)
    predictions = run_model(model, device, exam_list, parameters)
    os.makedirs(os.path.dirname(output_path), exist_ok=True)

    # Take the positive prediction

    # insert the first column as ID DGPP
    col1 = [] 
    for i in range(len(exam_list)):
        col1.append([exam_list[i]['case'][0].split('_')[0]]) # remove _png sufix

    # fprmat output predicions with first column with ID
    colnp = np.array(col1)
    ID_predictions = np.hstack((colnp, predictions)) # join ID with predictions

    # screen output
    print('Using model: ', parameters["model_path"])

    # calculate AUC
    calc_auc(exam_list, ID_predictions)


    # insert ID column header
    lista = ['ID']
    for x in LABELS.LIST:
        lista.append(x)

    df = pd.DataFrame(ID_predictions, columns=lista)
    #print(df.head())

    #df = pd.DataFrame(predictions, columns=LABELS.LIST) #orig
    df.to_csv(output_path, index=False, float_format='%.4f')

    # Also output to main dir if single
    if (exam_list[0]['case'] == ['single']):
        print('\nResultado predicao tipo "', output_path.split('/')[-1].split('_')[0], '" deste exame:')
        print(df)
        print('arquivo csv: ', output_path, '\n')


def main():
    parser = argparse.ArgumentParser(description='Run image-only model or image+heatmap model')
    parser.add_argument('--model-mode', default=MODELMODES.VIEW_SPLIT, type=str)
    parser.add_argument('--model-path', required=True)
    parser.add_argument('--data-path', required=True)
    parser.add_argument('--image-path', required=True)
    parser.add_argument('--output-path', required=True)
    parser.add_argument('--batch-size', default=1, type=int)
    parser.add_argument('--seed', default=0, type=int)
    parser.add_argument('--use-heatmaps', action="store_true")
    parser.add_argument('--heatmaps-path')
    parser.add_argument('--use-augmentation', action="store_true")
    parser.add_argument('--use-hdf5', action="store_true")
    parser.add_argument('--num-epochs', default=1, type=int)
    parser.add_argument('--device-type', default="cpu", choices=['gpu', 'cpu'])
    parser.add_argument("--gpu-number", type=int, default=0)
    args = parser.parse_args()

    parameters = {
        "device_type": args.device_type,
        "gpu_number": args.gpu_number,
        "max_crop_noise": (100, 100),
        "max_crop_size_noise": 100,
        "image_path": args.image_path,
        "batch_size": args.batch_size,
        "seed": args.seed,
        "augmentation": args.use_augmentation,
        "num_epochs": args.num_epochs,
        "use_heatmaps": args.use_heatmaps,
        "heatmaps_path": args.heatmaps_path,
        "use_hdf5": args.use_hdf5,
        "model_mode": args.model_mode,
        "model_path": args.model_path,
    }
    load_run_save(
        data_path=args.data_path,
        output_path=args.output_path,
        parameters=parameters,
    )


if __name__ == "__main__":
    main()
