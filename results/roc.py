#
# Print ROC and PR curves and AUC values
# 
# Need to use harcoded names yet
#
# Run in top folder:
# python3 ../roc.py
#
# DGPP - Aug/2019


import pandas as pd
import numpy as np
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score, auc
from sklearn.metrics import average_precision_score
from sklearn.metrics import precision_recall_curve
import matplotlib.pyplot as plt
from itertools import cycle
import sys
import time
import datetime
import os


# NET TYPE IMAGE or IMAGE+HEATMAPS
#net_type = 'imageheatmaps'
net_type = 'image'



pltpath = 'plots'
# cria diretorio plots
if (not os.path.isdir(pltpath)):
    #print ("Not a valid output directory")
    print('Creating dir: ', pltpath)
    os.makedirs(pltpath, exist_ok=True)
    if (not os.path.isdir(pltpath)):
        print("Error creating output dir")
        sys.exit()

# read cancers labels (ground truth)
labels_pd = pd.read_excel('labels_cancers_100.xlsx', index_col=0)  # doctest: 

labels = np.asarray(labels_pd)
print(labels.shape)
print(labels)

# cancers
if (net_type == 'imageheatmaps'):
      predictions_pd = pd.read_excel('imageheatmaps_predictions.xlsx', index_col=0) 
elif (net_type == 'image'):
      #predictions_pd = pd.read_excel('100_cancers_image_predictions.xlsx', index_col=0)  
      predictions_pd = pd.read_csv('100_cancers_image_predictions.csv', 
                                    usecols=["left_benign", "right_benign", "left_malignant", "right_malignant"])
predictions = np.asarray(predictions_pd)
print(predictions.shape, predictions.dtype)
print(predictions)

# normals
if (net_type == 'imageheatmaps'):
      predictions_normals_pd = pd.read_excel('normals_imageheatmaps_predictions.xlsx', index_col=0)  
elif (net_type == 'image'):
      #predictions_normals_pd = pd.read_excel('100_normals_image_predictions.xlsx', index_col=0)  
      predictions_normals_pd = pd.read_csv('100_normals_image_predictions.csv', 
                                    usecols=["left_benign", "right_benign", "left_malignant", "right_malignant"]) 

predictions_normals = np.asarray(predictions_normals_pd)
print(predictions_normals.shape, predictions_normals.dtype)
print(predictions_normals)

# create labels normals = all zeros (==ground truth)
labels_normals = np.zeros_like(labels)
print(labels_normals.shape)
print(labels_normals)

# join labels and predictions 
labels = np.concatenate((labels, labels_normals))
predictions = np.concatenate((predictions, predictions_normals))

print(predictions.shape)
print(labels.shape)

# calculate AUC
auc_ = roc_auc_score(labels[:, 0:2], predictions[:, 0:2])
print('AUC Benign: %.3f' % auc_)

auc_ = roc_auc_score(labels[:, 2:5], predictions[:, 2:5])
print('AUC Malignant: %.3f' % auc_)


# using ravel - most correct
auc_ = roc_auc_score(labels[:, 0:2].ravel(), predictions[:, 0:2].ravel())
print('AUC Benign - 2: %.3f' % auc_)

auc_ = roc_auc_score(labels[:, 2:5].ravel(), predictions[:, 2:5].ravel())
print('AUC Malignant: %.3f' % auc_)


# get time for file naming
ts = time.time()
st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d-%H:%M:%S')

# Compute ROC curve and ROC area for each class BENIGN (0..1)
fpr = dict()
tpr = dict()
roc_auc = dict()
fpr["micro"], tpr["micro"], thr = roc_curve(labels[:, 0:2].ravel(), predictions[:, 0:2].ravel())
roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

print(fpr["micro"], tpr["micro"], thr)

# Plot all ROC curves
plt.figure()
plt.plot(fpr["micro"], tpr["micro"],
         label='ROC curve (area = {0:0.2f})'
               ''.format(roc_auc["micro"]),
         color='deeppink', linestyle=':', linewidth=4)

plt.plot([0, 1], [0, 1], 'k--', lw=2)
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Benigno '+net_type+' (left+right)')
plt.legend(loc="lower right")
plt.savefig('plots/'+str(st)+'_Benigno_ROC_'+net_type+'.png')
plt.show()



##### Compute ROC curve and ROC area for each class MALIGN (2..3)
fpr = dict()
tpr = dict()
roc_auc = dict()
# Compute micro-average ROC curve and ROC area
fpr["micro"], tpr["micro"], thr = roc_curve(labels[:, 2:5].ravel(), predictions[:, 2:5].ravel())
roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])	

print(fpr["micro"], tpr["micro"], thr)

# Plot all ROC curves
plt.figure()
plt.plot(fpr["micro"], tpr["micro"],
         label='ROC curve (area = {0:0.2f})'
               ''.format(roc_auc["micro"]),
         color='deeppink', linestyle=':', linewidth=4)

plt.plot([0, 1], [0, 1], 'k--', lw=2)
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Maligno '+net_type+' (left+right)')
plt.legend(loc="lower right")
plt.savefig('plots/'+str(st)+'_Maligno_ROC_'+net_type+'.png')
plt.show()


# Precision/recal curve - Malign
precision, recall, thresholds = precision_recall_curve(labels[:, 2:5].ravel(), predictions[:, 2:5].ravel())
average_precision = average_precision_score(labels[:, 2:5].ravel(), predictions[:, 2:5].ravel())

print('Average precision-recall score: {0:0.2f}'.format(average_precision))

# Plot all ROC curves
plt.figure()
plt.plot(precision, recall,
         label='Precision-Recall curve (area = {0:0.2f})'
               ''.format(average_precision),
         color='deeppink', linestyle=':', linewidth=4)

plt.plot([0, 1], [0, 1], 'k--', lw=2)
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('Recall')
plt.ylabel('Precision')
plt.title('Maligno '+net_type+' (left+right)')
plt.legend(loc="lower right")
plt.savefig('plots/'+str(st)+'_Maligno_PR_'+net_type+'.png')
plt.show()