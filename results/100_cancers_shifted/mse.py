import pandas as pd
import numpy as np

def mse(imageA, imageB):
	err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
	err /= float(imageA.shape[0] * imageA.shape[1])
	
	# return the MSE, the lower the error, the more "similar"
	# the two images are
	return err


labels_pd = pd.read_excel('labels_cancers_100.xlsx', index_col=0)  # doctest: 

labels = np.asarray(labels_pd)
print(labels.shape)
print(labels)


predictions_pd = pd.read_excel('summary_predictions_cancers_100.xlsx', index_col=0)  # doctest: 
predictions = np.asarray(predictions_pd)
print(predictions.shape)
print(predictions)


mse_error = mse(labels, predictions)
print('MSE: ', mse_error)

