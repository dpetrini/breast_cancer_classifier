import pandas as pd
import numpy as np
import argparse
import sys



# MAIN
ap = argparse.ArgumentParser(description='Convert ddsm csv in NYU label format.')
ap.add_argument("-f", "--file",  required = True,
     help = "csv file to convert.")

args = vars(ap.parse_args())

print(args)


if (args['file'].split('.')[-1] != 'csv'):
    print('Input must be csv file.')
    sys.exit()



# read to pandas frame

df = pd.read_csv(args['file'])

print(df[df['patient_id'].str.contains("C-0200")])

arrays = df[df['patient_id'].str.contains("C-0200")].values

print(arrays, arrays.shape, arrays[0, 1:5])
