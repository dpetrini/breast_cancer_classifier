#!/bin/bash

# para rodar rede NYU original com modelo oringal

F=$1
EXEC_PYTHON='python3'    # or 'python3'

echo 'Using python exec: --------->>> '$EXEC_PYTHON


NUM_PROCESSES=10
DEVICE_TYPE='gpu'
NUM_EPOCHS=10
HEATMAP_BATCH_SIZE=100
GPU_NUMBER=1

DATA_FOLDER='sample_data/images'
INITIAL_EXAM_LIST_PATH='sample_data/exam_list_before_cropping.pkl'
PATCH_MODEL_PATH='models/sample_patch_model.p'

IMAGE_MODEL_PATH='models/sample_image_model.p' # original
#IMAGE_MODEL_PATH='models/sample_image_model.p' # original

IMAGE_MODEL_PATH='models_train/2020-01-19-06:50:34_best_model_FT_DDSM_50ep.pt'

#IMAGEHEATMAPS_MODEL_PATH='models/sample_imageheatmaps_model.p'

IMAGEHEATMAPS_MODEL_PATH='models_train/2020-01-27-16:08_best_model_2_5_FT_1e-06_ICESP_20ep.pt'

CROPPED_IMAGE_PATH='sample_output/cropped_images'
CROPPED_EXAM_LIST_PATH='sample_output/cropped_images/cropped_exam_list.pkl'
EXAM_LIST_PATH='sample_output/data.pkl'
HEATMAPS_PATH='sample_output/heatmaps'
IMAGE_PREDICTIONS_PATH='sample_output/image_predictions.csv'
IMAGEHEATMAPS_PREDICTIONS_PATH='sample_output/imageheatmaps_predictions.csv'
export PYTHONPATH=$(pwd):$PYTHONPATH


# Seleciona se pula essas etapas - Mode fast training
if [ "$2" == "fast" ]
then   
    echo '--> Fast predict - not cropping/extract-centers'
else
    echo 'Stage 1: Crop Mammograms'
    $EXEC_PYTHON src/cropping/crop_mammogram.py \
        --input-data-folder $F$DATA_FOLDER \
        --output-data-folder $F$CROPPED_IMAGE_PATH \
        --exam-list-path $F$INITIAL_EXAM_LIST_PATH  \
        --cropped-exam-list-path $F$CROPPED_EXAM_LIST_PATH  \
        --num-processes $NUM_PROCESSES

    echo 'Stage 2: Extract Centers'
    $EXEC_PYTHON src/optimal_centers/get_optimal_centers.py \
        --cropped-exam-list-path $F$CROPPED_EXAM_LIST_PATH \
        --data-prefix $F$CROPPED_IMAGE_PATH \
        --output-exam-list-path $F$EXAM_LIST_PATH \
        --num-processes $NUM_PROCESSES
fi

#echo 'Stage 3: Generate Heatmaps'
#$EXEC_PYTHON src/heatmaps/run_producer.py \
#    --model-path $PATCH_MODEL_PATH \
#    --data-path $F$EXAM_LIST_PATH \
#    --image-path $F$CROPPED_IMAGE_PATH \
#    --batch-size $HEATMAP_BATCH_SIZE \
#    --output-heatmap-path $F$HEATMAPS_PATH \
#    --device-type $DEVICE_TYPE \
#    --gpu-number $GPU_NUMBER

# echo 'Stage 4a: Run Classifier (Image)'
# $EXEC_PYTHON src/modeling/run_model.py \
#      --model-path $IMAGE_MODEL_PATH \
#      --data-path $F$EXAM_LIST_PATH \
#      --image-path $F$CROPPED_IMAGE_PATH \
#      --output-path $F$IMAGE_PREDICTIONS_PATH \
#      --use-augmentation \
#      --num-epochs $NUM_EPOCHS \
#      --device-type $DEVICE_TYPE \
#      --gpu-number $GPU_NUMBER

echo 'Stage 4b: Run Classifier (Image+Heatmaps)'
$EXEC_PYTHON src/modeling/run_model.py \
   --model-path $IMAGEHEATMAPS_MODEL_PATH \
   --data-path $F$EXAM_LIST_PATH \
   --image-path $F$CROPPED_IMAGE_PATH \
   --output-path $F$IMAGEHEATMAPS_PREDICTIONS_PATH \
   --use-heatmaps \
   --heatmaps-path $F$HEATMAPS_PATH \
   --use-augmentation \
   --num-epochs $NUM_EPOCHS \
   --device-type $DEVICE_TYPE \
   --gpu-number $GPU_NUMBER
