# Wrapper para executar NYU com exame unico e batch de dados DDSM-new-version 
# Ult. Edicao: 09/08/2019
#
# Exemplos de execucao:
# python3 ./test_usp.py  -d 'test_inbreast_36/' -t 'test_inbreast_36_dest/' -r 'test_inbreast_36_dest/'
#
# Usa run_test.sh e sr/modeling/test_model.py
#
# # Colocar o modelo a ser usado hardcoded no arquivo run_test.sh
#
# (nao) Cria csvs no diretorio sample_output e para modo single exibe os resultados na tela tambem.

from subprocess import call
import argparse
import pickle
import os
import sys
from shutil import copyfile

INPUT_IMAGES = 'sample_data/images/'
OUTPUT_DIR = 'sample_output'
INPUT_PICKLE = 'sample_data/'

def unpickle_from_file(file_name):
     with open(file_name, 'rb') as handle:
         return pickle.load(handle)

def pickle_to_file(file_name, data, protocol = pickle.HIGHEST_PROTOCOL):
    with open(file_name, 'wb') as handle:    
        pickle.dump(data, handle, protocol)

# MAIN
ap = argparse.ArgumentParser(description='Customized NYU process (poli-usp). Please check you python executable in run.sh.')
ap.add_argument("-e", "--exam", required = False, nargs = '*',
    help = "name of 4 files in order: Left-CC, Right-CC Left-MLO Right-MLO")
ap.add_argument("-u", "--unique", required = False,
    help = "prefixo dos 4 arquivos de exame supondo _L_CC.png _R_CC.png _L_MLO.png _R_MLO.png")    
ap.add_argument("-d", "--dpath", required = False,
    help = "path to DDSM category top folder (batch processing)")
ap.add_argument("-t", "--tpath", required = False,
    help = "temporary folder for images and cropped images (batch processing)")
ap.add_argument("-r", "--rpath", required = False,
    help = "reuse temporary folder for images and cropped images (batch processing) - consistence up to you!")

args = vars(ap.parse_args())
#print (args)

if args['dpath'] is None and args['exam'] is None and args['unique'] is None:
    #print('This will proceed to regular sample_data/images folder processing:') 
    ap.print_help()     # imprime help
    sys.exit()  # for a while
    print('Running train script...')
    call_lst = ['./run_test.sh']
    call(call_lst)

# init database
exam_list = []
left_cc = ''
right_cc = ''
left_mlo = ''
right_mlo = ''

# entrada de prefixo para inferir nomes de exames
if args['unique'] is not None:

    prefix = args['unique']
    side_l = ''
    side_r = ''

    # checa qual tipo de lado 'L' ou 'LEFT' é valido
    if os.path.exists(prefix+'_L_CC.png'):
        side_l = '_L_'
        side_r = '_R_'
    elif os.path.exists(prefix+'_LEFT_CC.png'):
        side_l = '_LEFT_'
        side_r = '_RIGHT_'
    elif os.path.exists(prefix+'LCC.png'):
        side_l = 'L'
        side_r = 'R'
    else:
        ap.print_help()     # imprime help
        print('Nomes de arquivos inválidos')
        sys.exit()          


    args['exam'] = []
    args['exam'].append(prefix+side_l+'CC.png')
    args['exam'].append(prefix+side_r+'CC.png')
    args['exam'].append(prefix+side_l+'MLO.png')
    args['exam'].append(prefix+side_r+'MLO.png')

    print('Considerando exames: ', args['exam'])
    #sys.exit()


# lets handle 1 exam (4 images) user input
if args['exam'] is not None:

    # check if user input 4 images
    if len(args['exam']) is not 4:
        print('You must input 4 images a exam.') 
        sys.exit()  

    # create image directory if not exist
    if os.path.isdir(INPUT_IMAGES) is False:
        os.makedirs(INPUT_IMAGES, exist_ok=False)

    # clean images folder
    for curdir, dirs, files in os.walk(INPUT_IMAGES, topdown=False): 
        for name in files: 
            os.remove(os.path.join(curdir, name))

    # clean output folder
    for curdir, dirs, files in os.walk(OUTPUT_DIR, topdown=False):
        #print(curdir, dirs, files ) 
        for name in files: 
            os.remove(os.path.join(curdir, name))


    # copy images to data folder
    print('Exam input. Reading files.')
    for file in args['exam']:
        if os.path.isfile(file):
            #print(file, INPUT_IMAGES)
            #print("file:", os.path.basename(file), "dst:", INPUT_IMAGES+os.path.basename(file))
            #copyfile(file, INPUT_IMAGES+file)
            copyfile(file, INPUT_IMAGES+os.path.basename(file))
        else:
            print('Not valid input files.') 
            sys.exit() 


    # update pickle data
    exam_list.append({
        'horizontal_flip': 'NO',
        'L-CC': [os.path.basename(args['exam'][0]).split('.')[0]],
        'R-CC': [os.path.basename(args['exam'][1]).split('.')[0]],
        'L-MLO': [os.path.basename(args['exam'][2]).split('.')[0]],
        'R-MLO': [os.path.basename(args['exam'][3]).split('.')[0]],
        'case' : ['single'],             # pega os 4 primeiros chars...
    })

    pickle_to_file(INPUT_PICKLE+'exam_list_before_cropping.pkl', 
                exam_list, 
                protocol = pickle.HIGHEST_PROTOCOL)

    #print(exam_list)

    #run NYU
    if os.name == 'nt':
        print('Para Windows favor executar: "./run.sh" manualmente em shell bash (ex. Git bash).')
    else:
        print('Running train script...')
        call_lst = ['./run_test.sh']
        call(call_lst)

# let reuse already prepared ouput dir (with cropped imagens etc)
elif args['rpath'] is not None:
    print('DDSM-like processing (top folder with cases). WITH LAST PREPARED OUTPUT DIR - USE with care!')
    #run NYU
    if os.name == 'nt':
        print('Para Windows favor executar: "./run.sh ', args['tpath'], '" manualmente em shell bash (ex. Git bash).')
    else:
        print('Running train script...')
        call_lst = ['./run_test.sh', args['rpath'], 'fast'] 
        call(call_lst)

# lets handle many exams folder input (like ddsm part)     
elif args['dpath'] is not None:
    print('DDSM-like processing (top folder with cases).')

    if os.path.isdir(args['dpath']) is None:
        print("Invalid path.")

    # check for temp path
    if args['tpath'] is None:
        print("Must enter a temporary path for images and cropped imgs.")
        sys.exit()

    if os.path.isdir(args['tpath']) is None:
        print("Invalid temporary path.")

    # prepare temp path
    os.makedirs(os.path.join(args['tpath'], INPUT_IMAGES), exist_ok=True) # create if need
    # clean
    for curdir, dirs, files in os.walk(os.path.join(args['tpath'], INPUT_IMAGES), topdown=False): 
        for name in files: 
            os.remove(os.path.join(args['tpath'], INPUT_IMAGES, name))
    
    # TODO apagar também de OUTPUT_DIR/

    for curdir, dirs, files in os.walk(args['dpath']):
        print (curdir, dirs, files)
        for f in files:
            if f.endswith('.png'):
                print(curdir.split('/')[-1], f)
                copyfile(os.path.join(curdir,f), os.path.join(args['tpath'], INPUT_IMAGES, f))

                # check views - all possible combinations so far
                if 'L_CC' in f or 'LEFT_CC' in f or 'LCC' in f:
                    left_cc = f.split('.')[0]
                if 'R_CC' in f or 'RIGHT_CC' in f or 'RCC' in f :
                    right_cc = f.split('.')[0]
                if 'L_MLO' in f or 'LEFT_MLO' in f or 'LMLO' in f:
                    left_mlo = f.split('.')[0]
                if 'R_MLO' in f or 'RIGHT_MLO' in f or 'RMLO' in f:
                    right_mlo = f.split('.')[0]

        # update pickle data
        if len(files):
            if len(left_mlo) and len(left_cc) and len(right_mlo) and len(right_cc):
                exam_list.append({
                    'horizontal_flip': 'NO',
                    'L-CC': [left_cc],
                    'R-CC': [right_cc],
                    'L-MLO': [left_mlo],
                    'R-MLO': [right_mlo],
                    'case' : [curdir.split('/')[-1]]
                })
            else:
                print('Error in: ')
                print(files)
                sys.exit()

    print(exam_list)

    pickle_to_file(os.path.join(args['tpath'], INPUT_PICKLE, 'exam_list_before_cropping.pkl'), 
                exam_list, 
                protocol = pickle.HIGHEST_PROTOCOL)

    #sys.exit()

    #run NYU
    if os.name == 'nt':
        print('Para Windows favor executar: "./run.sh ', args['tpath'], '" manualmente em shell bash (ex. Git bash).')
    else:
        print('Running train script...')
        call_lst = ['./run_test.sh', args['tpath']]
        call(call_lst)




sys.exit()


# outpath = args["outpath"]

# # check user input

# # clean images folder

# # copy images to data folder

# # update pickle data
# exam_list = unpickle_from_file("./exam_list_before_cropping.pkl")

# exam_list.append({'horizontal_flip': 'NO', 
#                   'L-CC': ['38_L_CC'], 
#                   'R-CC': ['38_R_CC'], 
#                   'L-MLO': ['38_L_MLO'], 
#                   'R-MLO': ['38_R_MLO']})
# print (exam_list)
# pickle_to_file(INPUT_PICKLE+'exam_list_before_cropping.pkl', 
#                 exam_list, 
#                 protocol = pickle.HIGHEST_PROTOCOL)

# # executa NYU
# subprocess.call(['./run.sh'])