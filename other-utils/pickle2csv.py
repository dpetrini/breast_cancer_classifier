import csv
import pickle
import numpy
import sys

def unpickle_from_file(file_name):
     with open(file_name, 'rb') as handle:
         return pickle.load(handle)

#MAIN
path_pickle = sys.argv[1]
path_csv = sys.argv[2]

content = []
content = unpickle_from_file(path_pickle)

print (content)

# faz parsing to campo que queremos extrair no formato xcvfvfv
for line in content:
    print(line['case'][0].split('/')[-1].split('\\')[-1].split('_')[0])

with open(path_csv,'w', newline='') as f:
    writer = csv.writer(f)
    writer.writerow(['cases'])
    for line in content: 
        writer.writerow(line)
        #writer.writerow([line['case'][0].split('/')[-1].split('\\')[-1].split('_')[0]])
