# normalize 16 bits png file
#
# DGPP 01/07/2019

import numpy as np
import cv2
import sys

if len(sys.argv) != 2:
    print('Usage: python norm-ddsm.py imgname')
    sys.exit()

img = cv2.imread(sys.argv[1], -1)

max = np.max(img)
min = np.min(img)
print(img.shape, img.dtype, max, min)
#img =  cv2.normalize(img, None, alpha=0, beta=max-min, norm_type=cv2.NORM_MINMAX)
img =  cv2.normalize(img, None, alpha=0, beta=max, norm_type=cv2.NORM_MINMAX)

#cv2.imwrite(sys.argv[1].split('.')[0]+'_shift.png', img.astype(np.uint16))
cv2.imwrite(sys.argv[1].split('.')[0]+'_norm_max.png', img.astype(np.uint16))
