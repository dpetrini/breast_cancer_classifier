import csv
import pickle
import numpy
import sys

def unpickle_from_file(file_name):
     with open(file_name, 'rb') as handle:
         return pickle.load(handle)

#MAIN
path_pickle = sys.argv[1]

content = []
content = unpickle_from_file(path_pickle)

print (content)
