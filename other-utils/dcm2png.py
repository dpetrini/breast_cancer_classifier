# Converte dicom para png - usa 12 bits por default

import png
import pydicom
import sys


def save_dicom_image_as_png(dicom_filename, png_filename, bitdepth=12):
    """
    Save 12-bit mammogram from dicom as rescaled 16-bit png file.
    :param dicom_filename: path to input dicom file.
    :param png_filename: path to output png file.
    :param bitdepth: bit depth of the input image. Set it to 12 for 12-bit mammograms.
    """
    image = pydicom.read_file(dicom_filename).pixel_array
    with open(png_filename, 'wb') as f:
        writer = png.Writer(height=image.shape[0], width=image.shape[1], bitdepth=bitdepth, greyscale=True)
        writer.write(f, image.tolist())



if len(sys.argv) != 3:
    print('Usage: python dcm2png.py imgdcm imgpng')
    sys.exit()

save_dicom_image_as_png(sys.argv[1], sys.argv[2], bitdepth=12)