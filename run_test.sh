#!/bin/bash

# Roda teste dos modelos gerados com transfer learning

# Aqui se coloca os modelos recem criados em IMAGE_MODEL_PATH=

F=$1
FAST=$2

EXEC_PYTHON='python3'    # or 'python3'

echo 'Using python exec: --------->>> '$EXEC_PYTHON


NUM_PROCESSES=10
DEVICE_TYPE='gpu'
NUM_EPOCHS=10
HEATMAP_BATCH_SIZE=100
GPU_NUMBER=1

DATA_FOLDER='sample_data/images'
INITIAL_EXAM_LIST_PATH='sample_data/exam_list_before_cropping.pkl'
PATCH_MODEL_PATH='models/sample_patch_model.p'


IMAGE_MODEL_PATH='models/sample_image_model.p'

# AQUI SE COLOCAM OS MODELOS DE TRANSFER LEARNING QUE SE QUER TESTAR
#IMAGE_MODEL_PATH='models_train/2019-12-27-12:06:19_model_.pt'
#IMAGE_MODEL_PATH='models_train/2019-12-29-17:20:15_model_FE_inbreast_0_786.pt'

#IMAGE_MODEL_PATH='models_train/2020-01-14-12:04:25_model_FE_ICESP.pt'

#IMAGE_MODEL_PATH='models_train/2020-01-19-06:50:34_best_model_FT_DDSM_50ep.pt'



IMAGEHEATMAPS_MODEL_PATH='models/sample_imageheatmaps_model.p'

#IMAGEHEATMAPS_MODEL_PATH='models_train/2020-01-15-22:35:42_best_model_FE_ICESP_8ep.pt'


CROPPED_IMAGE_PATH='sample_output/cropped_images'
CROPPED_EXAM_LIST_PATH='sample_output/cropped_images/cropped_exam_list.pkl'
EXAM_LIST_PATH='sample_output/data.pkl'
HEATMAPS_PATH='sample_output/heatmaps'
IMAGE_PREDICTIONS_PATH='sample_output/image_predictions.csv'
IMAGEHEATMAPS_PREDICTIONS_PATH='sample_output/imageheatmaps_predictions.csv'
export PYTHONPATH=$(pwd):$PYTHONPATH

# Seleciona se pula essas etapas - Mode fast training
if [ "$2" == "fast" ]
then   
    echo '--> Fast training - not cropping/extract-centers'
else
    echo 'Stage 1: Crop Mammograms'
    $EXEC_PYTHON src/cropping/crop_mammogram.py \
        --input-data-folder $F$DATA_FOLDER \
        --output-data-folder $F$CROPPED_IMAGE_PATH \
        --exam-list-path $F$INITIAL_EXAM_LIST_PATH  \
        --cropped-exam-list-path $F$CROPPED_EXAM_LIST_PATH  \
        --num-processes $NUM_PROCESSES

    echo 'Stage 2: Extract Centers'
    $EXEC_PYTHON src/optimal_centers/get_optimal_centers.py \
        --cropped-exam-list-path $F$CROPPED_EXAM_LIST_PATH \
        --data-prefix $F$CROPPED_IMAGE_PATH \
        --output-exam-list-path $F$EXAM_LIST_PATH \
        --num-processes $NUM_PROCESSES

fi

# echo 'Stage 3: Generate Heatmaps'
# $EXEC_PYTHON src/heatmaps/run_producer.py \
#     --model-path $PATCH_MODEL_PATH \
#     --data-path $F$EXAM_LIST_PATH \
#     --image-path $F$CROPPED_IMAGE_PATH \
#     --batch-size $HEATMAP_BATCH_SIZE \
#     --output-heatmap-path $F$HEATMAPS_PATH \
#     --device-type $DEVICE_TYPE \
#     --gpu-number $GPU_NUMBER

# echo 'Stage 4a: Run Train (Image)'
# $EXEC_PYTHON src/modeling/test_model.py \
#    --model-path $IMAGE_MODEL_PATH \
#    --data-path $F$EXAM_LIST_PATH \
#    --image-path $F$CROPPED_IMAGE_PATH \
#    --output-path $F$IMAGE_PREDICTIONS_PATH \
#    --use-augmentation \
#    --num-epochs $NUM_EPOCHS \
#    --device-type $DEVICE_TYPE \
#    --gpu-number $GPU_NUMBER

echo 'Stage 4b: Run Classifier (Image+Heatmaps)'
$EXEC_PYTHON src/modeling/test_model.py \
    --model-path $IMAGEHEATMAPS_MODEL_PATH \
    --data-path $F$EXAM_LIST_PATH \
    --image-path $F$CROPPED_IMAGE_PATH \
    --output-path $F$IMAGEHEATMAPS_PREDICTIONS_PATH \
    --use-heatmaps \
    --heatmaps-path $F$HEATMAPS_PATH \
    --use-augmentation \
    --num-epochs $NUM_EPOCHS \
    --device-type $DEVICE_TYPE \
    --gpu-number $GPU_NUMBER
